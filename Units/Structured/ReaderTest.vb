﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting

Namespace Structured

  ''' <summary>
  ''' Contains all unit tests for the <see cref="isr.IO.Structured.Reader">Reader</see> class.
  ''' </summary>
  ''' <remarks></remarks>
''' <license>
''' (c) 2007 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
  ''' <history date="12/08/07" by="David Hary" revision="6.0.2898.x">
''' created
  ''' </history>
  <TestClass()> _
  Public Class ReaderTest

    Private Shared _testContext As TestContext
    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Shared Property TestContext() As TestContext
      Get
        Return _testContext
      End Get
      Set(ByVal value As TestContext)
        _testContext = value
      End Set
    End Property

#Region " ADDITIONAL TEST ATTRIBUTED METHODS "

    Private Const sectionName As String = "Unit Tests"
    Private Shared _reader As isr.IO.Structured.Reader
    Private Shared _writer As isr.IO.Structured.Writer
    Private Shared dateValue As DateTime = DateTime.Now
    Private Shared stringValue As String = "String value"
    Private Shared booleanValue As Boolean = True
    Private Shared byteValue As Byte = 129
    Private Shared decimalValue As Decimal = CDec(12.12)
    Private Shared doubleValue As Double = Math.PI
    Private Shared shortValue As Int16 = 16
    Private Shared integerValue As Int32 = 32
    Private Shared longValue As Int64 = 64
    Private Shared singleValue As Single = Single.MinValue

    ''' <summary>
    ''' Runs code before running the first test in the class.
    ''' Tests the connection to the module reader and instantiates 
    ''' a module reader module level elements.
    ''' </summary>
    ''' <param name="testContext"></param>
    ''' <remarks></remarks>
    <ClassInitialize()> _
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)

      Try

        ' write a reecord of data for testing to the test file.
        _writer = New isr.IO.Structured.Writer()

        ' ask for a new file.
        _writer.NewFile()

        ' ask for a new record
        _writer.NewRecord()

        ' add data to the record
        _writer.AddField(booleanValue)
        _writer.AddField(byteValue)
        _writer.AddField(dateValue)
        _writer.AddField(dateValue.ToOADate)
        _writer.AddField(decimalValue)
        _writer.AddField(doubleValue)
        _writer.AddField(longValue)
        _writer.AddField(singleValue)
        _writer.AddField(stringValue)
        _writer.WriteFields()
        _writer.Dispose()

        _reader = New isr.IO.Structured.Reader()
        Assert.IsTrue(_reader.ReadFields, "Structured.Reader.ReadFields from: {0} ", _reader.FilePathName)

      Catch ex As Exception

        ' close to meet strong guarantees
        Try
          ReaderTest.MyClassCleanup()
        Finally
        End Try

        ' throw an exception
        _testContext.WriteLine("Exception Opening. {0)", ex.Message)

      End Try
    End Sub

    ''' <summary>
    ''' Runs code after all tests in a class have run.
    ''' </summary>
    ''' <remarks></remarks>
    <ClassCleanup()> _
    Public Shared Sub MyClassCleanup()
      Try
        If Not _reader Is Nothing Then
          _reader.Dispose()
          _reader = Nothing
        End If
        If Not _writer Is Nothing Then
          _writer.Dispose()
          _writer = Nothing
        End If
      Catch ex As Exception
        ' throw an exception
        _testContext.WriteLine("Exception Opening. {0)", ex.Message)
      Finally
        _reader = Nothing
        _writer = Nothing
      End Try
    End Sub

#Region " UNUSED "
#If False Then

  ''' <summary>
  ''' Runs code before running the first test in the class.
  ''' Tests the connection to the module reader and instantiates 
  ''' a module reader module level elements.
  ''' </summary>
  ''' <param name="testContext"></param>
  ''' <remarks></remarks>
  <ClassInitialize()> _
  Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
  End Sub

  ''' <summary>
  ''' Runs code after all tests in a class have run.
  ''' </summary>
  ''' <remarks></remarks>
  <ClassCleanup()> _
  Public Shared Sub MyClassCleanup()
  End Sub

  ''' <summary>
  ''' Runs code before running each test
  ''' </summary>
  ''' <remarks></remarks>
  <TestInitialize()> _
  Public Sub MyTestInitialize()
  End Sub

  ''' <summary>
  ''' Runs code after each test has run.
  ''' </summary>
  ''' <remarks></remarks>
  <TestCleanup()> _
  Public Sub MyTestCleanup()
  End Sub

#End If
#End Region

#End Region

    '''<summary>
    '''A test for FieldBoolean
    '''</summary>
    <TestMethod()> _
    Public Sub FieldBooleanTest()

      Dim actual As Boolean = _reader.FieldBoolean()
      Dim expected As Boolean = booleanValue
      Assert.AreEqual(expected, actual, "Structured.Reader.FieldBoolean: Expected '{0}' ?= '{1}'", expected, actual)

    End Sub

    '''<summary>
    '''A test for FieldByte
    '''</summary>
    <TestMethod()> _
    Public Sub FieldByteTest()

      Dim actual As Byte = _reader.FieldByte()
      Dim expected As Byte = byteValue
      Assert.AreEqual(expected, actual, "Structured.Reader.FieldByte: Expected '{0}' ?= '{1}'", expected, actual)

    End Sub

    '''<summary>
    '''A test for FieldDate
    '''</summary>
    <TestMethod()> _
    Public Sub FieldDateTest()

      Dim actual As Date = _reader.FieldDate()
      Dim expected As Date = dateValue
      Assert.AreEqual(expected, actual, "Structured.Reader.FieldDate: Expected '{0}' ?= '{1}'", expected, actual)

    End Sub

    '''<summary>
    '''A test for FieldDateTime
    '''</summary>
    <TestMethod()> _
    Public Sub FieldDateTimeTest()

      Dim actual As DateTime = _reader.FieldDateTime()
      Dim expected As DateTime = dateValue
      Assert.AreEqual(expected, actual, "Structured.Reader.FieldDateTime: Expected '{0}' ?= '{1}'", expected, actual)

    End Sub

    '''<summary>
    '''A test for FieldDecimal
    '''</summary>
    <TestMethod()> _
    Public Sub FieldDecimalTest()

      Dim actual As Decimal = _reader.FieldDecimal()
      Dim expected As Decimal = decimalValue
      Assert.AreEqual(expected, actual, "Structured.Reader.FieldDecimal: Expected '{0}' ?= '{1}'", expected, actual)

    End Sub

    '''<summary>
    '''A test for FieldDouble
    '''</summary>
    <TestMethod()> _
    Public Sub FieldDoubleTest()

      Dim actual As Double = _reader.FieldDouble()
      Dim expected As Double = doubleValue
      Assert.AreEqual(expected, actual, "Structured.Reader.FieldDouble: Expected '{0}' ?= '{1}'", expected, actual)

    End Sub

    '''<summary>
    '''A test for FieldInt16
    '''</summary>
    <TestMethod()> _
    Public Sub FieldInt16Test()

      Dim actual As Int16 = _reader.FieldInt16()
      Dim expected As Int16 = shortValue
      Assert.AreEqual(expected, actual, "Structured.Reader.FieldInt16: Expected '{0}' ?= '{1}'", expected, actual)

    End Sub

    '''<summary>
    '''A test for FieldInt32
    '''</summary>
    <TestMethod()> _
    Public Sub FieldInt32Test()

      Dim actual As Int32 = _reader.FieldInt32()
      Dim expected As Int32 = integerValue
      Assert.AreEqual(expected, actual, "Structured.Reader.FieldInt32: Expected '{0}' ?= '{1}'", expected, actual)

    End Sub

    '''<summary>
    '''A test for FieldInt64
    '''</summary>
    <TestMethod()> _
    Public Sub FieldInt64Test()

      Dim actual As Int64 = _reader.FieldInt64()
      Dim expected As Int64 = longValue
      Assert.AreEqual(expected, actual, "Structured.Reader.FieldInt64: Expected '{0}' ?= '{1}'", expected, actual)

    End Sub

    '''<summary>
    '''A test for FieldSingle
    '''</summary>
    <TestMethod()> _
    Public Sub FieldSingleTest()

      Dim actual As Single = _reader.FieldSingle()
      Dim expected As Single = singleValue
      Assert.AreEqual(expected, actual, "Structured.Reader.FieldSingle: Expected '{0}' ?= '{1}'", expected, actual)

    End Sub

    '''<summary>
    '''A test for FieldString
    '''</summary>
    <TestMethod()> _
    Public Sub FieldStringTest()

      Dim actual As String = _reader.FieldString()
      Dim expected As String = stringValue
      Assert.AreEqual(expected, actual, "Structured.Reader.FieldString: Expected '{0}' ?= '{1}'", expected, actual)

    End Sub

    '''<summary>
    '''A test for FromOADate
    '''</summary>
    <TestMethod()> _
    Public Sub FromOADate()

      Dim actual As DateTime = Date.FromOADate(_reader.FieldDouble())
      Dim expected As DateTime = dateValue
      Assert.AreEqual(expected, actual, "Structured.Reader.FromOADate: Expected '{0}' ?= '{1}'", expected, actual)

    End Sub

    '''<summary>
    '''A test for ReadFields
    '''</summary>
    <TestMethod()> _
    Public Sub ReadFieldsTest()
      Assert.IsTrue(_reader.ReadFields, "Structured.Reader.ReadFields from: {0} ", _reader.FilePathName)
    End Sub
  End Class

End Namespace

