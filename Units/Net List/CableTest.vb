Imports System
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualStudio.TestTools.UnitTesting

''' <summary>
''' Contains all unit tests for the <see cref="isr.IO.NetList.Cable">Cable</see> class.
''' </summary>
''' <remarks></remarks>
''' <license>
''' (c) 2007 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="12/12/07" by="David Hary" revision="6.0.2902.x">
''' Created
''' </history>
<TestClass()> Public Class CableTest

  Private _testContext As TestContext

  '''<summary>
  '''Gets or sets the test context which provides
  '''information about and functionality for the current test run.
  '''</summary>
  Public Property TestContext() As TestContext
    Get
      Return _testContext
    End Get
    Set(ByVal value As TestContext)
      _testContext = value
    End Set
  End Property

#Region " ADDITIONAL TEST ATTRIBUTED METHODS "

#Region " UNUSED "
#If False Then

  ''' <summary>
  ''' Runs code before running the first test in the class.
  ''' Tests the connection to the module reader and instantiates 
  ''' a module reader module level elements.
  ''' </summary>
  ''' <param name="testContext"></param>
  ''' <remarks></remarks>
  <ClassInitialize()> _
  Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
  End Sub

  ''' <summary>
  ''' Runs code after all tests in a class have run.
  ''' </summary>
  ''' <remarks></remarks>
  <ClassCleanup()> _
  Public Shared Sub MyClassCleanup()
  End Sub

  ''' <summary>
  ''' Runs code before running each test
  ''' </summary>
  ''' <remarks></remarks>
  <TestInitialize()> _
  Public Sub MyTestInitialize()
  End Sub

  ''' <summary>
  ''' Runs code after each test has run.
  ''' </summary>
  ''' <remarks></remarks>
  <TestCleanup()> _
  Public Sub MyTestCleanup()
  End Sub

#End If
#End Region

#End Region

  ''' <summary>
  ''' Test reading DB9 null modem cable.
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GenericCableTest(ByVal cableName As String, ByVal filePathName As String, _
                               ByVal expectedConnectorsCount As Integer, _
                               ByVal expectedNodesCount As Integer, _
                               ByVal expectedPinsCount As Integer, _
                               ByVal expectedWiresCount As Integer)

    Dim cable As New isr.IO.NetList.Cable

    Do
      Dim actual As Boolean = cable.ReadNetList(filePathName)
      Assert.IsTrue(actual, "Failed reading cable {0} from {1}", cableName, filePathName)
    Loop Until True

    Do
      Dim actual As Boolean = cable.PopulateWires(True)
      Assert.IsTrue(actual, "Failed populating cable {0} wires", cableName)
    Loop Until True

    Do
      Dim actualConnectorsCount As Integer = cable.Connectors.Count
      Assert.AreEqual(expectedConnectorsCount, actualConnectorsCount, _
                      "Cable {0} Connectors count: Expected '{0}' ?= '{1}'", cableName, expectedConnectorsCount, actualConnectorsCount)
    Loop Until True

    Do
      Dim actualNodesCount As Integer = cable.Nodes.Count
      Assert.AreEqual(expectedNodesCount, actualNodesCount, _
                      "Cable {0} Nodes count: Expected '{0}' ?= '{1}'", cableName, expectedNodesCount, actualNodesCount)
    Loop Until True

    Do
      Dim actualPinsCount As Integer = cable.pins.Count
      Assert.AreEqual(expectedPinsCount, actualPinsCount, _
                      "Cable {0} Pins count: Expected '{0}' ?= '{1}'", cableName, expectedPinsCount, actualPinsCount)
    Loop Until True

    Do
      Dim actualWiresCount As Integer = cable.Wires.Count
      Assert.AreEqual(expectedWiresCount, actualWiresCount, _
                      "Cable {0} Wires count: Expected '{0}' ?= '{1}'", cableName, expectedWiresCount, actualWiresCount)
    Loop Until True

  End Sub

  ''' <summary>
  ''' Test reading DB9 null modem cable.
  ''' </summary>
  ''' <remarks></remarks>
  <TestMethod(), Description("Reads 5 Connector Cable")> Public Sub FiveConnectorCableTest()

    Dim filePathName As String = "C:\My\Projects\Sandia\CITA 3.0\Data\Nets\5 CONNECTOR CABLE.NET"
    GenericCableTest("5CONN", filePathName, 5, 26, 67, 41)

  End Sub

  ''' <summary>
  ''' Test reading DB9 null modem cable.
  ''' </summary>
  ''' <remarks></remarks>
  <TestMethod(), Description("Reads DB9 Cable")> Public Sub DB9CableTest()

    Dim filePathName As String = "C:\My\Projects\Sandia\CITA 3.0\Data\Nets\DE-9 PIN TO PIN CABLE.NET"
    GenericCableTest("DB9", filePathName, 2, 9, 18, 9)
#If False Then
    Dim cable As New isr.IO.NetList.Cable

    Do
      Dim actual As Boolean = cable.ReadNetList(filePathName)
      Assert.IsTrue(actual, "Failed reading cable {0}", filePathName)
    Loop Until True

    Do
      Dim actual As Boolean = cable.PopulateWires()
      Assert.IsTrue(actual, "Failed populating wires")
    Loop Until True

    Do
      Dim actualConnectorsCount As Integer = cable.Connectors.Count
      Dim expectedConnectorsCount As Integer = 2
      Assert.AreEqual(expectedConnectorsCount, actualConnectorsCount, "Connectors count: Expected '{0}' ?= '{1}'", expectedConnectorsCount, actualConnectorsCount)
    Loop Until True

    Do
      Dim actualNodesCount As Integer = cable.Nodes.Count
      Dim expectedNodesCount As Integer = 9
      Assert.AreEqual(expectedNodesCount, actualNodesCount, "Nodes count: Expected '{0}' ?= '{1}'", expectedNodesCount, actualNodesCount)
    Loop Until True

    Do
      Dim actualPinsCount As Integer = cable.pins.Count
      Dim expectedPinsCount As Integer = 18
      Assert.AreEqual(expectedPinsCount, actualPinsCount, "Pins count: Expected '{0}' ?= '{1}'", expectedPinsCount, actualPinsCount)
    Loop Until True

    Do
      Dim actualWiresCount As Integer = cable.Wires.Count
      Dim expectedWiresCount As Integer = 9
      Assert.AreEqual(expectedWiresCount, actualWiresCount, "Wires count: Expected '{0}' ?= '{1}'", expectedWiresCount, actualWiresCount)
    Loop Until True
#End If

  End Sub

End Class
