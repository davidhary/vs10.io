Namespace Structured

    ''' <summary>
    ''' Writes delimited files.
    ''' </summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="04/20/06" by="David Hary" revision="1.1.2301.x">
    ''' created.
    ''' </history>
    Public Class Writer

        Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        Public Sub New()

            ' instantiate the base class
            Me.New(Reader.DefaultFilePathName)

        End Sub

        ''' <summary>Constructs this class.</summary>
        ''' <param name="filePathName">Specifies the file path name.</param>
        Public Sub New(ByVal filePathName As String)

            ' instantiate the base class
            MyBase.New()
            _filePathName = filePathName

        End Sub

        ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        ''' <remarks>Do not make this method overridable (virtual) because a derived 
        '''   class should not be able to override this method.</remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        Private _isDisposed As Boolean
        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
        ''' provided proper implementation.
        ''' </summary>
        Protected Property IsDisposed() As Boolean
            Get
                Return _isDisposed
            End Get
            Private Set(ByVal value As Boolean)
                _isDisposed = value
            End Set
        End Property

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                Try

                    If disposing Then

                        ' Free managed resources when explicitly called

                    End If

                    ' Free shared unmanaged resources

                Finally

                    ' set the sentinel indicating that the class was disposed.
                    Me.IsDisposed = True

                End Try

            End If

        End Sub

        ''' <summary>This destructor will run only if the Dispose method 
        '''   does not get called. It gives the base class the opportunity to 
        '''   finalize. Do not provide destructors in types derived from this class.</summary>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal in terms of
            ' readability and maintainability.
            Dispose(False)
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Sub

#End Region

#Region " FILE METHODS AND PROPERTIES "

        Private _hasFieldsEnclosedInQuotes As Boolean
        ''' <summary>
        ''' Holds true if fields are to be enclosed in quotes.
        ''' </summary>
        Public Property HasFieldsEnclosedInQuotes() As Boolean
            Get
                Return _hasFieldsEnclosedInQuotes
            End Get
            Set(ByVal value As Boolean)
                _hasFieldsEnclosedInQuotes = value
            End Set
        End Property

        Private _filePathName As String = String.Empty
        ''' <summary>Gets or sets the file name.</summary>
        ''' <value><c>FilePathName</c> is a String property.</value>
        ''' <remarks>Use this property to get or set the file name.</remarks>
        Public Property FilePathName() As String
            Get
                If _filePathName.Length = 0 Then
                    ' set default file name if empty.
                    _filePathName = Reader.DefaultExtension
                End If
                Return _filePathName
            End Get
            Set(ByVal value As String)
                _filePathName = value
            End Set
        End Property

#End Region

#Region " FIELD METHODS AND PROPERTIES "

        ''' <summary>
        ''' Adds a "T:String" value to the current record.
        ''' </summary>
        ''' <param name="value">Specifies the value to add to the record.</param>
        Public Function AddField(ByVal value As String) As String
            Me.Fields.Add(value)
            Return value
        End Function

        ''' <summary>
        ''' Adds a "T:Boolean" value to the current record.
        ''' </summary>
        ''' <param name="value">Specifies the value to add to the record.</param>
        Public Function AddField(ByVal value As Boolean) As String
            Return Me.AddField(value.ToString(Globalization.CultureInfo.CurrentCulture))
        End Function

        ''' <summary>
        ''' Adds a "T:Byte" value to the current record.
        ''' </summary>
        ''' <param name="value">Specifies the value to add to the record.</param>
        Public Function AddField(ByVal value As Byte) As String
            Return Me.AddField(value.ToString(Globalization.CultureInfo.CurrentCulture))
        End Function

        ''' <summary>
        ''' Adds a "T:Byte" value to the current record.
        ''' </summary>
        ''' <param name="value">Specifies the value to add to the record.</param>
        ''' <param name="format">Specifies the format to use when expressing the value.</param>
        Public Function AddField(ByVal value As Byte, ByVal format As String) As String
            Return Me.AddField(value.ToString(format, Globalization.CultureInfo.CurrentCulture))
        End Function

        ''' <summary>
        ''' Adds a "T:DateTime" value to the current record.
        ''' </summary>
        ''' <param name="value">Specifies the value to add to the record.</param>
        Public Function AddField(ByVal value As DateTime) As String
            Return Me.AddField(value.ToString(Globalization.CultureInfo.CurrentCulture))
        End Function

        ''' <summary>
        ''' Adds a "T:DateTime" value to the current record.
        ''' </summary>
        ''' <param name="value">Specifies the value to add to the record.</param>
        ''' <param name="format">Specifies the format to use when expressing the value.</param>
        Public Function AddField(ByVal value As DateTime, ByVal format As String) As String
            Return Me.AddField(value.ToString(format, Globalization.CultureInfo.CurrentCulture))
        End Function

        ''' <summary>
        ''' Adds a "T:Decimal" value to the current record.
        ''' </summary>
        ''' <param name="value">Specifies the value to add to the record.</param>
        Public Function AddField(ByVal value As Decimal) As String
            Return Me.AddField(value.ToString(Globalization.CultureInfo.CurrentCulture))
        End Function

        ''' <summary>
        ''' Adds a "T:Decimal" value to the current record.
        ''' </summary>
        ''' <param name="value">Specifies the value to add to the record.</param>
        ''' <param name="format">Specifies the format to use when expressing the value.</param>
        Public Function AddField(ByVal value As Decimal, ByVal format As String) As String
            Return Me.AddField(value.ToString(format, Globalization.CultureInfo.CurrentCulture))
        End Function

        ''' <summary>
        ''' Adds a "T:Double" value to the current record.
        ''' </summary>
        ''' <param name="value">Specifies the value to add to the record.</param>
        Public Function AddField(ByVal value As Double) As String
            Return Me.AddField(value.ToString(Globalization.CultureInfo.CurrentCulture))
        End Function

        ''' <summary>
        ''' Adds a "T:Double" value to the current record.
        ''' </summary>
        ''' <param name="value">Specifies the value to add to the record.</param>
        ''' <param name="format">Specifies the format to use when expressing the value.</param>
        Public Function AddField(ByVal value As Double, ByVal format As String) As String
            Return Me.AddField(value.ToString(format, Globalization.CultureInfo.CurrentCulture))
        End Function

        ''' <summary>
        ''' Adds a "T:Int64" value to the current record.
        ''' </summary>
        ''' <param name="value">Specifies the value to add to the record.</param>
        Public Function AddField(ByVal value As Int64) As String
            Return Me.AddField(value.ToString(Globalization.CultureInfo.CurrentCulture))
        End Function

        ''' <summary>
        ''' Adds a "T:Int64" value to the current record.
        ''' </summary>
        ''' <param name="value">Specifies the value to add to the record.</param>
        ''' <param name="format">Specifies the format to use when expressing the value.</param>
        Public Function AddField(ByVal value As Int64, ByVal format As String) As String
            Return Me.AddField(value.ToString(format, Globalization.CultureInfo.CurrentCulture))
        End Function

        ''' <summary>
        ''' Adds a "T:Single" value to the current record.
        ''' </summary>
        ''' <param name="value">Specifies the value to add to the record.</param>
        Public Function AddField(ByVal value As Single) As String
            Return Me.AddField(value.ToString(Globalization.CultureInfo.CurrentCulture))
        End Function

        ''' <summary>
        ''' Adds a "T:Single" value to the current record.
        ''' </summary>
        ''' <param name="value">Specifies the value to add to the record.</param>
        ''' <param name="format">Specifies the format to use when expressing the value.</param>
        Public Function AddField(ByVal value As Single, ByVal format As String) As String
            Return Me.AddField(value.ToString(format, Globalization.CultureInfo.CurrentCulture))
        End Function

#End Region

#Region " FIELDS METHODS AND PROPERTIES "

        Private _delimiter As String = ","
        ''' <summary>
        ''' Holds the file delimiter
        ''' </summary>
        Public Property Delimiter() As String
            Get
                Return _delimiter
            End Get
            Set(ByVal value As String)
                _delimiter = value
            End Set
        End Property

        Private _fields As ArrayList
        ''' <summary>
        ''' Holds "T:ArrayList" of field values.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Fields() As ArrayList
            If _fields Is Nothing Then
                _fields = New ArrayList
            End If
            Return Me._fields
        End Function

        ''' <summary>
        ''' Builds the current record for saving to the file.
        ''' </summary>
        ''' <param name="newLine">Specifies if the record is being appedned as a new line.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function BuildRecord(ByVal newLine As Boolean) As String
            Dim record As New Text.StringBuilder
            For Each field As String In Me.Fields
                If record.Length > 0 Then
                    record.Append(Me._delimiter)
                End If
                If Me._hasFieldsEnclosedInQuotes Then
                    record.AppendFormat(Globalization.CultureInfo.CurrentCulture, """{0}""", field)
                Else
                    record.Append(field)
                End If
            Next
            If newLine Then
                record.Insert(0, Environment.NewLine)
            End If
            Return record.ToString
        End Function

        Private _isNew As Boolean
        ''' <summary>
        ''' Tag the next write as creating a new file.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub NewFile()
            _isNew = True
        End Sub

        ''' <summary>
        ''' Creates a new record.
        ''' </summary>
        Public Sub NewRecord()
            _fields = New ArrayList
        End Sub

        ''' <summary>
        ''' Writes the current fields record to the file.
        ''' </summary>
        Public Function WriteFields() As Boolean

            ' append to he file if it exists and not requesting a new file.
            Dim append As Boolean = (Not _isNew) AndAlso My.Computer.FileSystem.FileExists(Me.FilePathName)
            My.Computer.FileSystem.WriteAllText(Me.FilePathName, Me.BuildRecord(append), append)
            ' toggle the new flag so as the append from now on.
            _isNew = False
        End Function

#End Region

    End Class

End Namespace

