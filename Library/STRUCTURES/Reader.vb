Namespace Structured

    ''' <summary>
    ''' Reads delimited files.
    ''' </summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="04/20/06" by="David Hary" revision="1.1.2301.x">
    ''' created.
    ''' </history>
    Public Class Reader

        Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        Public Sub New()

            ' instantiate the base class
            Me.New(Reader.DefaultFilePathName)

        End Sub

        ''' <summary>Constructs this class.</summary>
        ''' <param name="filePathName">Specifies the file path name.</param>
        Public Sub New(ByVal filePathName As String)

            ' instantiate the base class
            MyBase.New()
            _filePathName = filePathName

            ' open the parser
            Me._parser = New Microsoft.VisualBasic.FileIO.TextFieldParser(Me._filePathName)
            Me._parser.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited
            Me._parser.Delimiters = New String() {Me._delimiter}

        End Sub

        ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        ''' <remarks>Do not make this method overridable (virtual) because a derived 
        '''   class should not be able to override this method.</remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        Private _isDisposed As Boolean
        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
        ''' provided proper implementation.
        ''' </summary>
        Protected Property IsDisposed() As Boolean
            Get
                Return _isDisposed
            End Get
            Private Set(ByVal value As Boolean)
                _isDisposed = value
            End Set
        End Property

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                Try

                    If disposing Then

                        ' Free managed resources when explicitly called
                        If Me._parser IsNot Nothing Then
                            Me._parser.Close()
                            Me._parser.Dispose()
                            Me._parser = Nothing
                        End If

                    End If

                    ' Free shared unmanaged resources

                Finally

                    ' set the sentinel indicating that the class was disposed.
                    Me.IsDisposed = True

                End Try

            End If

        End Sub

        ''' <summary>This destructor will run only if the Dispose method 
        '''   does not get called. It gives the base class the opportunity to 
        '''   finalize. Do not provide destructors in types derived from this class.</summary>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal in terms of
            ' readability and maintainability.
            Dispose(False)
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Sub

#End Region

#Region " SHARED "

        ''' <summary>
        ''' Holds the default extension.
        ''' </summary>
        ''' <remarks></remarks>
        Public Const DefaultExtension As String = ".csv"

        ''' <summary>
        ''' Returns the default file name for the structured I/O.  This would be
        ''' the executabel file name appended with the 
        ''' <see cref="DefaultExtension">default extension</see>.
        ''' </summary>
        Public Shared ReadOnly Property DefaultFilePathName() As String
            Get
                Return Windows.Forms.Application.ExecutablePath & DefaultExtension
            End Get
        End Property

        ''' <summary>
        ''' Return file rows as a jagged array allowing each row
        ''' to be of different length.
        ''' </summary>
        Public Shared Function ReadAllRows(ByVal filePathName As String) As String()()

            Dim data()() As String = {}
            Using MyReader As New Microsoft.VisualBasic.FileIO.TextFieldParser(filePathName)

                MyReader.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited
                MyReader.Delimiters = New String() {","}
                'MyReader.HasFieldsEnclosedInQuotes'
                Dim currentRow As String()
                'Loop through all of the fields in the file. 
                'If any lines are corrupt, report an error and continue parsing. 
                While Not MyReader.EndOfData
                    currentRow = MyReader.ReadFields()
                    If (currentRow IsNot Nothing) AndAlso currentRow.Length > 0 Then
                        ReDim data(data.GetLength(0))(currentRow.Length - 1)
                        data(data.GetUpperBound(0)) = currentRow
                    End If
                End While

            End Using

            Return data

        End Function

#End Region

#Region " FILE METHODS AND PROPERTIES "

        Private _filePathName As String = String.Empty
        ''' <summary>Gets or sets the file name.</summary>
        ''' <value><c>FilePathName</c> is a String property.</value>
        ''' <remarks>Use this property to get or set the file name.</remarks>
        Public Property FilePathName() As String
            Get
                If _filePathName.Length = 0 Then
                    ' set default file name if empty.
                    _filePathName = Reader.DefaultExtension
                End If
                Return _filePathName
            End Get
            Set(ByVal value As String)
                _filePathName = value
            End Set
        End Property

#End Region

#Region " FIELD METHODS AND PROPERTIES "

        Private _fieldIndex As Integer
        ''' <summary>
        ''' Specifies the field index from which to read a field value.
        ''' Resets to zero upon reading a new record.
        ''' </summary>
        Public Property FieldIndex() As Integer
            Get
                Return _fieldIndex
            End Get
            Set(ByVal value As Integer)
                _fieldIndex = value
            End Set
        End Property

        ''' <summary>
        ''' Returns the field for the specified <paramref name="index">index</paramref>.
        ''' </summary>
        ''' <param name="index">Specifies the index in the fields record from which
        ''' to get the value.</param>
        Public Function FieldString(ByVal index As Integer) As String
            If index < 0 OrElse index >= Me._fields.Length Then
                Throw New ArgumentOutOfRangeException("index", index, _
                    String.Format(Globalization.CultureInfo.CurrentCulture, "Must be positive value less than {0}", Me._fields.Length))
            End If
            Return Me._fields(index)
        End Function

        ''' <summary>
        ''' Returns the field for the current <see cref="FieldIndex">index</see>.
        ''' Increments the current field index.
        ''' </summary>
        Public Function FieldString() As String
            FieldString = FieldString(Me._fieldIndex)
            Me._fieldIndex += 1
        End Function

        ''' <summary>
        ''' Returns a "T:Boolean" field value for the specified field <paramref name="index">index</paramref>.
        ''' </summary>
        ''' <param name="index">Specifies the index in the fields record from which
        ''' to get the value.</param>
        Public Function FieldBoolean(ByVal index As Integer) As Boolean
            Return Boolean.Parse(Me.FieldString(index))
        End Function

        ''' <summary>
        ''' Returns a "T:Boolean" field value for the current <see cref="FieldIndex">index</see>.
        ''' Increments the current field index.
        ''' </summary>
        Public Function FieldBoolean() As Boolean
            FieldBoolean = Me.FieldBoolean(Me._fieldIndex)
            Me._fieldIndex += 1
        End Function

        ''' <summary>
        ''' Returns a "T:Byte" field value for the specified field <paramref name="index">index</paramref>.
        ''' </summary>
        ''' <param name="index">Specifies the index in the fields record from which
        ''' to get the value.</param>
        Public Function FieldByte(ByVal index As Integer) As Byte
            Return Byte.Parse(Me.FieldString(index), Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture)
        End Function

        ''' <summary>
        ''' Returns a "T:Byte" field value for the current <see cref="FieldIndex">index</see>.
        ''' Increments the current field index.
        ''' </summary>
        Public Function FieldByte() As Byte
            FieldByte = Me.FieldByte(Me._fieldIndex)
            Me._fieldIndex += 1
        End Function

        ''' <summary>
        ''' Returns a "T:Date" field value for the specified field <paramref name="index">index</paramref>.
        ''' </summary>
        ''' <param name="index">Specifies the index in the fields record from which
        ''' to get the value.</param>
        Public Function FieldDate(ByVal index As Integer) As Date
            Return Date.Parse(Me.FieldString(index), Globalization.CultureInfo.CurrentCulture)
        End Function

        ''' <summary>
        ''' Returns a "T:Date" field value for the current <see cref="FieldIndex">index</see>.
        ''' Increments the current field index.
        ''' </summary>
        Public Function FieldDate() As Date
            FieldDate = Me.FieldDate(Me._fieldIndex)
            Me._fieldIndex += 1
        End Function

        ''' <summary>
        ''' Returns a "T:DateTime" field value for the specified field <paramref name="index">index</paramref>.
        ''' </summary>
        ''' <param name="index">Specifies the index in the fields record from which
        ''' to get the value.</param>
        Public Function FieldDateTime(ByVal index As Integer) As DateTime
            Return DateTime.Parse(Me.FieldString(index), Globalization.CultureInfo.CurrentCulture)
        End Function

        ''' <summary>
        ''' Returns a "T:DateTime" field value for the current <see cref="FieldIndex">index</see>.
        ''' Increments the current field index.
        ''' </summary>
        Public Function FieldDateTime() As DateTime
            FieldDateTime = Me.FieldDateTime(Me._fieldIndex)
            Me._fieldIndex += 1
        End Function

        ''' <summary>
        ''' Returns a "T:Decimal" field value for the specified field <paramref name="index">index</paramref>.
        ''' </summary>
        ''' <param name="index">Specifies the index in the fields record from which
        ''' to get the value.</param>
        Public Function FieldDecimal(ByVal index As Integer) As Decimal
            Return Decimal.Parse(Me.FieldString(index), Globalization.NumberStyles.Float, Globalization.CultureInfo.CurrentCulture)
        End Function

        ''' <summary>
        ''' Returns a "T:Decimal" field value for the current <see cref="FieldIndex">index</see>.
        ''' Increments the current field index.
        ''' </summary>
        Public Function FieldDecimal() As Decimal
            FieldDecimal = Me.FieldDecimal(Me._fieldIndex)
            Me._fieldIndex += 1
        End Function

        ''' <summary>
        ''' Returns a "T:Double" field value for the specified field <paramref name="index">index</paramref>.
        ''' </summary>
        ''' <param name="index">Specifies the index in the fields record from which
        ''' to get the value.</param>
        Public Function FieldDouble(ByVal index As Integer) As Double
            Return Double.Parse(Me.FieldString(index), Globalization.NumberStyles.Float, Globalization.CultureInfo.CurrentCulture)
        End Function

        ''' <summary>
        ''' Returns a "T:Double" field value for the current <see cref="FieldIndex">index</see>.
        ''' Increments the current field index.
        ''' </summary>
        Public Function FieldDouble() As Double
            FieldDouble = Me.FieldDouble(Me._fieldIndex)
            Me._fieldIndex += 1
        End Function

        ''' <summary>
        ''' Returns a "T:Int16" field value for the specified field <paramref name="index">index</paramref>.
        ''' </summary>
        ''' <param name="index">Specifies the index in the fields record from which
        ''' to get the value.</param>
        Public Function FieldInt16(ByVal index As Integer) As Int16
            Return Int16.Parse(Me.FieldString(index), Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture)
        End Function

        ''' <summary>
        ''' Returns a "T:Int16" field value for the current <see cref="FieldIndex">index</see>.
        ''' Increments the current field index.
        ''' </summary>
        Public Function FieldInt16() As Int16
            FieldInt16 = Me.FieldInt16(Me._fieldIndex)
            Me._fieldIndex += 1
        End Function

        ''' <summary>
        ''' Returns a "T:Int32" field value for the specified field <paramref name="index">index</paramref>.
        ''' </summary>
        ''' <param name="index">Specifies the index in the fields record from which
        ''' to get the value.</param>
        Public Function FieldInt32(ByVal index As Integer) As Int32
            Return Int32.Parse(Me.FieldString(index), Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture)
        End Function

        ''' <summary>
        ''' Returns a "T:Int32" field value for the current <see cref="FieldIndex">index</see>.
        ''' Increments the current field index.
        ''' </summary>
        Public Function FieldInt32() As Int32
            FieldInt32 = Me.FieldInt32(Me._fieldIndex)
            Me._fieldIndex += 1
        End Function

        ''' <summary>
        ''' Returns a "T:Int64" field value for the specified field <paramref name="index">index</paramref>.
        ''' </summary>
        ''' <param name="index">Specifies the index in the fields record from which
        ''' to get the value.</param>
        Public Function FieldInt64(ByVal index As Integer) As Int64
            Return Int64.Parse(Me.FieldString(index), Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture)
        End Function

        ''' <summary>
        ''' Returns a "T:Int64" field value for the current <see cref="FieldIndex">index</see>.
        ''' Increments the current field index.
        ''' </summary>
        Public Function FieldInt64() As Int64
            FieldInt64 = Me.FieldInt64(Me._fieldIndex)
            Me._fieldIndex += 1
        End Function

        ''' <summary>
        ''' Returns a "T:Single" field value for the specified field <paramref name="index">index</paramref>.
        ''' </summary>
        ''' <param name="index">Specifies the index in the fields record from which
        ''' to get the value.</param>
        Public Function FieldSingle(ByVal index As Integer) As Single
            Return Single.Parse(Me.FieldString(index), Globalization.NumberStyles.Float, Globalization.CultureInfo.CurrentCulture)
        End Function

        ''' <summary>
        ''' Returns a "T:Single" field value for the current <see cref="FieldIndex">index</see>.
        ''' Increments the current field index.
        ''' </summary>
        Public Function FieldSingle() As Single
            FieldSingle = Me.FieldSingle(Me._fieldIndex)
            Me._fieldIndex += 1
        End Function

#End Region

#Region " FIELDS METHODS AND PROPERTIES "

        Private _fields As String() = {}
        ''' <summary>
        ''' Holds the last record that was read from the file.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Fields() As String()
            Return Me._fields
        End Function

        ''' <summary>
        ''' Reads th next record and returns an array of doubles.
        ''' </summary>
        Public Function ReadFieldsDouble() As Double()

            If Me.ReadFields() Then

                ' allocate data array
                Dim data(Me._fields.Length - 1) As Double

                For i As Int32 = 0 To Me._fields.Length - 1
                    data(i) = Me.FieldDouble(i)
                Next
                Return data

            Else

                ' return the empty array 
                Dim data() As Double = {}
                Return data

            End If

        End Function

        ''' <summary>
        ''' Return file rows as a jagged two-dimensional array allowing each row
        ''' to be of different length.
        ''' </summary>
        Public Function ReadRowFieldsDouble() As Double()()

            Dim data()() As Double = {}
            Dim rowIndex As Int32
            Do While Not Me._parser.EndOfData
                Dim oneDimensionValues As Double() = ReadFieldsDouble()
                If (oneDimensionValues IsNot Nothing) AndAlso oneDimensionValues.Length > 0 Then
                    ReDim data(rowIndex)(oneDimensionValues.Length - 1)
                    For i As Int32 = 0 To Math.Min(data.GetUpperBound(0), oneDimensionValues.Length - 1)
                        data(rowIndex)(i) = oneDimensionValues(i)
                    Next
                End If
                rowIndex += 1
            Loop
            Return data

        End Function

        ''' <summary>
        ''' Returns True if the parser read fields from the file.
        ''' </summary>
        Public ReadOnly Property HasFields() As Boolean
            Get
                Return Me._fields IsNot Nothing AndAlso Me._fields.Length > 0
            End Get
        End Property

        ''' <summary>
        ''' Reads a record of fields from the file.
        ''' </summary>
        ''' <returns>True if has new data or false it end of file or no data.</returns>
        ''' <remarks></remarks>
        Public Function ReadFields() As Boolean

            Me._fieldIndex = 0
            Me._fields = New String() {}
            If Not Me._parser.EndOfData Then
                Me._fields = Me._parser.ReadFields()
            End If
            Return Me.HasFields

        End Function

#End Region

#Region " READER METHODS AND PROPERTIES "

        ''' <summary>
        ''' Returns true if the parser is open.
        ''' </summary>
        ''' <value><c>IsOpen</c>Is a Boolean property</value>
        Public ReadOnly Property IsOpen() As Boolean
            Get
                Return _parser IsNot Nothing
            End Get
        End Property

        Private _parser As Microsoft.VisualBasic.FileIO.TextFieldParser
        ''' <summary>
        ''' Holds reference to the 
        ''' <see cref="Microsoft.VisualBasic.FileIO.TextFieldParser">text field parser</see>
        ''' for reading the file.
        ''' </summary>
        Public ReadOnly Property Parser() As Microsoft.VisualBasic.FileIO.TextFieldParser
            Get
                Return _parser
            End Get
        End Property

        Private _delimiter As String = ","
        ''' <summary>
        ''' Holds the file delimiter
        ''' </summary>
        Public Property Delimiter() As String
            Get
                Return _delimiter
            End Get
            Set(ByVal value As String)
                _delimiter = value
            End Set
        End Property

#End Region

    End Class

End Namespace
