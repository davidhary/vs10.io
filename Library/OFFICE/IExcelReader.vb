Namespace Office

    ''' <summary>
    ''' Defines an interface for reading excel workbooks.
    ''' </summary>
    ''' <license>
    ''' (c) 2009 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="01/07/09" by="David Hary" revision="1.2.3294.x">
    ''' created.
    ''' </history>
    Public Interface IExcelReader

        Inherits IDisposable

        ''' <summary>
        ''' Gets the name of the last file that was read.
        ''' </summary>
        ''' <remarks></remarks>
        ReadOnly Property FilePathName() As String

        ''' <summary>
        ''' Reads all worksheets from the Excel file.
        ''' </summary>
        ''' <param name="filePathName"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function ReadWorkbook(ByVal filePathName As String) As Boolean

        ''' <summary>
        ''' Returns the name of the currently selected worksheet.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        ReadOnly Property CurrentWorksheetName() As String

        ''' <summary>
        ''' Selects a workseet by name.
        ''' </summary>
        ''' <param name="name"></param>
        ''' <remarks></remarks>
        Function ActivateWorksheet(ByVal name As String) As Boolean

        ''' <summary>
        ''' Returns a cell value from the work sheet or DB NUll.
        ''' </summary>
        ''' <param name="row">Specifies a row number</param>
        ''' <param name="col">Specifies a column number</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function CellValueGetter(ByVal row As Integer, ByVal col As Integer) As Object

        ''' <summary>Returns the value of the given cell as a nullable Boolean type.</summary>
        ''' <param name="row">Specifies a row number</param>
        ''' <param name="col">Specifies a column number</param>
        ''' <param name="defaultValue">Specifies a default value</param>
        Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Boolean?) As Boolean?

        ''' <summary>Returns the value of the specified cell as a Boolean type.</summary>
        ''' <param name="row">Specifies a row number</param>
        ''' <param name="col">Specifies a column number</param>
        ''' <param name="defaultValue">Specifies a default value</param>
        Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Boolean) As Boolean

        ''' <summary>Returns the value of the specified cell as a nullable Date type.</summary>
        ''' <param name="row">Specifies a row number</param>
        ''' <param name="col">Specifies a column number</param>
        ''' <param name="defaultValue">Specifies a default value</param>
        Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As DateTime?) As DateTime?

        ''' <summary>Returns the value of the specified cell as a Date type.</summary>
        ''' <param name="row">Specifies a row number</param>
        ''' <param name="col">Specifies a column number</param>
        ''' <param name="defaultValue">Specifies a default value</param>
        Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As DateTime) As DateTime

        ''' <summary>Returns the value of the specified cell as a nullable Double type.</summary>
        ''' <param name="row">Specifies a row number</param>
        ''' <param name="col">Specifies a column number</param>
        ''' <param name="defaultValue">Specifies a default value</param>
        Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Double?) As Double?

        ''' <summary>Returns the value of the specified cell as a Double type.</summary>
        ''' <param name="row">Specifies a row number</param>
        ''' <param name="col">Specifies a column number</param>
        ''' <param name="defaultValue">Specifies a default value</param>
        Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Double) As Double

        ''' <summary>Returns the value of the specified cell as a Nullable Integer type.</summary>
        ''' <param name="row">Specifies a row number</param>
        ''' <param name="col">Specifies a column number</param>
        ''' <param name="defaultValue">Specifies a default value</param>
        Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Integer?) As Integer?

        ''' <summary>Returns the value of the specified cell as a Integer type.</summary>
        ''' <param name="row">Specifies a row number</param>
        ''' <param name="col">Specifies a column number</param>
        ''' <param name="defaultValue">Specifies a default value</param>
        Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Integer) As Integer

        ''' <summary>
        ''' Returns a cell value or default value of String type.
        ''' </summary>
        ''' <param name="row">Specifies a row number</param>
        ''' <param name="col">Specifies a column number</param>
        ''' <param name="defaultValue">Specifies a default value</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As String) As String

        ''' <summary>
        ''' Gets the left Column.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        ReadOnly Property CurrentWorksheetUsedRangeColumnNumber() As Integer

        ''' <summary>
        ''' Gets the top row.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        ReadOnly Property CurrentWorksheetUsedRangeRowNumber() As Integer

        ''' <summary>Returns true if the specified cell includes a DBNull value.</summary>
        ''' <param name="row">Specifies a row number</param>
        ''' <param name="col">Specifies a column number</param>
        Function IsDBNull(ByVal row As Integer, ByVal col As Integer) As Boolean

        ''' <summary>Returns true if the specified cell is empty.</summary>
        ''' <param name="row">Specifies a row number</param>
        ''' <param name="col">Specifies a column number</param>
        Function IsEmpty(ByVal row As Integer, ByVal col As Integer) As Boolean

        ''' <summary>Returns true if the specified cell is empty or includes a DBNull value.</summary>
        ''' <param name="row">Specifies a row number</param>
        ''' <param name="col">Specifies a column number</param>
        Function IsDBNullOrEmpty(ByVal row As Integer, ByVal col As Integer) As Boolean

    End Interface

End Namespace
