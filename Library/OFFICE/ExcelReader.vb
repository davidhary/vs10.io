Imports System.IO
Imports System.Data

Namespace Office

    ''' <summary>Handles access to Excel spreadsheets using an <see cref="IExcelReader">Excel Data Reader.</see>.</summary>
    ''' <license>
    ''' (c) 2009 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="01/07/09" by="David Hary" revision="1.2.3294.x">
    ''' created.
    ''' </history>
    Public Class ExcelReader

        Implements IExcelReader

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        Public Sub New()

            MyBase.New()

        End Sub

        ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        ''' <remarks>Do not make this method overridable (virtual) because a derived 
        '''   class should not be able to override this method.</remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        Private _isDisposed As Boolean
        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
        ''' provided proper implementation.
        ''' </summary>
        Protected Property IsDisposed() As Boolean
            Get
                Return _isDisposed
            End Get
            Private Set(ByVal value As Boolean)
                _isDisposed = value
            End Set
        End Property

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                Try

                    If disposing Then

                        ' Free managed resources when explicitly called
                        If _workbook IsNot Nothing Then
                            _workbook.Dispose()
                            _workbook = Nothing
                        End If

                        If _worksheet IsNot Nothing Then
                            _worksheet.Dispose()
                            _worksheet = Nothing
                        End If

                    End If

                    ' Free unmanaged resources

                Finally

                    ' set the sentinel indicating that the class was disposed.
                    Me.IsDisposed = True

                End Try

            End If

        End Sub

        ''' <summary>This destructor will run only if the Dispose method 
        '''   does not get called. It gives the base class the opportunity to 
        '''   finalize. Do not provide destructors in types derived from this class.</summary>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal in terms of
            ' readability and maintainability.
            Dispose(False)
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Sub

#End Region

#Region " EXCEL READER "

        Private _filePathName As String
        ''' <summary>
        ''' Gets the name of the last file that was read.
        ''' </summary>
        ''' <remarks></remarks>
        Public ReadOnly Property FilePathName() As String Implements IExcelReader.FilePathName
            Get
                Return _filePathName
            End Get
        End Property

        ''' <summary>
        ''' Reads all worksheets from the Excel file.
        ''' </summary>
        ''' <param name="filePathName"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overridable Function ReadWorkbook(ByVal filePathName As String) As Boolean Implements IExcelReader.ReadWorkbook

            ' dispose of previous data
            If _workbook IsNot Nothing Then
                _workbook.Dispose()
                _workbook = Nothing
            End If

            ' dispose of the worksheet
            If _worksheet IsNot Nothing Then
                _worksheet.Dispose()
                _worksheet = Nothing
            End If

            ' clear the file name
            _filePathName = ""

            If String.IsNullOrEmpty(filePathName) Then
                Throw New ArgumentNullException("filePathName", "failed reading workbook because file name was not specified.")
            End If
            If Not System.IO.File.Exists(filePathName) Then
                Throw New IOException("failed reading workbook because the workbook file {0} was not found.", filePathName)
            End If

            _filePathName = filePathName

            ' read the worksheets using OLE DB.
            _workbook = isr.IO.Office.ExcelImport.ImportWorkbook(_filePathName, False)

            Return True

        End Function

        Private _workbook As DataSet
        ''' <summary>
        ''' Gets reference to the workbook dataset.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Workbook() As DataSet
            Get
                Return _workbook
            End Get
        End Property

#End Region

#Region " WORKSHEET "

        Private _worksheet As DataTable
        ''' <summary>
        ''' Gets reference to the worksheet table representing the last selected work sheet.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Worksheet() As DataTable
            Get
                Return _worksheet
            End Get
        End Property

        ''' <summary>
        ''' Returns the name of the currently selected worksheet.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property CurrentWorksheetName() As String Implements IExcelReader.CurrentWorksheetName
            Get
                If _worksheet Is Nothing Then
                    Return ""
                Else
                    Return _worksheet.TableName
                End If
            End Get
        End Property

        ''' <summary>
        ''' Selects a workseet by name.
        ''' </summary>
        ''' <param name="name"></param>
        ''' <remarks></remarks>
        Public Overridable Function ActivateWorksheet(ByVal name As String) As Boolean Implements IExcelReader.ActivateWorksheet

            If _workbook Is Nothing Then
                Throw New BaseException("failed activating worksheet because workbook was not read from the Excel file.")
            End If

            ' Select the specified sheet
            If Not String.IsNullOrEmpty(Me.CurrentWorksheetName) _
              AndAlso Me.CurrentWorksheetName.Equals(name, StringComparison.OrdinalIgnoreCase) Then
                Return True
            Else
                Try
                    _worksheet = _workbook.Tables(name)
                Finally
                End Try
                If String.IsNullOrEmpty(Me.CurrentWorksheetName) Then
                    Throw New BaseException("failed activating worksheet {0} in {1}", name, _filePathName)
                    Return False
                Else
                    Return True
                End If
            End If

        End Function

        ''' <summary>
        ''' Gets the left Column.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property CurrentWorksheetUsedRangeColumnNumber() As Integer Implements IExcelReader.CurrentWorksheetUsedRangeColumnNumber
            Get
                Return 1
            End Get
        End Property

        ''' <summary>
        ''' Gets the top row.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property CurrentWorksheetUsedRangeRowNumber() As Integer Implements IExcelReader.CurrentWorksheetUsedRangeRowNumber
            Get
                Return 1
            End Get
        End Property

#End Region

#Region " GETTERS "

        ''' <summary>
        ''' Returns a cell value from the work sheet.
        ''' </summary>
        ''' <param name="row">Specifies a one-based row number</param>
        ''' <param name="col">Specifies a one-based column number</param>
        ''' <returns>Cell value or DB Null if column or row indexes exceed the number of rows or columns in the worksheet.
        ''' This allows reading values that are generally thought to be defined in a worksheet but where not year defined
        ''' in the particular worksheet and thus might be expected to be given a default value. The default values are derived in
        ''' the relevant getters.  Otherwise, a DB Null is returned.</returns>
        ''' <remarks>Excel rows and columns are one-based.</remarks>
        Public Function CellValueGetter(ByVal row As Integer, ByVal col As Integer) As Object Implements IExcelReader.CellValueGetter
            If _worksheet Is Nothing Then
                Throw New IOException("Worksheet not activated")
            End If
            If row <= 0 Then
                Throw New ArgumentOutOfRangeException("row", "row number must be a positive")
            End If
            If col <= 0 Then
                Throw New ArgumentOutOfRangeException("col", "column number must be positive")
            End If
            If row <= _worksheet.Rows.Count AndAlso col <= _worksheet.Columns.Count Then
                Return isr.IO.Parser.Parse(_worksheet.Rows(row - 1).Item(col - 1))
            Else
                Return System.DBNull.Value
            End If
        End Function

        ''' <summary>Returns the value of the given cell as a nullable Boolean type.</summary>
        ''' <param name="row">Specifies a one-based row number</param>
        ''' <param name="col">Specifies a one-based column number</param>
        ''' <param name="defaultValue">Specifies a default value</param>
        ''' <remarks>Excel rows and columns are one-based.</remarks>
        Public Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Boolean?) As Boolean? Implements IExcelReader.CellValueGetter

            If _worksheet Is Nothing Then
                Throw New IOException("Worksheet not activated")
            End If
            Return isr.IO.Parser.Parse(CellValueGetter(row, col), defaultValue)

        End Function

        ''' <summary>Returns the value of the specified cell as a Boolean type.</summary>
        ''' <param name="row">Specifies a one-based row number</param>
        ''' <param name="col">Specifies a one-based column number</param>
        ''' <param name="defaultValue">Specifies a default value</param>
        ''' <remarks>Excel rows and columns are one-based.</remarks>
        Public Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Boolean) As Boolean Implements IExcelReader.CellValueGetter
            Return CellValueGetter(row, col, New Boolean?(defaultValue)).Value
        End Function

        ''' <summary>Returns the value of the specified cell as a nullable Date type.</summary>
        ''' <param name="row">Specifies a one-based row number</param>
        ''' <param name="col">Specifies a one-based column number</param>
        ''' <param name="defaultValue">Specifies a default value</param>
        ''' <remarks>Excel rows and columns are one-based.</remarks>
        Public Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As DateTime?) As DateTime? Implements IExcelReader.CellValueGetter

            Dim value As Double? = CellValueGetter(row, col, New Double?)
            If value.HasValue Then
                Return DateTime.FromOADate(value.Value)
            Else
                Return defaultValue
            End If

        End Function

        ''' <summary>Returns the value of the specified cell as a datetime type.</summary>
        ''' <param name="row">Specifies a one-based row number</param>
        ''' <param name="col">Specifies a one-based column number</param>
        ''' <param name="defaultValue">Specifies a default value</param>
        ''' <remarks>Excel rows and columns are one-based.</remarks>
        Public Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As DateTime) As DateTime Implements IExcelReader.CellValueGetter
            Return CellValueGetter(row, col, New DateTime?(defaultValue)).Value
        End Function

        ''' <summary>Returns the value of the specified cell as a nullable Double type.</summary>
        ''' <param name="row">Specifies a one-based row number</param>
        ''' <param name="col">Specifies a one-based column number</param>
        ''' <param name="defaultValue">Specifies a default value</param>
        ''' <remarks>Excel rows and columns are one-based.</remarks>
        Public Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Double?) As Double? Implements IExcelReader.CellValueGetter

            If _worksheet Is Nothing Then
                Throw New IOException("Worksheet not activated")
            End If
            Return isr.IO.Parser.Parse(CellValueGetter(row, col), defaultValue)

        End Function

        ''' <summary>Returns the value of the specified cell as a double type.</summary>
        ''' <param name="row">Specifies a one-based row number</param>
        ''' <param name="col">Specifies a one-based column number</param>
        ''' <param name="defaultValue">Specifies a default value</param>
        ''' <remarks>Excel rows and columns are one-based.</remarks>
        Public Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Double) As Double Implements IExcelReader.CellValueGetter
            Return CellValueGetter(row, col, New Double?(defaultValue)).Value
        End Function

        ''' <summary>Returns the value of the specified cell as a Nullable Integer type.</summary>
        ''' <param name="row">Specifies a one-based row number</param>
        ''' <param name="col">Specifies a one-based column number</param>
        ''' <param name="defaultValue">Specifies a default value</param>
        ''' <remarks>Excel rows and columns are one-based.</remarks>
        Public Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Integer?) As Integer? Implements IExcelReader.CellValueGetter

            Dim value As Double? = CellValueGetter(row, col, New Double?)
            If value.HasValue Then
                Return Convert.ToInt32(value.Value)
            Else
                Return defaultValue
            End If

        End Function

        ''' <summary>Returns the value of the specified cell as a Integer type.</summary>
        ''' <param name="row">Specifies a one-based row number</param>
        ''' <param name="col">Specifies a one-based column number</param>
        ''' <param name="defaultValue">Specifies a default value</param>
        ''' <remarks>Excel rows and columns are one-based.</remarks>
        Public Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As Integer) As Integer Implements IExcelReader.CellValueGetter
            Return CellValueGetter(row, col, New Integer?(defaultValue)).Value
        End Function

        ''' <summary>
        ''' Returns a cell value or default value of String type.
        ''' </summary>
        ''' <param name="row">Specifies a one-based row number</param>
        ''' <param name="col">Specifies a one-based column number</param>
        ''' <param name="defaultValue">Specifies a default value</param>
        ''' <returns></returns>
        ''' <remarks>Excel rows and columns are one-based.</remarks>
        Public Function CellValueGetter(ByVal row As Integer, ByVal col As Integer, ByVal defaultValue As String) As String Implements IExcelReader.CellValueGetter
            If _worksheet Is Nothing Then
                Throw New IOException("Worksheet not activated")
            End If
            Return isr.IO.Parser.Parse(CellValueGetter(row, col), defaultValue)
        End Function

        ''' <summary>Returns true if the specified cell includes a DBNull value.</summary>
        ''' <param name="row">Specifies a one-based row number</param>
        ''' <param name="col">Specifies a one-based column number</param>
        ''' <remarks>Excel rows and columns are one-based.</remarks>
        Public Function IsDBNull(ByVal row As Integer, ByVal col As Integer) As Boolean Implements IExcelReader.IsDBNull

            If _worksheet Is Nothing Then
                Throw New IOException("Worksheet not activated")
            End If
            Return isr.IO.Parser.IsDBNull(CellValueGetter(row, col))

        End Function

        ''' <summary>Returns true if the specified cell is empty.</summary>
        ''' <param name="row">Specifies a one-based row number</param>
        ''' <param name="col">Specifies a one-based column number</param>
        ''' <remarks>Excel rows and columns are one-based.</remarks>
        Public Function IsEmpty(ByVal row As Integer, ByVal col As Integer) As Boolean Implements IExcelReader.IsEmpty
            If _worksheet Is Nothing Then
                Throw New IOException("Worksheet not activated")
            End If
            Return isr.IO.Parser.IsEmpty(CellValueGetter(row, col))
        End Function

        ''' <summary>Returns true if the specified cell is empty or includes a DBNull value.</summary>
        ''' <param name="row">Specifies a one-based row number</param>
        ''' <param name="col">Specifies a one-based column number</param>
        ''' <remarks>Excel rows and columns are one-based.</remarks>
        Public Function IsDBNullOrEmpty(ByVal row As Integer, ByVal col As Integer) As Boolean Implements IExcelReader.IsDBNullOrEmpty
            Return Me.IsDBNull(row, col) OrElse Me.IsEmpty(row, col)
        End Function

#End Region

    End Class

End Namespace
