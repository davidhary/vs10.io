''' <summary>Extends the <see cref="System.IO.BinaryReader">system binary reader</see> class.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="03/07/2006" by="David Hary" revision="1.0.2257.x">
''' created.
''' </history>
Public Class BinaryReader

    ' based on the system binary reader
    Inherits System.IO.BinaryReader

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="filePathName">Specifies the name of the binary file which to read.</param>
    ''' <remarks>Use this constructor to open a binary reader for the specified file.</remarks>
    Public Sub New(ByVal filePathName As String)

        ' instantiate the base class
        MyBase.New(New System.IO.FileStream(filePathName, System.IO.FileMode.Open, System.IO.FileAccess.Read))
        _filePathName = filePathName

    End Sub

    Private _isDisposed As Boolean
    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return _isDisposed
        End Get
        Private Set(ByVal value As Boolean)
            _isDisposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        If Not Me.IsDisposed Then

            Try

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources

            Finally

                ' set the sentinel indicating that the class was disposed.
                Me.IsDisposed = True

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End If

    End Sub

#End Region

#Region " METHODS  AND  PROPERTIES "

    ''' <summary>Closes the binary reader.</summary>
    ''' <exception cref="OperationCloseException" guarantee="strong"></exception>
    ''' <remarks>Use this method to close the instance.  The method returns true if success or 
    '''   false if it failed closing the instance.</remarks>
    Public Overrides Sub [Close]()

        ' Close the file.
        Dim fs As System.IO.FileStream = CType(MyBase.BaseStream, System.IO.FileStream)
        MyBase.Close()
        If fs IsNot Nothing Then
            fs.Close()
            fs = Nothing
        End If

    End Sub

    Private _filePathName As String = String.Empty
    ''' <summary>Gets or sets the file name.</summary>
    ''' <value><c>FilePathName</c> is a String property.</value>
    ''' <remarks>Use this property to get or set the file name.</remarks>
    Public ReadOnly Property FilePathName() As String
        Get
            Return _filePathName
        End Get
    End Property

    ''' <summary>Opens a binary file for reading and returns a reference to 
    '''   the reader. The file is <see cref="System.IO.FileMode.Open">opened</see> in 
    '''   <see cref="System.IO.FileAccess.Read">read access</see>.
    ''' </summary>
    ''' <param name="filePathName">Specifies the file name.</param>
    ''' <returns>A reference to an open <see cref="IO.BinaryReader">binary reader</see>.</returns>
    Public Shared Function OpenBinaryReader(ByVal filePathName As String) As isr.IO.BinaryReader

        If filePathName Is Nothing Then
            Throw New ArgumentNullException("filePathName")
        End If

        If Not System.IO.File.Exists(filePathName) Then
            Dim message As String = "Failed opening a binary reader -- file not found."
            Throw New System.IO.FileNotFoundException(message, filePathName)
        End If

        Return New isr.IO.BinaryReader(filePathName)

    End Function

    ''' <summary>Reads a single-dimension <see cref="Double">double-precision</see>
    '''   array from the data file.</summary>
    ''' <remarks>Returns values as an array.</remarks>
    Public Overloads Function ReadDoubleArray() As Double()

        ' read the stored length
        Dim storedLength As Int32 = MyBase.ReadInt32

        If storedLength < 0 Then
            Dim message As String = "Corrupt file.  Program encountered a negative array length of {0}."
            message = String.Format(Globalization.CultureInfo.CurrentCulture, message, storedLength)
            Throw New isr.IO.BaseException(message)
        End If

        Return Me.ReadDoubleArray(storedLength)

    End Function

    ''' <summary>Reads a single-dimension <see cref="Double">double-precision</see>
    '''   array from the data file.</summary>
    ''' <param name="elementCount">Specifies the number of data points.</param>
    ''' <remarks>Returns values as an array.</remarks>
    Public Overloads Function ReadDoubleArray(ByVal elementCount As Int32) As Double()

        If elementCount < 0 Then
            Dim message As String = "Array size specified as {0} must be non-negative."
            message = String.Format(Globalization.CultureInfo.CurrentCulture, message, elementCount)
            Throw New ArgumentOutOfRangeException("elementCount", elementCount, message)
        End If

        If elementCount = 0 Then

            ' return the empty array 
            Dim data() As Double = {}
            Return data

        Else
            ' allocate data array
            Dim data(elementCount - 1) As Double

            ' Read the voltage from the file
            For sampleNumber As Int32 = 0 To elementCount - 1
                data(sampleNumber) = MyBase.ReadDouble
            Next sampleNumber
            Return data
        End If

    End Function

    ''' <summary>Reads a single-dimension <see cref="Double">double-precision</see>
    '''   array from the data file.</summary>
    ''' <param name="elementCount">Specifies the number of data points.</param>
    ''' <param name="verifyLength">If true, verifies the length againts the given length.</param>
    ''' <remarks>Returns values as an array.</remarks>
    Public Overloads Function ReadDoubleArray(ByVal elementCount As Int32, _
        ByVal verifyLength As Boolean) As Double()

        If verifyLength Then
            Dim data() As Double = Me.ReadDoubleArray()
            If data.Length <> elementCount Then
                Dim message As String = "Data length stored in file of {0} elements does not match the expected data length of {1} elements."
                message = String.Format(Globalization.CultureInfo.CurrentCulture, message, data.Length, elementCount)
                Throw New ArgumentOutOfRangeException("elementCount", elementCount, message)
            Else
                Return data
            End If
        Else
            Return Me.ReadDoubleArray()
        End If

    End Function

    ''' <summary>Reads a single-dimension <see cref="Double">double-precision</see> 
    '''   array from the data file.</summary>
    ''' <param name="count">Specifies the number of data points.</param>
    ''' <param name="startIndex">Specifies the index of the first data point.</param>
    ''' <param name="stepSize">Specifies the step size between adjacent data points.</param>
    ''' <remarks>Returns values as an array.</remarks>
    Public Overloads Function ReadDouble(ByVal count As Int32, _
      ByVal startIndex As Int32, ByVal stepSize As Int32) As Double()

        ' read the stored length
        Dim storedLength As Int32 = MyBase.ReadInt32

        If storedLength <> count Then
            Dim message As String = "Data length stored in file of {0} elements does not match the expected data length of {1} elements."
            message = String.Format(Globalization.CultureInfo.CurrentCulture, message, storedLength, count)
            Throw New ArgumentOutOfRangeException("count", count, message)
        End If

        ' allocate data array
        Dim data(count - 1) As Double

        ' skip samples to get to the first channel of this sample set.
        If startIndex > 0 Then
            For i As Integer = 1 To startIndex
                MyBase.ReadDouble()
            Next
        End If
        ' Read the voltage from the file
        For sampleNumber As Int32 = 0 To count - 1
            data(sampleNumber) = MyBase.ReadDouble
            If stepSize > 1 Then
                For i As Integer = 2 To stepSize
                    MyBase.ReadDouble()
                Next
            End If

        Next sampleNumber

        Return data

    End Function

    ''' <summary>Reads a double precision value from the data file.</summary>
    ''' <param name="location">Specifies the file location.</param>
    ''' <returns>A double.</returns>
    Public Overloads Function ReadDouble(ByVal location As Int64) As Double

        MyBase.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
        Return MyBase.ReadDouble()

    End Function

    ''' <summary>Reads a single-dimension <see cref="Int32">Integer</see>
    '''   array from the data file.</summary>
    ''' <remarks>Returns values as an array.</remarks>
    Public Overloads Function ReadInt32Array() As Int32()

        ' read the stored length
        Dim storedLength As Int32 = MyBase.ReadInt32

        If storedLength < 0 Then
            Dim message As String = "Corrupt file.  Program encountered a negative array length of {0}."
            message = String.Format(Globalization.CultureInfo.CurrentCulture, message, storedLength)
            Throw New isr.IO.BaseException(message)
        End If

        Return Me.ReadInt32Array(storedLength)

    End Function

    ''' <summary>Reads a single-dimension <see cref="Int32">Integer</see>
    '''   array from the data file.</summary>
    ''' <param name="elementCount">Specifies the number of data points.</param>
    ''' <remarks>Returns values as an array.</remarks>
    Public Overloads Function ReadInt32Array(ByVal elementCount As Int32) As Int32()

        If elementCount < 0 Then
            Dim message As String = "Array size must be non-negative {0}."
            message = String.Format(Globalization.CultureInfo.CurrentCulture, message, elementCount)
            Throw New ArgumentOutOfRangeException("elementCount", elementCount, message)
        End If

        If elementCount = 0 Then

            ' return the empty array 
            Dim data() As Int32 = {}
            Return data

        Else
            ' allocate data array
            Dim data(elementCount - 1) As Int32

            ' Read the voltage from the file
            For sampleNumber As Int32 = 0 To elementCount - 1
                data(sampleNumber) = MyBase.ReadInt32
            Next sampleNumber
            Return data
        End If

    End Function

    ''' <summary>Reads a single-dimension <see cref="Int32">Integer</see>
    '''   array from the data file.</summary>
    ''' <param name="elementCount">Specifies the number of data points.</param>
    ''' <param name="verifyLength">If true, verifies the length againts the given length.</param>
    ''' <remarks>Returns values as an array.</remarks>
    Public Overloads Function ReadInt32Array(ByVal elementCount As Int32, _
        ByVal verifyLength As Boolean) As Int32()

        If verifyLength Then
            Dim data() As Int32 = Me.ReadInt32Array()
            If data.Length <> elementCount Then
                Dim message As String = "Data length stored in file of {0} elements does not match the expected data length of {1} elements."
                message = String.Format(Globalization.CultureInfo.CurrentCulture, message, data.Length, elementCount)
                Throw New ArgumentOutOfRangeException("elementCount", elementCount, message)
            Else
                Return data
            End If
        Else
            Return Me.ReadInt32Array()
        End If

    End Function

    ''' <summary>Reads a single-dimension <see cref="Int32">Integer</see> 
    '''   array from the data file.</summary>
    ''' <param name="count">Specifies the number of data points.</param>
    ''' <param name="startIndex">Specifies the index of the first data point.</param>
    ''' <param name="stepSize">Specifies the step size between adjacent data points.</param>
    ''' <remarks>Returns values as an array.</remarks>
    Public Overloads Function ReadInt32(ByVal count As Int32, _
      ByVal startIndex As Int32, ByVal stepSize As Int32) As Int32()

        ' read the stored length
        Dim storedLength As Int32 = MyBase.ReadInt32

        If storedLength <> count Then
            Dim message As String = "Data length stored in file of {0} elements does not match the expected data length of {1} elements."
            message = String.Format(Globalization.CultureInfo.CurrentCulture, message, storedLength, count)
            Throw New ArgumentOutOfRangeException("count", count, message)
        End If

        ' allocate data array
        Dim data(count - 1) As Int32

        ' skip samples to get to the first channel of this sample set.
        If startIndex > 0 Then
            For i As Integer = 1 To startIndex
                MyBase.ReadInt32()
            Next
        End If
        ' Read the voltage from the file
        For sampleNumber As Int32 = 0 To count - 1
            data(sampleNumber) = MyBase.ReadInt32
            If stepSize > 1 Then
                For i As Integer = 2 To stepSize
                    MyBase.ReadInt32()
                Next
            End If

        Next sampleNumber

        Return data

    End Function

    ''' <summary>Reads an Int32 value from the data file.</summary>
    ''' <param name="location">Specifies the file location.</param>
    ''' <returns>A double.</returns>
    Public Overloads Function ReadInt32(ByVal location As Int64) As Int32

        MyBase.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
        Return MyBase.ReadInt32()

    End Function

    ''' <summary>Reads a <see cref="Int64">long integer</see> 
    '''     long-integer array from the data file.</summary>
    ''' <remarks>Returns values as an array.</remarks>
    Public Overloads Function ReadInt64Array() As Int64()

        ' read the stored length
        Dim storedLength As Int32 = MyBase.ReadInt32

        If storedLength < 0 Then
            Dim message As String = "Corrupt file.  Program encountered a negative array length of {0}."
            message = String.Format(Globalization.CultureInfo.CurrentCulture, message, storedLength)
            Throw New isr.IO.BaseException(message)
        End If

        Return Me.ReadInt64Array(storedLength)

    End Function

    ''' <summary>Reads a <see cref="Int64">long integer</see> 
    '''     long integer array from the data file.</summary>
    ''' <param name="elementCount">Specifies the number of data points.</param>
    ''' <remarks>Returns values as an array.</remarks>
    Public Overloads Function ReadInt64Array(ByVal elementCount As Int32) As Int64()

        If elementCount < 0 Then
            Dim message As String = "Array size must be non-negative {0}."
            message = String.Format(Globalization.CultureInfo.CurrentCulture, message, elementCount)
            Throw New ArgumentOutOfRangeException("elementCount", elementCount, message)
        End If

        If elementCount = 0 Then

            ' return the empty array 
            Dim data() As Int64 = {}
            Return data

        Else
            ' allocate data array
            Dim data(elementCount - 1) As Int64

            ' Read the voltage from the file
            For sampleNumber As Int32 = 0 To elementCount - 1
                data(sampleNumber) = MyBase.ReadInt64
            Next sampleNumber
            Return data
        End If

    End Function

    ''' <summary>Reads a <see cref="Int64">long integer</see> 
    '''     long integer array from the data file.</summary>
    ''' <param name="elementCount">Specifies the number of data points.</param>
    ''' <param name="verifyLength">If true, verifies the length againts the given length.</param>
    ''' <remarks>Returns values as an array.</remarks>
    Public Overloads Function ReadInt64Array(ByVal elementCount As Int32, _
        ByVal verifyLength As Boolean) As Int64()

        If verifyLength Then
            Dim data() As Int64 = Me.ReadInt64Array()
            If data.Length <> elementCount Then
                Dim message As String = "Data length stored in file of {0} elements does not match the expected data length of {1} elements."
                message = String.Format(Globalization.CultureInfo.CurrentCulture, message, data.Length, elementCount)
                Throw New ArgumentOutOfRangeException("elementCount", elementCount, message)
            Else
                Return data
            End If
        Else
            Return Me.ReadInt64Array()
        End If

    End Function

    ''' <summary>Reads a single-dimension <see cref="Int64">Long Integer</see> 
    '''   array from the data file.</summary>
    ''' <param name="count">Specifies the number of data points.</param>
    ''' <param name="startIndex">Specifies the index of the first data point.</param>
    ''' <param name="stepSize">Specifies the step size between adjacent data points.</param>
    ''' <remarks>Returns values as an array.</remarks>
    Public Overloads Function ReadInt64(ByVal count As Int32, _
      ByVal startIndex As Int32, ByVal stepSize As Int32) As Int64()

        ' read the stored length
        Dim storedLength As Int32 = MyBase.ReadInt32

        If storedLength <> count Then
            Dim message As String = "Data length stored in file of {0} elements does not match the expected data length of {1} elements."
            message = String.Format(Globalization.CultureInfo.CurrentCulture, message, storedLength, count)
            Throw New ArgumentOutOfRangeException("count", count, message)
        End If

        ' allocate data array
        Dim data(count - 1) As Int64

        ' skip samples to get to the first channel of this sample set.
        If startIndex > 0 Then
            For i As Integer = 1 To startIndex
                MyBase.ReadInt64()
            Next
        End If
        ' Read the voltage from the file
        For sampleNumber As Int32 = 0 To count - 1
            data(sampleNumber) = MyBase.ReadInt64
            If stepSize > 1 Then
                For i As Integer = 2 To stepSize
                    MyBase.ReadInt64()
                Next
            End If

        Next sampleNumber

        Return data

    End Function

    ''' <summary>Reads a Int64 value from the data file.</summary>
    ''' <param name="location">Specifies the file location.</param>
    ''' <returns>A double.</returns>
    Public Overloads Function ReadInt64(ByVal location As Int64) As Int64

        MyBase.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
        Return MyBase.ReadInt64()

    End Function

    ''' <summary>Reads a single-dimension <see cref="Single">single-precision</see>
    '''   array from the data file.</summary>
    ''' <remarks>Returns values as an array.</remarks>
    Public Overloads Function ReadSingleArray() As Single()

        ' read the stored length
        Dim storedLength As Int32 = MyBase.ReadInt32

        If storedLength < 0 Then
            Dim message As String = "Corrupt file.  Program encountered a negative array length of {0}."
            message = String.Format(Globalization.CultureInfo.CurrentCulture, message, storedLength)
            Throw New isr.IO.BaseException(message)
        End If

        Return Me.ReadSingleArray(storedLength)

    End Function

    ''' <summary>Reads a single-dimension <see cref="Single">single-precision</see>
    '''   array from the data file.</summary>
    ''' <param name="elementCount">Specifies the number of data points.</param>
    ''' <remarks>Returns values as an array.</remarks>
    Public Overloads Function ReadSingleArray(ByVal elementCount As Int32) As Single()

        If elementCount < 0 Then
            Dim message As String = "Array size must be non-negative {0}."
            message = String.Format(Globalization.CultureInfo.CurrentCulture, message, elementCount)
            Throw New ArgumentOutOfRangeException("elementCount", elementCount, message)
        End If

        If elementCount = 0 Then

            ' return the empty array 
            Dim data() As Single = {}
            Return data

        Else
            ' allocate data array
            Dim data(elementCount - 1) As Single

            ' Read the voltage from the file
            For sampleNumber As Int32 = 0 To elementCount - 1
                data(sampleNumber) = MyBase.ReadSingle
            Next sampleNumber
            Return data
        End If

    End Function

    ''' <summary>Reads a single-dimension <see cref="Single">single-precision</see>
    '''   array from the data file.</summary>
    ''' <param name="elementCount">Specifies the number of data points.</param>
    ''' <param name="verifyLength">If true, verifies the length againts the given length.</param>
    ''' <remarks>Returns values as an array.</remarks>
    Public Overloads Function ReadSingleArray(ByVal elementCount As Int32, _
        ByVal verifyLength As Boolean) As Single()

        If verifyLength Then
            Dim data() As Single = Me.ReadSingleArray()
            If data.Length <> elementCount Then
                Dim message As String = "Data length stored in file of {0} elements does not match the expected data length of {1} elements."
                message = String.Format(Globalization.CultureInfo.CurrentCulture, message, data.Length, elementCount)
                Throw New ArgumentOutOfRangeException("elementCount", elementCount, message)
            Else
                Return data
            End If
        Else
            Return Me.ReadSingleArray()
        End If

    End Function

    ''' <summary>Reads a single-dimension <see cref="Single">single-precision</see> 
    '''   array from the data file.</summary>
    ''' <param name="count">Specifies the number of data points.</param>
    ''' <param name="startIndex">Specifies the index of the first data point.</param>
    ''' <param name="stepSize">Specifies the step size between adjacent data points.</param>
    ''' <remarks>Returns values as an array.</remarks>
    Public Overloads Function ReadSingle(ByVal count As Int32, _
      ByVal startIndex As Int32, ByVal stepSize As Int32) As Single()

        ' read the stored length
        Dim storedLength As Int32 = MyBase.ReadInt32

        If storedLength <> count Then
            Dim message As String = "Data length stored in file of {0} elements does not match the expected data length of {1} elements."
            message = String.Format(Globalization.CultureInfo.CurrentCulture, message, storedLength, count)
            Throw New ArgumentOutOfRangeException("count", count, message)
        End If

        ' allocate data array
        Dim data(count - 1) As Single

        ' skip samples to get to the first channel of this sample set.
        If startIndex > 0 Then
            For i As Integer = 1 To startIndex
                MyBase.ReadSingle()
            Next
        End If
        ' Read the voltage from the file
        For sampleNumber As Int32 = 0 To count - 1
            data(sampleNumber) = MyBase.ReadSingle
            If stepSize > 1 Then
                For i As Integer = 2 To stepSize
                    MyBase.ReadSingle()
                Next
            End If

        Next sampleNumber

        Return data

    End Function

    ''' <summary>Reads a single precision value from the data file.</summary>
    ''' <param name="location">Specifies the file location.</param>
    ''' <returns>A single.</returns>
    Public Overloads Function ReadSingle(ByVal location As Int64) As Single

        MyBase.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
        Return MyBase.ReadSingle()

    End Function

    ''' <summary>Reads a string value from the data file.</summary>
    ''' <param name="location">Specifies the file location.</param>
    ''' <returns>A double.</returns>
    Public Overloads Function ReadString(ByVal location As Int64) As String

        MyBase.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
        Return MyBase.ReadString()

    End Function

#End Region

End Class

