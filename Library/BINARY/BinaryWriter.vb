''' <summary>Extends the <see cref="System.IO.BinaryWriter">system binary reader</see> class.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="03/07/2006" by="David Hary" revision="1.0.2257.x">
''' created.
''' </history>
Public Class BinaryWriter

    Inherits System.IO.BinaryWriter

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="filePathName">Specifies the file name.</param>
    ''' <param name="fileMode">Specifies the <see cref="System.IO.FileMode">file mode</see>.
    '''   This would <see cref="System.IO.FileMode.CreateNew">Create New</see> to create
    '''   a new file, <see cref="System.IO.FileMode.OpenOrCreate">create or open an exisitng</see> 
    '''   file or <see cref="System.IO.FileMode.Append">append</see>.
    '''   to open and position for appending to the file.</param>
    ''' <remarks>Use this constructor to open a binary reader for the specified file.</remarks>
    Public Sub New(ByVal filePathName As String, ByVal fileMode As System.IO.FileMode)

        ' instantiate the base class
        MyBase.New(New System.IO.FileStream(filePathName, fileMode))
        _filePathName = filePathName

    End Sub

    Private _isDisposed As Boolean
    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return _isDisposed
        End Get
        Private Set(ByVal value As Boolean)
            _isDisposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        If Not Me.IsDisposed Then

            Try

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources

            Finally

                ' set the sentinel indicating that the class was disposed.
                Me.IsDisposed = True

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End If


    End Sub

#End Region

#Region " METHODS  AND  PROPERTIES "

    ''' <summary>Closes the binary reader.</summary>
    ''' <exception cref="OperationCloseException" guarantee="strong"></exception>
    ''' <remarks>Use this method to close the instance.  The method returns true if success or 
    '''   false if it failed closing the instance.</remarks>
    Public Overrides Sub [Close]()

        ' Close the file.
        Dim fs As System.IO.FileStream = CType(MyBase.BaseStream, System.IO.FileStream)
        MyBase.Close()
        If fs IsNot Nothing Then
            fs.Close()
            fs = Nothing
        End If

    End Sub

    Private _filePathName As String = String.Empty
    ''' <summary>Gets or sets the file name.</summary>
    ''' <value><c>FilePathName</c> is a String property.</value>
    ''' <remarks>Use this property to get or set the file name.</remarks>
    Public ReadOnly Property FilePathName() As String
        Get
            Return _filePathName
        End Get
    End Property

    ''' <summary>Writes a single-dimension <see cref="Double">double-precision</see> 
    '''   array to the data file.</summary>
    ''' <param name="data">Holds the values to write.</param>
    ''' <remarks>Returns values as an array.</remarks>
    Public Overloads Sub Write(ByVal data() As Double)

        If data Is Nothing Then
            Throw New ArgumentNullException("data")
        End If

        ' wirte the data size for verifying size and, indirectly also location, upon read
        MyBase.Write(Convert.ToInt32(data.Length))

        ' write values to the file
        For i As Int32 = 0 To data.GetUpperBound(0)
            MyBase.Write(data(i))
        Next i

    End Sub

    ''' <summary>Writes a single-dimension <see cref="Int32">Integer</see> 
    '''   array to the data file.</summary>
    ''' <param name="data">Holds the values to write.</param>
    ''' <remarks>Returns values as an array.</remarks>
    Public Overloads Sub Write(ByVal data() As Int32)

        If data Is Nothing Then
            Throw New ArgumentNullException("data")
        End If

        ' wirte the data size for verifying size and, indirectly also location, upon read
        MyBase.Write(Convert.ToInt32(data.Length))

        ' write values to the file
        For i As Int32 = 0 To data.GetUpperBound(0)
            MyBase.Write(data(i))
        Next i

    End Sub

    ''' <summary>Writes a single-dimension <see cref="Int64">Long Integer</see> 
    '''   array to the data file.</summary>
    ''' <param name="data">Holds the values to write.</param>
    ''' <remarks>Returns values as an array.</remarks>
    Public Overloads Sub Write(ByVal data() As Int64)

        If data Is Nothing Then
            Throw New ArgumentNullException("data")
        End If

        ' wirte the data size for verifying size and, indirectly also location, upon read
        MyBase.Write(Convert.ToInt32(data.Length))

        ' write values to the file
        For i As Int32 = 0 To data.GetUpperBound(0)
            MyBase.Write(data(i))
        Next i

    End Sub

    ''' <summary>Writes a single-dimension <see cref="Single">single-precision</see> 
    '''   array to the data file.</summary>
    ''' <param name="data">Holds the values to write.</param>
    ''' <remarks>Returns values as an array.</remarks>
    Public Overloads Sub Write(ByVal data() As Single)

        If data Is Nothing Then
            Throw New ArgumentNullException("data")
        End If

        ' wirte the data size for verifying size and, indirectly also location, upon read
        MyBase.Write(Convert.ToInt32(data.Length))

        ' write values to the file
        For i As Int32 = 0 To data.GetUpperBound(0)
            MyBase.Write(data(i))
        Next i

    End Sub

    ''' <summary>Writes a string to the binary file padding it with spaces as necessary
    '''   to fill the length.</summary>
    ''' <param name="data">Holds the values to write.</param>
    ''' <param name="length"></param>
    Public Overloads Sub Write(ByVal data As String, ByVal length As Int32)

        If data Is Nothing Then
            Throw New ArgumentNullException("data")
        End If

        ' pad but make sure not to exceed length.
        MyBase.Write(data.PadRight(length).Substring(0, length))

    End Sub

    ''' <summary>Writes a double-precision value to the data file.</summary>
    ''' <param name="value">Specifies the value to write.</param>
    ''' <param name="location">Specifies the file location.</param>
    ''' <remarks>Returns values as an array.</remarks>
    Public Overloads Sub Write(ByVal value As Double, ByVal location As Int64)

        MyBase.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
        MyBase.Write(value)

    End Sub

    ''' <summary>Writes an Int32 value to the data file.</summary>
    ''' <param name="value">Specifies the value to write.</param>
    ''' <param name="location">Specifies the file location.</param>
    ''' <remarks>Returns values as an array.</remarks>
    Public Overloads Sub Write(ByVal value As Int32, ByVal location As Int64)

        MyBase.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
        MyBase.Write(value)

    End Sub

    ''' <summary>Writes a Int64 value to the data file.</summary>
    ''' <param name="value">Specifies the value to write.</param>
    ''' <param name="location">Specifies the file location.</param>
    ''' <remarks>Returns values as an array.</remarks>
    Public Overloads Sub Write(ByVal value As Int64, ByVal location As Int64)

        MyBase.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
        MyBase.Write(value)

    End Sub

    ''' <summary>Writes a single-precision value to the data file.</summary>
    ''' <param name="value">Specifies the value to write.</param>
    ''' <param name="location">Specifies the file location.</param>
    ''' <remarks>Returns values as an array.</remarks>
    Public Overloads Sub Write(ByVal value As Single, ByVal location As Int64)

        MyBase.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
        MyBase.Write(value)

    End Sub

    ''' <summary>Writes a string value to the data file.</summary>
    ''' <param name="value">Specifies the value to write.</param>
    ''' <param name="location">Specifies the file location.</param>
    ''' <remarks>Returns values as an array.</remarks>
    Public Overloads Sub Write(ByVal value As String, ByVal location As Int64)

        If value Is Nothing Then
            Throw New ArgumentNullException("value")
        End If

        MyBase.BaseStream.Seek(location, System.IO.SeekOrigin.Begin)
        MyBase.Write(value)

    End Sub

#End Region

End Class

