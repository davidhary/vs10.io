''' <summary>Handles end-of-file exceptions.</summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="03/04/04" by="David Hary" revision="1.0.1524.x">
''' created.
''' </history>
<Serializable()> Public Class EndOfFileException
    Inherits isr.IO.IOException

    Private Const _defMessage As String = "Reached end of file."
    Private Const _messageFormat As String = "Reached end of file '{0}'"

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructor with no parameters.</summary>
    Public Sub New()
        MyBase.New(_defMessage)
    End Sub

    ''' <summary>Constructor specifying the Message to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>Constructor specifying the Message and InnerException property to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Sets a reference to the InnerException.</param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>Constructes an exception with file name.  Dummy value is used to 
    '''   distintguish from a constructor with message only.</summary>
    ''' <param name="filePathName">Specifies the file path name.</param>
    ''' <param name="dummyValue">Place holder of nothing.</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="dummyValue")> _
    Public Sub New(ByVal filePathName As String, ByVal dummyValue As String)
        MyBase.New(_messageFormat, filePathName)
    End Sub

    ''' <summary>Constructor used for deserialization of the exception class.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, _
        ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

#End Region
End Class

''' <summary>Handles Input Output exceptions.</summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="03/04/04" by="David Hary" revision="1.0.1524.x">
''' created.
''' </history>
<Serializable()> Public Class IOException
    Inherits isr.IO.BaseException

    Private Const _defMessage As String = "failed accessing file."
    Private Const _messageFormat As String = "Failed accessing file '{0}'"

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructor with no parameters.</summary>
    Public Sub New()
        MyBase.New(_defMessage)
    End Sub

    ''' <summary>Constructor specifying the Message to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>Constructor specifying the Message and InnerException property to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Sets a reference to the InnerException.</param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>Constructes an exception with file name.  Dummy value is used to 
    '''   distintguish from a constructor with message only.</summary>
    ''' <param name="filePathName">Specifies the file path name.</param>
    ''' <param name="dummyValue">Place holder of nothing.</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="dummyValue")> _
    Public Sub New(ByVal filePathName As String, ByVal dummyValue As String)
        MyBase.New(_messageFormat, filePathName)
    End Sub

    ''' <summary>Constructor specifying the meesage format and its arguments.</summary>
    ''' <param name="format">Specifies the exception message format.</param>
    ''' <param name="args">Specifies the message arguments.</param>
    Public Sub New(ByVal format As String, ByVal ParamArray args() As Object)
        MyBase.New(format, args)
    End Sub

    ''' <summary>Constructor used for deserialization of the exception class.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, _
        ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

#End Region
End Class

''' <summary>Handles data conversion exceptions.</summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="03/04/04" by="David Hary" revision="1.0.1524.x">
''' created.
''' </history>
<Serializable()> Public Class CastException
    Inherits isr.IO.BaseException

    Private Const _defMessage As String = "failed converting delimited value to the expected type."
    Private Const _messageFormat As String = "Failed converting delimited value '{1}' in file '{0}' to the requested type."

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructor with no parameters.</summary>
    Public Sub New()
        MyBase.New(_defMessage)
    End Sub

    ''' <summary>Constructor specifying the Message to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>Constructor specifying the Message and InnerException property to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Sets a reference to the InnerException.</param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>Constructes an exception with file name and a value.</summary>
    ''' <param name="filePathName">Specifies the file path name.</param>
    ''' <param name="Value">Place holder of nothing.</param>
    Public Sub New(ByVal filePathName As String, ByVal value As String)
        MyBase.New(_messageFormat, filePathName, value)
    End Sub

    ''' <summary>Constructor used for deserialization of the exception class.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, _
        ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

#End Region

End Class

