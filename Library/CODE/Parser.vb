''' <summary>Parses object values.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/01/09" by="David Hary" revision="3.0.3296.x">
''' created.
''' </history>
Public NotInheritable Class Parser

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Prevent constructing this class.</summary>
    Private Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " NULL OR EMPTY "

    Private Shared _dbNullString As String = "#NULL#"
    ''' <summary>Gets or sets the DBNull string as stored in Excel.</summary>
    Public Shared ReadOnly Property DBNullString() As String
        Get
            Return _dbNullString
        End Get
    End Property

    ''' <summary>
    ''' Returns true if the value is a DB Null.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function IsDBNull(ByVal value As Object) As Boolean
        Return value IsNot Nothing AndAlso (TypeOf value Is DBNull)
    End Function

    ''' <summary>
    ''' Returns true if the value is empty.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function IsEmpty(ByVal value As Object) As Boolean
        Return value Is Nothing OrElse _
          ((TypeOf value Is String) AndAlso String.IsNullOrEmpty(value.ToString))
    End Function

    ''' <summary>Returns true if the specified cell is empty or includes a DBNull value.</summary>
    Public Shared Function IsDBNullOrEmpty(ByVal value As Object) As Boolean
        Return IsDBNull(value) OrElse IsEmpty(value)
    End Function

#End Region

#Region " OBJECT PARSERS "

    ''' <summary>
    ''' Returns a value or DB Null
    ''' </summary>
    ''' <param name="value">Specifies the parsed alue.</param>
    ''' <returns></returns>
    Public Shared Function Parse(ByVal value As Object) As Object
        If value IsNot Nothing AndAlso (TypeOf value Is String) _
            AndAlso Convert.ToString(value, _
              Globalization.CultureInfo.CurrentCulture).Equals(_dbNullString) Then
            value = System.DBNull.Value
        End If
        Return value
    End Function

    ''' <summary>Returns the value of the given cell as a nullable Boolean type.</summary>
    ''' <param name="value">Specifies the parsed alue.</param>
    ''' <param name="defaultValue">Specifies a default value</param>
    Public Shared Function Parse(ByVal value As Object, ByVal defaultValue As Boolean?) As Boolean?

        If (TypeOf value Is Boolean) Then
            Return CType(value, Boolean)
        ElseIf TypeOf value Is String Then
            Dim output As Boolean
            If Boolean.TryParse(Convert.ToString(value, Globalization.CultureInfo.CurrentCulture), output) Then
                Return output
            Else
                Return defaultValue
            End If
        Else
            Return defaultValue
        End If

    End Function

    ''' <summary>Returns the value of the specified cell as a Boolean type.</summary>
    ''' <param name="value">Specifies the parsed alue.</param>
    ''' <param name="defaultValue">Specifies a default value</param>
    Public Shared Function Parse(ByVal value As Object, ByVal defaultValue As Boolean) As Boolean
        Return Parse(value, New Boolean?(defaultValue)).Value
    End Function

    ''' <summary>Returns the value of the specified cell as a nullable Date type.</summary>
    ''' <param name="value">Specifies the parsed alue.</param>
    ''' <param name="defaultValue">Specifies a default value</param>
    Public Shared Function Parse(ByVal value As Object, ByVal defaultValue As DateTime?) As DateTime?

        Dim output As Double? = Parse(value, New Double?)
        If output.HasValue Then
            Return DateTime.FromOADate(output.Value)
        Else
            Return defaultValue
        End If

    End Function

    ''' <summary>Returns the value of the specified cell as a datetime type.</summary>
    ''' <param name="value">Specifies the parsed alue.</param>
    ''' <param name="defaultValue">Specifies a default value</param>
    Public Shared Function Parse(ByVal value As Object, ByVal defaultValue As DateTime) As DateTime
        Return Parse(value, New DateTime?(defaultValue)).Value
    End Function

    ''' <summary>Returns the value of the specified cell as a nullable Double type.</summary>
    ''' <param name="value">Specifies the parsed alue.</param>
    ''' <param name="defaultValue">Specifies a default value</param>
    Public Shared Function Parse(ByVal value As Object, ByVal defaultValue As Double?) As Double?

        If value Is Nothing Then
            Return defaultValue
        Else
            If TypeOf value Is Double Then
                Dim numericValue As Double
                numericValue = CType(value, Double)
                If Double.IsNaN(numericValue) Then
                    Return defaultValue
                Else
                    Return numericValue
                End If
            ElseIf TypeOf value Is String Then
                Dim textValue As String = Convert.ToString(value, Globalization.CultureInfo.CurrentCulture).Trim
                If textValue.StartsWith("&", StringComparison.OrdinalIgnoreCase) Then
                    Dim numericValue As Long
                    textValue = textValue.TrimStart("&Hh".ToCharArray)
                    If Long.TryParse(textValue, Globalization.NumberStyles.AllowHexSpecifier, _
                                       Globalization.CultureInfo.CurrentCulture, numericValue) Then
                        Return numericValue
                    Else
                        Return defaultValue
                    End If
                ElseIf textValue.StartsWith("0x", StringComparison.OrdinalIgnoreCase) Then
                    Dim numericValue As Long
                    textValue = textValue.Substring(2)
                    If Long.TryParse(textValue, Globalization.NumberStyles.AllowHexSpecifier, _
                                       Globalization.CultureInfo.CurrentCulture, numericValue) Then
                        Return numericValue
                    Else
                        Return defaultValue
                    End If
                Else
                    Dim numericValue As Double
                    If Double.TryParse(textValue, Globalization.NumberStyles.Number _
                                       Or Globalization.NumberStyles.AllowExponent, _
                                       Globalization.CultureInfo.CurrentCulture, numericValue) Then
                        Return numericValue
                    Else
                        Return defaultValue
                    End If
                End If
            Else
                Return defaultValue
            End If
        End If

    End Function

    ''' <summary>Returns the value of the specified cell as a double type.</summary>
    ''' <param name="value">Specifies the parsed alue.</param>
    ''' <param name="defaultValue">Specifies a default value</param>
    Public Shared Function Parse(ByVal value As Object, ByVal defaultValue As Double) As Double
        Return Parse(value, New Double?(defaultValue)).Value
    End Function

    ''' <summary>Returns the value of the specified cell as a Nullable Integer type.</summary>
    ''' <param name="value">Specifies the parsed alue.</param>
    ''' <param name="defaultValue">Specifies a default value</param>
    Public Shared Function Parse(ByVal value As Object, ByVal defaultValue As Integer?) As Integer?

        Dim output As Double? = Parse(value, New Double?)
        If output.HasValue Then
            Return Convert.ToInt32(output.Value)
        Else
            Return defaultValue
        End If

    End Function

    ''' <summary>Returns the value of the specified cell as a Integer type.</summary>
    ''' <param name="value">Specifies the parsed alue.</param>
    ''' <param name="defaultValue">Specifies a default value</param>
    Public Shared Function Parse(ByVal value As Object, ByVal defaultValue As Integer) As Integer
        Return Parse(value, New Integer?(defaultValue)).Value
    End Function

    ''' <summary>
    ''' Returns a cell value or default value of String type.
    ''' </summary>
    ''' <param name="value">Specifies the parsed alue.</param>
    ''' <param name="defaultValue">Specifies a default value</param>
    ''' <returns></returns>
    Public Shared Function Parse(ByVal value As Object, ByVal defaultValue As String) As String
        If IsDBNullOrEmpty(value) Then
            Return defaultValue
        Else
            Return Convert.ToString(value, Globalization.CultureInfo.CurrentCulture)
        End If
    End Function

#End Region


End Class
