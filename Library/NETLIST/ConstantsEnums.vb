﻿Namespace NetList

    ''' <summary>
    ''' Enumerates the states reading the net list
    ''' </summary>
    ''' <remarks></remarks>
    Friend Enum ReadingState
        None
        LookingForConnectorList
        ReadingConnectors
        LookingForWireList
        LookingForNode
        ReadingNode
        Done
        Failed
    End Enum

End Namespace
