Namespace NetList

    ''' <summary>This is the <see cref="isr.IO.NetList.Node">Node</see> class of the net list reader class library.
    ''' A node consists of a collection of connected pins.</summary>
    ''' <license>
    ''' (c) 2007 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="12/18/07" by="David Hary" revision="1.2.2908.x">
    ''' created.
    ''' </history>
    Public Class Node

        Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        Public Sub New()

            ' instantiate the class
            MyBase.New()
            _pins = New Collections.Generic.Dictionary(Of String, Pin)

        End Sub

        ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        ''' <remarks>Do not make this method overridable (virtual) because a derived 
        '''   class should not be able to override this method.</remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        Private _isDisposed As Boolean
        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
        ''' provided proper implementation.
        ''' </summary>
        Protected Property IsDisposed() As Boolean
            Get
                Return _isDisposed
            End Get
            Private Set(ByVal value As Boolean)
                _isDisposed = value
            End Set
        End Property

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                Try

                    If disposing Then

                        ' Free managed resources when explicitly called
                        _pins = Nothing

                    End If

                    ' Free shared unmanaged resources

                Finally

                    ' set the sentinel indicating that the class was disposed.
                    Me.IsDisposed = True

                End Try

            End If

        End Sub

        ''' <summary>This destructor will run only if the Dispose method 
        '''   does not get called. It gives the base class the opportunity to 
        '''   finalize. Do not provide destructors in types derived from this class.</summary>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal in terms of
            ' readability and maintainability.
            Dispose(False)
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Sub

#End Region

#Region " METHODS "

        ''' <summary>
        ''' Adds a new <see cref="Pin">Pin</see> to the <see cref="Node">Node</see>
        ''' or selects an existing pin.
        ''' </summary>
        ''' <param name="cable"></param>
        ''' <param name="record"></param>
        ''' <returns>A reference to the pin.</returns>
        ''' <remarks></remarks>
        Public Function AddPin(ByVal cable As Cable, ByVal record As String) As Pin

            If cable Is Nothing Then
                Throw New ArgumentNullException("cable")
            End If

            If String.IsNullOrEmpty(record) Then
                Throw New ArgumentNullException("record")
            End If

            Dim newPin As New Pin
            If newPin.Parse(record) Then

                ' add the pin to the cable
                newPin = cable.AddPin(newPin.Number, newPin.ConnectorNumber)

                ' set reference to the node number.
                newPin.NodeNumber = Me.Number

                ' add the pin to the node
                If Not Exists(newPin) Then
                    _pins.Add(newPin.Key, newPin)
                End If

                newPin = _pins.Item(newPin.Key)

            End If

            Return newPin

        End Function

        ''' <summary>
        ''' Returns true of the pin already exists.
        ''' </summary>
        ''' <param name="pin"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Exists(ByVal pin As Pin) As Boolean

            Return _pins IsNot Nothing AndAlso _pins.ContainsKey(pin.Number)

        End Function

        ''' <summary>
        ''' Parses the node record.
        ''' </summary>
        ''' <param name="record">A record from the net list file.</param>
        ''' <returns></returns>
        ''' <remarks>The node record includes the following information:<para>
        ''' 0         1   
        ''' 01234567890123
        ''' [00001] N00469
        ''' Number (1, 5)</para><para>
        ''' Name (8, EOL)</para><para>
        ''' </para>
        ''' </remarks>
        Public Function Parse(ByVal record As String) As Boolean

            If String.IsNullOrEmpty("record") Then
                Throw New ArgumentNullException("record")
            End If

            Me._number = Int32.Parse(record.Substring(1, 5).Trim, Globalization.CultureInfo.InvariantCulture)
            Me._name = record.Substring(8).Trim
            Return Me._number > 0 AndAlso Not String.IsNullOrEmpty(_name)

        End Function

        ''' <summary>
        ''' Rolls back cable assembly by clearing all the public keys.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub Rollback()
            For Each pin As Pin In _pins.Values
                pin.PublicKey = 0
            Next
        End Sub

        ''' <summary>
        ''' Selects a pin from the pins collection.
        ''' </summary>
        ''' <param name="pinKey"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function SelectPin(ByVal pinKey As String) As Pin

            If String.IsNullOrEmpty("pinKey") Then
                Throw New ArgumentNullException("pinKey")
            End If
            Dim newPin As Pin = _pins.Item(pinKey)
            Return newPin

        End Function

#End Region

#Region " PROPERTIES "

        Private _name As String
        ''' <summary>
        ''' Gets or sets the node name.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Name() As String
            Get
                Return _name
            End Get
            Set(ByVal value As String)
                _name = value
            End Set
        End Property

        Private _number As Integer
        ''' <summary>
        ''' Gets or sets the node number.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Number() As Integer
            Get
                Return _number
            End Get
            Set(ByVal value As Integer)
                _number = value
            End Set
        End Property

        Private _publicKey As Integer
        ''' <summary>
        ''' Gets or sets the public key reference from the database.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property PublicKey() As Integer
            Get
                Return _publicKey
            End Get
            Set(ByVal value As Integer)
                _publicKey = value
            End Set
        End Property

        Private _pins As Collections.Generic.Dictionary(Of String, Pin)
        ''' <summary>
        ''' Gets the collection of <see cref="Pin">Pins</see>. 
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Pins() As System.Collections.ObjectModel.ReadOnlyCollection(Of Pin)
            Get
                Return New System.Collections.ObjectModel.ReadOnlyCollection(Of Pin)( _
                    CType(_pins.Values.ToList, Global.System.Collections.Generic.IList(Of Global.isr.IO.NetList.Pin)))
            End Get
        End Property

#End Region

    End Class

End Namespace

