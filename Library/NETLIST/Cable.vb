Imports Microsoft.VisualBasic
Imports System.Collections.Generic

Namespace NetList

    ''' <summary>This is the <see cref="isr.IO.NetList.Cable">Cable</see> class of the net list reader class library.
    ''' A cable consists of a set of connector and nodes defining how the connector pins are
    ''' wired.</summary>
    ''' <license>
    ''' (c) 2007 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="12/18/07" by="David Hary" revision="1.2.2908.x">
    ''' created.
    ''' </history>
    Public Class Cable

        Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        Public Sub New()

            ' instantiate the class
            MyBase.New()
            _nodes = New Collections.Generic.Dictionary(Of Integer, Node)
            _connectors = New Collections.Generic.Dictionary(Of String, Connector)

        End Sub

        ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        ''' <remarks>Do not make this method overridable (virtual) because a derived 
        '''   class should not be able to override this method.</remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        Private _isDisposed As Boolean
        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
        ''' provided proper implementation.
        ''' </summary>
        Protected Property IsDisposed() As Boolean
            Get
                Return _isDisposed
            End Get
            Private Set(ByVal value As Boolean)
                _isDisposed = value
            End Set
        End Property

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                Try

                    If disposing Then

                        ' Free managed resources when explicitly called
                        _connectors = Nothing
                        _nodes = Nothing
                        _wires = Nothing

                    End If

                    ' Free shared unmanaged resources

                Finally

                    ' set the sentinel indicating that the class was disposed.
                    Me.IsDisposed = True

                End Try

            End If

        End Sub

        ''' <summary>This destructor will run only if the Dispose method 
        '''   does not get called. It gives the base class the opportunity to 
        '''   finalize. Do not provide destructors in types derived from this class.</summary>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal in terms of
            ' readability and maintainability.
            Dispose(False)
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Sub

#End Region

#Region " METHODS "

        ''' <summary>
        ''' Adds a <see cref="Connector">connector</see> to the <see cref="Cable">cable</see>.
        ''' </summary>
        ''' <param name="record">A record from the net list file.</param>
        ''' <remarks></remarks>
        Public Function AddConnector(ByVal record As String) As Connector

            Dim newConnector As New Connector
            If newConnector.Parse(record) Then
                If Not _connectors.ContainsKey(newConnector.Number) Then
                    _connectors.Add(newConnector.Number, newConnector)
                End If
            End If
            Return newConnector

        End Function

        ''' <summary>
        ''' Adds a <see cref="Node">Node</see> to the <see cref="Cable">cable</see>.
        ''' </summary>
        ''' <param name="record">A record from the net list file.</param>
        ''' <remarks></remarks>
        Public Function AddNode(ByVal record As String) As Node

            Dim newNode As New Node

            If newNode.Parse(record) Then
                _nodes.Add(newNode.Number, newNode)
            End If
            Return newNode

        End Function

        ''' <summary>
        ''' Adds <see cref="Wire">Wires</see> to the <see cref="Cable">cable</see>.
        ''' </summary>
        ''' <param name="node">A record from the net list file.</param>
        ''' <param name="includeRedundantWires">True to add also redundant wires.</param>
        ''' <remarks></remarks>
        Public Function AddWires(ByVal node As Node, ByVal includeRedundantWires As Boolean) As Boolean

            If node Is Nothing Then
                Throw New ArgumentNullException("node")
            End If

            If node.Pins.Count > 1 Then
                For i As Integer = 1 To node.Pins.Count - 1
                    Dim lastPin As Integer = i
                    If includeRedundantWires Then
                        lastPin = node.Pins.Count - 1
                    End If
                    For j As Integer = i To lastPin
                        Dim newWire As Wire = New Wire(node.Pins(i - 1), node.Pins(j))
                        newWire.IsPrimary = (i = j)
                        Debug.Assert(Not Me.Exists(newWire), "Wire exists")
                        If Not Me.Exists(newWire) Then
                            _wires.Add(newWire.Key, newWire)
                        End If
                    Next
                Next
            End If
            Return True

        End Function

        ''' <summary>
        ''' Adds a <see cref="Pin">Pin</see> to the <see cref="Connector">Connector</see>.
        ''' </summary>
        ''' <returns>A new or exiting pin</returns>
        ''' <remarks></remarks>
        Public Function AddPin(ByVal pinNumber As String, ByVal connectorNumber As String) As Pin

            If String.IsNullOrEmpty(pinNumber) Then
                Throw New ArgumentNullException("pinNumber")
            End If

            If String.IsNullOrEmpty(connectorNumber) Then
                Throw New ArgumentNullException("connectorNumber")
            End If

            If Not Me.ConnectorExists(connectorNumber) Then
                Dim message As String = "Failed adding pin '{0}'. Connector number '{1}' not found"
                message = String.Format(Globalization.CultureInfo.CurrentCulture, message, pinNumber, connectorNumber)
                Throw New isr.IO.BaseException(message)
            End If

            ' add the pin to the connector.
            Return Me.SelectConnector(connectorNumber).AddPin(pinNumber)

        End Function

        ''' <summary>
        ''' Returns true of the connector already exists.
        ''' </summary>
        ''' <param name="connectorNumber"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function ConnectorExists(ByVal connectorNumber As String) As Boolean

            Return _connectors IsNot Nothing AndAlso _connectors.ContainsKey(connectorNumber)

        End Function

        ''' <summary>
        ''' Returns true of the connector already exists.
        ''' </summary>
        ''' <param name="pin"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function ConnectorExists(ByVal pin As Pin) As Boolean

            Return _connectors IsNot Nothing AndAlso _connectors.ContainsKey(pin.ConnectorNumber)

        End Function

        ''' <summary>
        ''' Returns true of the connector already exists.
        ''' </summary>
        ''' <param name="connector"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Exists(ByVal connector As Connector) As Boolean

            Return connector IsNot Nothing AndAlso Me.ConnectorExists(connector.Number)

        End Function

        ''' <summary>
        ''' Returns true of the node already exists.
        ''' </summary>
        ''' <param name="node"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Exists(ByVal node As Node) As Boolean

            Return node IsNot Nothing AndAlso Me.NodeExists(node.Number)

        End Function

        ''' <summary>
        ''' Returns true of the node already exists.
        ''' </summary>
        ''' <param name="nodeNumber"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function NodeExists(ByVal nodeNumber As Integer) As Boolean

            Return _nodes IsNot Nothing AndAlso _nodes.ContainsKey(nodeNumber)

        End Function

        ''' <summary>
        ''' Returns true of the wire already exists.
        ''' </summary>
        ''' <param name="wire"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Exists(ByVal wire As Wire) As Boolean

            Return wire IsNot Nothing AndAlso Me.WireExists(wire.Key)

        End Function

        ''' <summary>
        ''' Returns the list of pin keys defined for this cable.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function PinKeys() As String()
            If Me.HasPins Then
                Dim pinArray As String() = {}
                For Each connector As Connector In _connectors.Values
                    Dim keys As String() = connector.PinKeys
                    Dim currentLength As Integer = pinArray.Length
                    Array.Resize(pinArray, pinArray.Length + keys.Length)
                    keys.CopyTo(pinArray, currentLength)
                Next
                Return pinArray
            Else
                Return New String() {}
            End If
        End Function

        Private _activeNode As Node

        ''' <summary>
        ''' Parses each record based on the reading state.
        ''' </summary>
        ''' <param name="record"></param>
        ''' <returns></returns>
        ''' <remarks>The sequence of reading the net list proceeds as follows:<para>
        ''' 1.  Look for the connector list.  This is signified by the &lt;&lt;&lt; Component List &gt;&gt;&gt;</para><para>
        ''' 2. Read the connectors.</para><para>
        ''' 3. Look for wire list signified by &lt;&lt;&lt; Wire List >>> &gt;&gt;&gt;</para><para>
        ''' 4. Repease looking for nodes.  Each nodes starts with a [</para><para>
        ''' 5. Fore each node read all the wires adding pins as necessary.</para><para>
        ''' </para>
        ''' </remarks>
        Private Function Parse(ByVal record As String, ByVal readingState As ReadingState) As ReadingState

            If readingState = NetList.ReadingState.Done Then

                ' if done do nothing.

            ElseIf readingState = NetList.ReadingState.Failed Then

                ' if failed do nothing.

            ElseIf readingState = NetList.ReadingState.LookingForConnectorList Then

                ' look for the connector header
                If record.IndexOf(Cable.ComponentListHeader, StringComparison.OrdinalIgnoreCase) >= 0 Then
                    readingState = NetList.ReadingState.ReadingConnectors
                End If

            ElseIf readingState = NetList.ReadingState.LookingForNode Then

                ' look for the node number prefix
                If record.IndexOf(Cable.NodeNumberPrfix, StringComparison.OrdinalIgnoreCase) >= 0 Then
                    readingState = NetList.ReadingState.ReadingNode
                    Me._activeNode = Me.AddNode(record)
                End If

            ElseIf readingState = NetList.ReadingState.LookingForWireList Then

                ' look for the wire list header
                If record.IndexOf(Cable.WireListHeader, StringComparison.OrdinalIgnoreCase) >= 0 Then
                    readingState = NetList.ReadingState.LookingForNode
                End If

            ElseIf readingState = NetList.ReadingState.None Then

                Return Me.Parse(record, NetList.ReadingState.LookingForConnectorList)

            ElseIf readingState = NetList.ReadingState.ReadingConnectors Then

                If record.Length > 2 Then

                    ' add a connector
                    Me.AddConnector(record)

                Else

                    ' if done reading connectors then look for wire list.
                    readingState = NetList.ReadingState.LookingForWireList

                End If

            ElseIf readingState = NetList.ReadingState.ReadingNode Then

                If record.Length > 2 Then

                    ' add a pin
                    Me._activeNode.AddPin(Me, record)

                Else

                    ' if done reading node look for the next node
                    readingState = NetList.ReadingState.LookingForNode

                End If

            Else

                Debug.Assert(False, "Unhandled reading state")

            End If

            Return readingState

        End Function

        ''' <summary>
        ''' Add pins to all connectors based on the number of pins specified.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function PopulateConnectors() As Boolean

            For Each existingConnector As Connector In _connectors.Values
                existingConnector.Populate()
            Next
            Return True

        End Function


        ''' <summary>
        ''' Creates the collection of wires.
        ''' </summary>
        ''' <param name="includeRedundantWires">True to add also redundant wires.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function PopulateWires(ByVal includeRedundantWires As Boolean) As Boolean

            _wires = New Collections.Generic.Dictionary(Of String, Wire)
            For Each newNode As Node In _nodes.Values
                Me.AddWires(newNode, includeRedundantWires)
            Next
            Return True

        End Function

        ''' <summary>
        ''' Reads the net list connectors from the file. 
        ''' </summary>
        ''' <param name="filePathName"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function ReadNetListConnectors(ByVal filePathName As String) As Boolean

            Dim success As Boolean = True

            If System.IO.File.Exists(filePathName) Then

                Dim fileContents As String
                fileContents = My.Computer.FileSystem.ReadAllText(filePathName)
                If String.IsNullOrEmpty(fileContents) Then
                    success = False
                Else
                    Dim rows As String() = fileContents.Split(CChar(Environment.NewLine))
                    If rows.Length < 3 Then
                        success = False
                    Else
                        Dim readingState As ReadingState = readingState.None
                        For Each record As String In rows
                            record = record.Trim(Environment.NewLine.ToCharArray).Trim
                            readingState = Me.Parse(record, readingState)
                            ' stop reading the first time we are looking for a node of wires
                            If readingState = NetList.ReadingState.Failed _
                                OrElse readingState = NetList.ReadingState.LookingForNode Then
                                Exit For
                            End If
                        Next
                        If readingState <> NetList.ReadingState.Failed Then
                            readingState = NetList.ReadingState.Done
                        End If
                        Return readingState = NetList.ReadingState.Done AndAlso Me._connectors.Count > 0
                    End If
                End If
            Else
                success = False
            End If

            Return success

        End Function


        ''' <summary>
        ''' Reads a net list from the file. 
        ''' </summary>
        ''' <param name="filePathName"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function ReadNetList(ByVal filePathName As String) As Boolean

            Dim success As Boolean = True

            If System.IO.File.Exists(filePathName) Then

                Dim fileContents As String
                fileContents = My.Computer.FileSystem.ReadAllText(filePathName)
                If String.IsNullOrEmpty(fileContents) Then
                    success = False
                Else
                    Dim rows As String() = fileContents.Split(CChar(Environment.NewLine))
                    If rows.Length < 3 Then
                        success = False
                    Else
                        Dim readingState As ReadingState = readingState.None
                        For Each record As String In rows
                            record = record.Trim(Environment.NewLine.ToCharArray).Trim
                            readingState = Me.Parse(record, readingState)
                            If readingState = NetList.ReadingState.Failed Then
                                Exit For
                            End If
                        Next
                        If readingState <> NetList.ReadingState.Failed Then
                            readingState = NetList.ReadingState.Done
                        End If
                        Return readingState = NetList.ReadingState.Done AndAlso Me._connectors.Count > 0 AndAlso Me._nodes.Count > 0
                    End If
                End If
            Else
                success = False
            End If

            Return success

        End Function

        ''' <summary>
        ''' Rolls back cable assembly by clearing all the public keys.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub Rollback()
            Me._publicKey = 0
            For Each connector As Connector In _connectors.Values
                connector.Rollback()
            Next
            For Each node As Node In _nodes.Values
                node.Rollback()
            Next
        End Sub

        ''' <summary>
        ''' Selects a Connector from the Connectors collection.
        ''' </summary>
        ''' <param name="connectorNumber"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function SelectConnector(ByVal connectorNumber As String) As Connector

            If String.IsNullOrEmpty("connectorNumber") Then
                Throw New ArgumentNullException("connectorNumber")
            End If
            Dim newConnector As Connector = _connectors.Item(connectorNumber)
            Return newConnector

        End Function

        ''' <summary>
        ''' Selects a Node from the Nodes collection.
        ''' </summary>
        ''' <param name="nodeNumber"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function SelectNode(ByVal nodeNumber As Integer) As Node

            Dim newNode As Node = _nodes.Item(nodeNumber)
            Return newNode

        End Function

        ''' <summary>
        ''' Returns true of the wire already exists.
        ''' </summary>
        ''' <param name="wireId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function WireExists(ByVal wireId As String) As Boolean

            Return _wires IsNot Nothing AndAlso _wires.ContainsKey(wireId)

        End Function

        ''' <summary>
        ''' Returns the list of wires as keys.  This can be displayed
        ''' in a list box.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function WireKeys() As String()
            If _wires Is Nothing Then
                Return New String() {}
            Else
                Return _wires.Keys.ToArray()
            End If
        End Function

#End Region

#Region " PROPERTIES "

        ''' <summary>
        ''' Gets the connector list header string.
        ''' </summary>
        ''' <remarks></remarks>
        Private Const ComponentListHeader As String = "<<< Component List >>>"

        ''' <summary>
        ''' Gets the wire list header string.
        ''' </summary>
        ''' <remarks></remarks>
        Private Const WireListHeader As String = "<<< Wire List >>>"

        ''' <summary>
        ''' Gets the first character for a new node record.
        ''' </summary>
        ''' <remarks></remarks>
        Private Const NodeNumberPrfix As String = "["

        Private _name As String
        ''' <summary>
        ''' Gets or sets the cable name.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Name() As String
            Get
                Return _name
            End Get
            Set(ByVal value As String)
                _name = value
            End Set
        End Property

        Private _number As String
        ''' <summary>
        ''' Gets or sets the cable number.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Number() As String
            Get
                Return _number
            End Get
            Set(ByVal value As String)
                _number = value
            End Set
        End Property

        Private _publicKey As Integer
        ''' <summary>
        ''' Gets or sets the public key reference from the database.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property PublicKey() As Integer
            Get
                Return _publicKey
            End Get
            Set(ByVal value As Integer)
                _publicKey = value
            End Set
        End Property

        Private _connectors As Collections.Generic.Dictionary(Of String, Connector)
        ''' <summary>
        ''' Gets the collection of <see cref="Connector">Connectors</see>. 
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Connectors() As System.Collections.ObjectModel.ReadOnlyCollection(Of Connector)
            Get
                Return New Collections.ObjectModel.ReadOnlyCollection(Of Connector)( _
                    CType(_connectors.Values.ToList, Global.System.Collections.Generic.IList(Of Global.isr.IO.NetList.Connector)))
            End Get
        End Property

        ''' <summary>
        ''' Gets the condition telling if the cable has connectors.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property HasConnectors() As Boolean
            Get
                Return _connectors IsNot Nothing AndAlso _connectors.Count > 0
            End Get
        End Property

        ''' <summary>
        ''' Gets the condition telling if the cable has pins.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property HasPins() As Boolean
            Get
                Return Me.HasConnectors AndAlso _connectors.Values.First.HasPins
            End Get
        End Property

        ''' <summary>
        ''' Gets the condition telling if the cable has wires.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property HasWires() As Boolean
            Get
                Return _wires IsNot Nothing AndAlso _wires.Count > 0
            End Get
        End Property

        Private _nodes As Collections.Generic.Dictionary(Of Integer, Node)
        ''' <summary>
        ''' Gets the collection of <see cref="Node">Nodes</see>. 
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Nodes() As System.Collections.ObjectModel.ReadOnlyCollection(Of Node)
            Get
                Return New Collections.ObjectModel.ReadOnlyCollection(Of Node)( _
                  CType(_nodes.Values.ToList, Global.System.Collections.Generic.IList(Of Global.isr.IO.NetList.Node)))
            End Get
        End Property

        ''' <summary>
        ''' Gets the collection of <see cref="Pin">pins</see>.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Pins() As System.Collections.ObjectModel.ReadOnlyCollection(Of Pin)
            Get
                Dim _pinsDictionary As New Collections.Generic.Dictionary(Of String, Pin)
                For Each Connector As Connector In _connectors.Values
                    For Each Pin As Pin In Connector.Pins
                        _pinsDictionary.Add(Pin.Key, Pin)
                    Next
                Next
                Return New Collections.ObjectModel.ReadOnlyCollection(Of Pin)( _
                    CType(_pinsDictionary.Values.ToList, Global.System.Collections.Generic.IList(Of Global.isr.IO.NetList.Pin)))
            End Get
        End Property

        Private _wires As Collections.Generic.Dictionary(Of String, Wire)
        ''' <summary>
        ''' Gets the collection of <see cref="Wire">Wires</see>. 
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Wires() As System.Collections.ObjectModel.ReadOnlyCollection(Of Wire)
            Get
                Return New Collections.ObjectModel.ReadOnlyCollection(Of Wire)( _
                    CType(_wires.Values.ToList, Global.System.Collections.Generic.IList(Of Global.isr.IO.NetList.Wire)))
            End Get
        End Property

#End Region

#Region " CREATABLE "

        Private _nonCreatableReason As String
        ''' <summary>
        ''' Gets teh reason why this connector is not creatable.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property NonCreatableReason() As String
            Get
                Return _nonCreatableReason
            End Get
        End Property
        ''' <summary>
        ''' Returns true if the connector is ready to populate pins.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Creatable() As Boolean
            Get
                _nonCreatableReason = ""
                For Each connector As isr.IO.NetList.Connector In Me.Connectors
                    If Not connector.Creatable Then
                        _nonCreatableReason = String.Format(Globalization.CultureInfo.CurrentCulture, _
                                                          "Connector number {0} is not creatable because {1}", connector.Number, connector.NonCreatableReason)
                        Return False
                    End If
                Next
                Return True
            End Get
        End Property

#End Region

    End Class

End Namespace

