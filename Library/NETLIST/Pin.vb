Namespace NetList

    ''' <summary>This is the <see cref="isr.IO.NetList.Pin">Pin</see> class of the net list reader class library.</summary>
    ''' <license>
    ''' (c) 2007 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="08/07/08" by="David Hary" revision="1.2.3141.x">
    ''' Add mating connector information for use with CITA cable tester.
    ''' </history>
    ''' <history date="08/07/08" by="David Hary" revision="1.2.3141.x">
    ''' Add node number.  Pins can be listed as belonging to a node separately from the connector.
    ''' The node is a functional description of the pins telling which pins are connected together.
    ''' </history>
    ''' <history date="12/18/07" by="David Hary" revision="1.2.2908.x">
    ''' created.
    ''' </history>
    Public Class Pin

        Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        Public Sub New()

            ' instantiate the class
            MyBase.New()

        End Sub

        ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        ''' <remarks>Do not make this method overridable (virtual) because a derived 
        '''   class should not be able to override this method.</remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        Private _isDisposed As Boolean
        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
        ''' provided proper implementation.
        ''' </summary>
        Protected Property IsDisposed() As Boolean
            Get
                Return _isDisposed
            End Get
            Private Set(ByVal value As Boolean)
                _isDisposed = value
            End Set
        End Property

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                Try

                    If disposing Then

                        ' Free managed resources when explicitly called

                    End If

                    ' Free shared unmanaged resources

                Finally

                    ' set the sentinel indicating that the class was disposed.
                    Me.IsDisposed = True

                End Try

            End If

        End Sub

        ''' <summary>This destructor will run only if the Dispose method 
        '''   does not get called. It gives the base class the opportunity to 
        '''   finalize. Do not provide destructors in types derived from this class.</summary>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal in terms of
            ' readability and maintainability.
            Dispose(False)
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Sub

#End Region

#Region " METHODS "

        ''' <summary>
        ''' Parses the pin record.
        ''' </summary>
        ''' <param name="record">A record from the net list file.</param>
        ''' <returns></returns>
        ''' <remarks>The pin record includes the pin information as follows (after Trim):<para>
        ''' 0         1         2         3         4         5         6         7   
        ''' 012345678901234567890123456789012345678901234567890123456789012345678901234567
        ''' P2              1       1               Passive        CONNECTOR DB9-2
        ''' J237            22      22              Passive        J237
        ''' Connector Number:  (0,15)</para><para>
        ''' Pin Number (16, 23)</para><para>
        ''' Pin Name (24, 39)</para><para>
        ''' Pin Type (40,54)</para><para>
        ''' Part Value (Connector Name/Description): (55,EOL)</para><para>
        ''' </para>
        ''' </remarks>
        Public Function Parse(ByVal record As String) As Boolean

            If String.IsNullOrEmpty("record") Then
                Throw New ArgumentNullException("record")
            End If

            Me._connectorNumber = record.Substring(0, 16).Trim
            Me._number = record.Substring(16, 8).Trim
            Me._name = record.Substring(24, 16).Trim
            Me._pinType = record.Substring(40, 15).Trim
            Me._partValue = record.Substring(55).Trim

            Return Not (String.IsNullOrEmpty(_connectorNumber) OrElse _
                          String.IsNullOrEmpty(_number) OrElse _
                          String.IsNullOrEmpty(_name) OrElse _
                          String.IsNullOrEmpty(_pinType) OrElse _
                            String.IsNullOrEmpty(_partValue))

        End Function

#End Region

#Region " PROPERTIES "

        ''' <summary>
        ''' Returns a unique key for a pin.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function BuildKey(ByVal pin As Pin) As String
            Return String.Format(Globalization.CultureInfo.CurrentCulture, _
                                 "{0}.{1}", pin.ConnectorNumber, pin.Number)
        End Function

        ''' <summary>
        ''' Returns a unique pin Key.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Key() As String
            Get
                Return Pin.BuildKey(Me)
            End Get
        End Property

        Private _name As String
        ''' <summary>
        ''' Gets or sets the pin name.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Name() As String
            Get
                Return _name
            End Get
            Set(ByVal value As String)
                _name = value
            End Set
        End Property

        Private _number As String
        ''' <summary>
        ''' Gets or sets the pin number.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Number() As String
            Get
                Return _number
            End Get
            Set(ByVal value As String)
                _number = value
            End Set
        End Property

        Private _publicKey As Integer
        ''' <summary>
        ''' Gets or sets the public key reference from the database.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property PublicKey() As Integer
            Get
                Return _publicKey
            End Get
            Set(ByVal value As Integer)
                _publicKey = value
            End Set
        End Property

        Private _pinType As String
        ''' <summary>
        ''' Gets or sets the pin type.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property PinType() As String
            Get
                Return _pinType
            End Get
            Set(ByVal value As String)
                _pinType = value
            End Set
        End Property

        Private _connectorNumber As String
        ''' <summary>
        ''' Gets or sets the connector number.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property ConnectorNumber() As String
            Get
                Return _connectorNumber
            End Get
            Set(ByVal value As String)
                _connectorNumber = value
            End Set
        End Property

        Private _nodeNumber As Integer?
        ''' <summary>
        ''' Gets or sets the node number.  The node number is nullable as not every 
        ''' pin belongs to a node.  Pins not belonging to a node are not wired.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property NodeNumber() As Integer?
            Get
                Return _nodeNumber
            End Get
            Set(ByVal value As Integer?)
                _nodeNumber = value
            End Set
        End Property

        Private _partValue As String
        ''' <summary>
        ''' Gets or sets the connector value.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property PartValue() As String
            Get
                Return _partValue
            End Get
            Set(ByVal value As String)
                _partValue = value
            End Set
        End Property

#End Region

#Region " CITA PROPERTIES "

        Private _matingSide As String = "P1"
        ''' <summary>
        ''' Gets or sets the mating side for this connectior.  For CITA systems this could be either 'P1' or 'P2'.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property MatingSide() As String
            Get
                Return _matingSide
            End Get
            Set(ByVal value As String)
                _matingSide = value
            End Set
        End Property

        Private _matingPinNumber As Integer = 1
        ''' <summary>
        ''' Gets or sets the pin number on the matring side to use for connecting this pin.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property MatingPinNumber() As Integer
            Get
                Return _matingPinNumber
            End Get
            Set(ByVal value As Integer)
                _matingPinNumber = value
            End Set
        End Property

#End Region


    End Class

End Namespace

