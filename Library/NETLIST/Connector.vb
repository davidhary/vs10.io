Namespace NetList

    ''' <summary>This is the <see cref="isr.IO.NetList.Connector">Connector</see> class of the net list reader class library.
    ''' A connector consists of a set of pins.</summary>
    ''' <license>
    ''' (c) 2007 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="08/07/08" by="David Hary" revision="1.2.3141.x">
    ''' Add support fo military style pins. Allow populating the connector before reading the connectors.
    ''' </history>
    ''' <history date="12/18/07" by="David Hary" revision="1.2.2908.x">
    ''' created.
    ''' </history>
    Public Class Connector

        Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        Public Sub New()

            ' instantiate the class
            MyBase.New()
            _pins = New Collections.Generic.Dictionary(Of String, Pin)

        End Sub

        ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        ''' <remarks>Do not make this method overridable (virtual) because a derived 
        '''   class should not be able to override this method.</remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        Private _isDisposed As Boolean
        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
        ''' provided proper implementation.
        ''' </summary>
        Protected Property IsDisposed() As Boolean
            Get
                Return _isDisposed
            End Get
            Private Set(ByVal value As Boolean)
                _isDisposed = value
            End Set
        End Property

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                Try

                    If disposing Then

                        ' Free managed resources when explicitly called
                        _pins = Nothing

                    End If

                    ' Free shared unmanaged resources

                Finally

                    ' set the sentinel indicating that the class was disposed.
                    Me.IsDisposed = True

                End Try

            End If

        End Sub

        ''' <summary>This destructor will run only if the Dispose method 
        '''   does not get called. It gives the base class the opportunity to 
        '''   finalize. Do not provide destructors in types derived from this class.</summary>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal in terms of
            ' readability and maintainability.
            Dispose(False)
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Sub

#End Region

#Region " PIN NUMBER FORMAT "

        ''' <summary>
        ''' Specifies the standard pins for alpha numberic pin set.
        ''' </summary>
        ''' <remarks></remarks>
        Private Shared _alphaPins As String() = {"A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", _
                                          "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "m", "n", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", _
                                           "AA", "BB", "CC", "DD", "EE", "FF", "GG", "HH", "JJ", "KK", "LL", "MM", "NN", "PP"}

        ''' <summary>
        ''' Builds a pin name from the pin number.
        ''' </summary>
        ''' <param name="pinNumber"></param>
        ''' <param name="useNumericFormat">True to use a numeric format for building the pin number.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function BuildPinNumber(ByVal pinNumber As Integer, ByVal useNumericFormat As Boolean) As String

            If useNumericFormat Then
                Return pinNumber.ToString("0", Globalization.CultureInfo.CurrentCulture)
            Else
                Return _alphaPins(pinNumber - 1)
            End If

        End Function

        Private _shellPinNumber As String = "SH"
        ''' <summary>
        ''' Gets or sets the shell pin number.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property ShellPinNumber() As String
            Get
                Return _shellPinNumber
            End Get
            Set(ByVal value As String)
                _shellPinNumber = value
            End Set
        End Property

#End Region

#Region " METHODS "

        ''' <summary>
        ''' Adds a new <see cref="Pin">Pin</see> to the <see cref="Connector">Connector</see>
        ''' or returns a reference to an existing pin.
        ''' </summary>
        ''' <param name="pinNumber"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function AddPin(ByVal pinNumber As String) As Pin

            If String.IsNullOrEmpty("pinNumber") Then
                Throw New ArgumentNullException("pinNumber")
            End If

            If Me.Exists(pinNumber) Then
                Return Me.SelectPin(pinNumber)
            Else
                Dim newPin As New Pin
                newPin.Name = pinNumber
                newPin.Number = pinNumber
                newPin.ConnectorNumber = Me.Number
                newPin.PartValue = Me.Name
                newPin.MatingSide = Me.MatingSide
                newPin.MatingPinNumber = _firstMatingPinNumber + _pins.Count
                _pins.Add(pinNumber, newPin)
                Return newPin
            End If

        End Function

        ''' <summary>
        ''' Returns true of the pin already exists.
        ''' </summary>
        ''' <param name="pin"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Exists(ByVal pin As Pin) As Boolean

            Return pin IsNot Nothing AndAlso Me.Exists(pin.Number)

        End Function

        ''' <summary>
        ''' Returns true of the pin already exists.
        ''' </summary>
        ''' <param name="pinNumber"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Exists(ByVal pinNumber As String) As Boolean

            Return _pins IsNot Nothing AndAlso _pins.ContainsKey(pinNumber)

        End Function

        ''' <summary>
        ''' Gets the condition telling if the connector has pins.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property HasPins() As Boolean
            Get
                Return _pins IsNot Nothing AndAlso _pins.Count > 0
            End Get
        End Property

        ''' <summary>
        ''' Parses the connector record.
        ''' </summary>
        ''' <param name="record">A record from the net list file.</param>
        ''' <returns></returns>
        ''' <remarks>The connector record includes the following information:<para>
        ''' 0         1         2         3         4         
        ''' 01234567890123456789012345678901234567890123456789
        ''' J236                          J236      J236
        ''' CONNECTOR DB9-1               P1        CONNECTOR DB9-1
        ''' Name (0, 29)</para><para>
        ''' Number (30, 39)</para><para>
        ''' Part Value (40, EOL)</para><para>
        ''' </para>
        ''' </remarks>
        Public Function Parse(ByVal record As String) As Boolean

            If String.IsNullOrEmpty("record") Then
                Throw New ArgumentNullException("record")
            End If

            Me._name = record.Substring(0, 30).Trim
            Me._number = record.Substring(30, 10).Trim

            Return Not (String.IsNullOrEmpty(_number) OrElse _
                          String.IsNullOrEmpty(_name))

        End Function

        ''' <summary>
        ''' Returns a string array of pin keys.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function PinKeys() As String()
            If Me.HasPins Then
                Dim keys(_pins.Count - 1) As String
                For i As Integer = 0 To _pins.Count - 1
                    keys(i) = _pins.Values(i).Key
                Next
                Return keys
            Else
                Return New String() {}
            End If
        End Function

        ''' <summary>
        ''' Adds pins to the connector based on the 
        ''' <see cref="PinCount">Pin Count</see> and 
        ''' <see cref="IsNumericPinNumbering">pin numbering format</see>.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Populate() As Boolean

            For i As Integer = 1 To Me.PinCount
                Me.AddPin(Connector.BuildPinNumber(i, Me._isNumericPinNumbering))
            Next
            If Me.HasShellPin Then
                Me.AddPin(Me.ShellPinNumber)
            End If
            Return True

        End Function

        ''' <summary>
        ''' Rolls back cable assembly by clearing all the public keys.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub Rollback()
            Me._publicKey = 0
            For Each pin As Pin In _pins.Values
                pin.PublicKey = 0
            Next
        End Sub

        ''' <summary>
        ''' Selects a pin from the pins collection.
        ''' </summary>
        ''' <param name="pinNumber"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function SelectPin(ByVal pinNumber As String) As Pin

            If String.IsNullOrEmpty("pinNumber") Then
                Throw New ArgumentNullException("pinNumber")
            End If
            Dim newPin As Pin = _pins.Item(pinNumber)
            Return newPin

        End Function

#End Region

#Region " PROPERTIES "

        Private _name As String
        ''' <summary>
        ''' Gets or sets the connector name.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Name() As String
            Get
                Return _name
            End Get
            Set(ByVal value As String)
                _name = value
            End Set
        End Property

        Private _number As String
        ''' <summary>
        ''' Gets or sets the connector number.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Number() As String
            Get
                Return _number
            End Get
            Set(ByVal value As String)
                _number = value
            End Set
        End Property

        Private _publicKey As Integer
        ''' <summary>
        ''' Gets or sets the public key reference from the database.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property PublicKey() As Integer
            Get
                Return _publicKey
            End Get
            Set(ByVal value As Integer)
                _publicKey = value
            End Set
        End Property

        Private _pins As Collections.Generic.Dictionary(Of String, Pin)
        ''' <summary>
        ''' Gets the collection of <see cref="Pin">Pins</see>. 
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Pins() As System.Collections.ObjectModel.ReadOnlyCollection(Of Pin)
            Get
                Return New System.Collections.ObjectModel.ReadOnlyCollection(Of Pin)( _
                    CType(_pins.Values.ToList, Global.System.Collections.Generic.IList(Of Global.isr.IO.NetList.Pin)))
            End Get
        End Property

        Private _pinCount As Integer
        ''' <summary>
        ''' Gets or sets the number of pins in the connector.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property PinCount() As Integer
            Get
                Return _pinCount
            End Get
            Set(ByVal value As Integer)
                _pinCount = value
            End Set
        End Property

        Private _isNumericPinNumbering As Boolean = True
        ''' <summary>
        ''' Gets or sets the pin numbering format
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property IsNumericPinNumbering() As Boolean
            Get
                Return _isNumericPinNumbering
            End Get
            Set(ByVal value As Boolean)
                _isNumericPinNumbering = value
            End Set
        End Property

#End Region

#Region " CITA PROPERTIES "

        Private _hasShellPin As Boolean = True
        ''' <summary>
        ''' Gets or sets the shall pin atribute.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property HasShellPin() As Boolean
            Get
                Return _hasShellPin
            End Get
            Set(ByVal value As Boolean)
                _hasShellPin = value
            End Set
        End Property

        Private _matingSide As String = "P1"
        ''' <summary>
        ''' Gets or sets the mating side for this connectior.  For CITA systems this could be either 'P1' or 'P2'.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property MatingSide() As String
            Get
                Return _matingSide
            End Get
            Set(ByVal value As String)
                _matingSide = value
            End Set
        End Property

        Private _firstMatingPinNumber As Integer = 1
        ''' <summary>
        ''' Gets or sets the first pin on the matring side to use for connecting the first
        ''' pin of this connector.  One based.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property FirstMatingPinNumber() As Integer
            Get
                Return _firstMatingPinNumber
            End Get
            Set(ByVal value As Integer)
                _firstMatingPinNumber = value
            End Set
        End Property

        Private _nonCreatableReason As String
        ''' <summary>
        ''' Gets teh reason why this connector is not creatable.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property NonCreatableReason() As String
            Get
                Return _nonCreatableReason
            End Get
        End Property

        ''' <summary>
        ''' Returns true if the connector is ready to populate pins.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Creatable() As Boolean
            Get
                _nonCreatableReason = ""
                If Me.PinCount <= 0 Then
                    _nonCreatableReason = String.Format(Globalization.CultureInfo.CurrentCulture, _
                                                      "Pin count '={0}' must be greater than 0", Me.PinCount)
                    Return False
                ElseIf Me.FirstMatingPinNumber <= 0 Then
                    _nonCreatableReason = String.Format(Globalization.CultureInfo.CurrentCulture, _
                                                      "First mating pin number '={0}' must be greater than 0", Me.FirstMatingPinNumber)
                    Return False
                ElseIf Not (Me.MatingSide = "P1" OrElse Me.MatingSide = "P2") Then
                    _nonCreatableReason = String.Format(Globalization.CultureInfo.CurrentCulture, _
                                                      "Mating side '={0}' must be eitehr P1 or P2", Me.MatingSide)
                End If
                Return True
            End Get
        End Property

#End Region

    End Class

End Namespace

