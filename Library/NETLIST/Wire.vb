Namespace NetList

    ''' <summary>This is the <see cref="isr.IO.NetList.Pin">Pin</see> class of the net list reader class library.</summary>
    ''' <license>
    ''' (c) 2007 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="12/18/07" by="David Hary" revision="1.2.2908.x">
    ''' created.
    ''' </history>
    Public Class Wire

        Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        Public Sub New(ByVal pinA As Pin, ByVal pinB As Pin)

            ' instantiate the class
            MyBase.New()

            Me._pinA = pinA
            Me._pinB = pinB

        End Sub

        ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        ''' <remarks>Do not make this method overridable (virtual) because a derived 
        '''   class should not be able to override this method.</remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        Private _isDisposed As Boolean
        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
        ''' provided proper implementation.
        ''' </summary>
        Protected Property IsDisposed() As Boolean
            Get
                Return _isDisposed
            End Get
            Private Set(ByVal value As Boolean)
                _isDisposed = value
            End Set
        End Property

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                Try

                    If disposing Then

                        ' Free managed resources when explicitly called

                    End If

                    ' Free shared unmanaged resources

                Finally

                    ' set the sentinel indicating that the class was disposed.
                    Me.IsDisposed = True

                End Try

            End If


        End Sub

        ''' <summary>This destructor will run only if the Dispose method 
        '''   does not get called. It gives the base class the opportunity to 
        '''   finalize. Do not provide destructors in types derived from this class.</summary>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal in terms of
            ' readability and maintainability.
            Dispose(False)
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Sub

#End Region

#Region " PROPERTIES "

        ''' <summary>
        ''' Returns a wire ID.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function BuildKey(ByVal wire As Wire) As String
            Return String.Format(Globalization.CultureInfo.CurrentCulture, _
                                 "{0}-{1}", wire.PinA.Key, wire.PinB.Key)
        End Function

        ''' <summary>
        ''' Returns a unique Wire Key.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Key() As String
            Get
                Return Wire.BuildKey(Me)
            End Get
        End Property

        Private _pinA As Pin
        ''' <summary>
        ''' Gets <see cref="Pin">pin A</see> of the wire.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property PinA() As Pin
            Get
                Return _pinA
            End Get
        End Property

        Private _pinB As Pin
        ''' <summary>
        ''' Gets <see cref="Pin">pin B</see> of the wire.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property PinB() As Pin
            Get
                Return _pinB
            End Get
        End Property

        Private _isPrimary As Boolean
        ''' <summary>
        ''' Gets or sets the property indicating if the wire is a primary wire derived from connecting 
        ''' sequential pins of a node or a redundant wire derived by connecting pins that are already 
        ''' connected by primary wires.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property IsPrimary() As Boolean
            Get
                Return _isPrimary
            End Get
            Set(ByVal value As Boolean)
                _isPrimary = value
            End Set
        End Property

        Private _publicKey As Integer
        ''' <summary>
        ''' Gets or sets the public key reference from the database.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property PublicKey() As Integer
            Get
                Return _publicKey
            End Get
            Set(ByVal value As Integer)
                _publicKey = value
            End Set
        End Property

#End Region

    End Class

End Namespace

