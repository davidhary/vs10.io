Namespace Delimited

    ''' <summary>This is the base class for the Delimited file class library.</summary>
    ''' <license>
    ''' (c) 2004 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="03/04/04" by="David Hary" revision="1.0.1524.x">
    ''' created.
    ''' </history>
    Public Class File

        Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        Public Sub New()

            ' instantiate the base class
            Me.New(String.Empty)

        End Sub

        ''' <summary>Constructs this class.</summary>
        ''' <param name="instanceName">Specifies the name of the instance.</param>
        ''' <remarks>Use this constructor to instantiate this class
        '''   and set the instance name, which is useful in tracing</remarks>
        Public Sub New(ByVal instanceName As String)

            ' instantiate the base class
            MyBase.New()
            _instanceName = instanceName

        End Sub

        ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        ''' <remarks>Do not make this method overridable (virtual) because a derived 
        '''   class should not be able to override this method.</remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        Private _isDisposed As Boolean
        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
        ''' provided proper implementation.
        ''' </summary>
        Protected Property IsDisposed() As Boolean
            Get
                Return _isDisposed
            End Get
            Private Set(ByVal value As Boolean)
                _isDisposed = value
            End Set
        End Property

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                Try

                    If disposing Then

                        ' Free managed resources when explicitly called
                        _statusMessage = String.Empty
                        _instanceName = String.Empty

                    End If

                    ' Free shared unmanaged resources

                Finally

                    ' set the sentinel indicating that the class was disposed.
                    Me.IsDisposed = True

                End Try

            End If

        End Sub

        ''' <summary>This destructor will run only if the Dispose method 
        '''   does not get called. It gives the base class the opportunity to 
        '''   finalize. Do not provide destructors in types derived from this class.</summary>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal in terms of
            ' readability and maintainability.
            Dispose(False)
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Sub

#End Region

#Region " METHODS AND PROPERTIES "

        ''' <summary>Overrides ToString returning the instance name if not empty.</summary>
        ''' <remarks>Use this method to return the instance name. If instance name is not set, 
        '''   returns the base class ToString value.</remarks>
        Public Overrides Function ToString() As String
            If String.IsNullOrEmpty(Me._instanceName) Then
                Return MyBase.ToString
            Else
                Return Me._instanceName
            End If
        End Function

        Private _instanceName As String = String.Empty
        ''' <summary>Gets or sets the name given to an instance of this class.</summary>
        ''' <value><c>InstanceName</c> is a String property.</value>
        ''' <history date="11/23/04" by="David Hary" revision="1.0.1788.x">
        '''   Correct code not to get instance name from .ToString but from MyBase.ToString
        '''   so that calling this method from the child class will not break the rule of
        '''   calling overridable methods from the constructor.
        ''' </history>
        Public Property InstanceName() As String
            Get
                If Not String.IsNullOrEmpty(_instanceName) Then
                    Return _instanceName
                Else
                    Return MyBase.ToString
                End If
            End Get
            Set(ByVal value As String)
                _instanceName = value
            End Set
        End Property

        Private _statusMessage As String = String.Empty
        ''' <summary>Gets or sets the status message.</summary>
        ''' <value>A <see cref="System.String">String</see>.</value>
        Public Property StatusMessage() As String
            Get
                Return _statusMessage
            End Get
            Set(ByVal value As String)
                _statusMessage = value
            End Set
        End Property

#End Region

#Region " METHODS "

        ''' <summary>Closes the instance.</summary>
        ''' <returns>A Boolean data type</returns>
        ''' <exception cref="OperationCloseException" guarantee="strong"></exception>
        ''' <remarks>Use this method to close the instance.  The method returns true if success or 
        '''   false if it failed closing the instance.</remarks>
        <System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.LinkDemand, unrestricted:=True)> _
      Public Overridable Function [Close]() As Boolean

            Try

                ' close the file if not closed
                If IsOpen() Then
                    Microsoft.VisualBasic.FileClose(_fileNumber)
                    ' zero the file number to indicate that the file is closed.
                    _fileNumber = 0
                End If

            Catch ex As System.Exception
                ' throw an exception
                Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "{0} failed closing: {1}", Me.InstanceName, ex.Message)
                Throw New isr.IO.OperationCloseException(Me.StatusMessage, ex)
            End Try

        End Function

#End Region

#Region " PROPERTIES "

        Private _filePathName As String = String.Empty
        ''' <summary>Gets or sets the file name.</summary>
        ''' <value><c>FilePathName</c> is a String property.</value>
        ''' <remarks>Use this property to get or set the file name.</remarks>
        Public Property FilePathName() As String
            Get
                If _filePathName.Length = 0 Then
                    ' set default file name if empty.
                    _filePathName = Windows.Forms.Application.ExecutablePath & ".csv"
                End If
                Return _filePathName
            End Get
            Set(ByVal value As String)
                _filePathName = value
            End Set
        End Property

        Private _fileNumber As Int32
        ''' <summary>Gets or sets the file number.</summary>
        ''' <value><c>FileNumber</c>is an Int32 property</value>
        Public Property FileNumber() As Int32
            Get
                Return _fileNumber
            End Get
            Set(ByVal value As Int32)
                _fileNumber = value
            End Set
        End Property

        ''' <summary>Returns true if the file is at end of file</summary>
        ''' <value><c>IsEndOfFile</c>Is a Boolean property</value>
        Public ReadOnly Property IsEndOfFile() As Boolean
            <System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.LinkDemand, unrestricted:=True)> Get
                Return Microsoft.VisualBasic.EOF(_fileNumber)
            End Get
        End Property

        ''' <summary>Returns true if the file is open</summary>
        ''' <value><c>IsOpen</c>Is a Boolean property</value>
        ''' <remarks>The file is open if the file number is not zero</remarks>
        Public ReadOnly Property IsOpen() As Boolean
            Get
                Return _fileNumber <> 0
            End Get
        End Property

        Private _endOfRecord As Boolean
        ''' <summary>Gets or sets the end-of-record indicator.</summary>
        ''' <value><c>EndOfRecord</c>is a Boolean property.</value>
        ''' <remarks>The end-of-record indicator is used to control how the object
        '''   handles writing.  When on, the writer will insert end of record 
        '''   character (CR LF) instead of a delimiter.</remarks>
        Public Property EndOfRecord() As Boolean
            Get
                Return _endOfRecord
            End Get
            Set(ByVal value As Boolean)
                _endOfRecord = value
            End Set
        End Property

        ''' <summary>The property returns a new free file number that can be used to open
        '''   a file.</summary>
        ''' <value><c>NewFileNumber</c>is in Int32 read only property.</value>
        ''' <remarks></remarks>
        Public Shared ReadOnly Property NewFileNumber() As Int32
            <System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.LinkDemand, unrestricted:=True)> Get
                Return Microsoft.VisualBasic.FreeFile()
            End Get
        End Property

#End Region

    End Class

End Namespace
