Namespace Delimited

    ''' <summary>This is the writer class of the delimited file class library.</summary>
    ''' <license>
    ''' (c) 2004 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="03/04/04" by="David Hary" revision="1.0.1524.x">
    ''' created.
    ''' </history>
    Public Class Writer

        ' based on the File inheritable class
        Inherits isr.IO.Delimited.File

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        Public Sub New()

            ' instantiate the base class
            MyBase.New()

        End Sub

        ''' <summary>Constructs this class.</summary>
        ''' <param name="instanceName">Specifies the name of the instance.</param>
        ''' <remarks>Use this constructor to instantiate this class
        '''   and set the instance name, which is useful in tracing</remarks>
        Public Sub New( _
          ByVal instanceName As String)

            ' instantiate the base class
            MyBase.New(instanceName)

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            If Not MyBase.IsDisposed Then

                Try

                    If disposing Then

                        ' dispose manager resources

                    End If

                    ' Free shared unmanaged resources

                Finally

                    ' Invoke the base class dispose method
                    MyBase.Dispose(disposing)

                End Try

            End If

        End Sub

#End Region

#Region " METHODS  and  PROPERTIES "

        ''' <summary>opens the file.</summary>
        ''' <returns>A Boolean data type</returns>
        ''' <param name="append">A boolean expression that specifies if the
        '''   new data will append to an existing file or if a new file will be 
        '''  Created possibly over-writing an existing file.</param>
        ''' <exception cref="OperationOpenException" guarantee="strong"></exception>
        ''' <remarks>Use this method to open the instance.  The method returns true if success or 
        '''   false if it failed opening the file.</remarks>
        <System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.LinkDemand, unrestricted:=True)> _
        Public Function [Open](ByVal append As Boolean) As Boolean

            Try

                MyBase.FileNumber = File.NewFileNumber
                If append Then
                    Microsoft.VisualBasic.FileOpen(MyBase.FileNumber, MyBase.FilePathName, Microsoft.VisualBasic.OpenMode.Append)
                Else
                    Microsoft.VisualBasic.FileOpen(MyBase.FileNumber, MyBase.FilePathName, Microsoft.VisualBasic.OpenMode.Output)
                End If
                Return True

            Catch ex As System.IO.IOException

                ' close to meet strong guarantees
                Try
                    Me.Close()
                Finally
                End Try

                ' catch an io exception so that we can return our open error handle
                Throw New isr.IO.IOException(String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "{0} Failed opening Delimited File for writing.", Me.InstanceName), ex)

            Catch

                ' close to meet strong guarantees
                Try
                    Me.Close()
                Finally
                End Try

                ' throw an exception
                Throw

            End Try

        End Function

        ''' <summary>Writes an end of record (CR LF) to the file.</summary>
        <System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.LinkDemand, unrestricted:=True)> _
        Public Sub EndRecord()
            Microsoft.VisualBasic.WriteLine(MyBase.FileNumber)
        End Sub

        Private _isAutoDelimited As Boolean
        ''' <summary>Gets or set limiting output data with commas and 
        ''' notation marks such as quotes for text.
        ''' With header sections, such notation needs to be turned off.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property IsAutoDelimited() As Boolean
            Get
                Return _isAutoDelimited
            End Get
            Set(ByVal value As Boolean)
                _isAutoDelimited = value
            End Set
        End Property

        Private _lastValue As Object = String.Empty
        ''' <summary>
        ''' Returns the last value written.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property LastValue() As Object
            Get
                Return _lastValue
            End Get
        End Property

        ''' <summary>
        ''' Writes a value to the delimited file and holds this value.
        ''' </summary>
        ''' <param name="value"></param>
        ''' <remarks></remarks>
        <System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.LinkDemand, unrestricted:=True)> _
        Public Sub Write(ByVal value As Object)
            _lastValue = value
            If MyBase.EndOfRecord Then
                If Me._isAutoDelimited Then
                    Microsoft.VisualBasic.WriteLine(MyBase.FileNumber, value)
                Else
                    Microsoft.VisualBasic.PrintLine(MyBase.FileNumber, value)
                End If
            Else
                If Me._isAutoDelimited Then
                    Microsoft.VisualBasic.Write(MyBase.FileNumber, value)
                Else
                    Microsoft.VisualBasic.Print(MyBase.FileNumber, value)
                    Microsoft.VisualBasic.Print(MyBase.FileNumber, ",")
                End If
            End If
        End Sub

        ''' <summary>Writes a value to the delimited file and holds this value.</summary>
        ''' <value><c>Value</c>is an Object property</value>
        Public Property Value() As Object
            Get
                Return _lastValue
            End Get
            <System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.LinkDemand, unrestricted:=True)> _
            Set(ByVal value As Object)
                Me.Write(value)
            End Set
        End Property

        ''' <summary>Writes a single dimension array to the file.</summary>
        ''' <param name="values">Contains the array values.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong">Values is Nothing.</exception>
        <System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.LinkDemand, unrestricted:=True)> _
        Public Sub WriteRow(ByVal values() As Double)

            If values Is Nothing Then
                Throw New System.ArgumentNullException("values")
            End If

            Dim stringRecord As New System.Text.StringBuilder
            For i As Int32 = 0 To values.Length - 1
                If i > 0 Then
                    stringRecord.Append(",")
                End If
                stringRecord.Append(values(i).ToString(Globalization.CultureInfo.CurrentCulture))
            Next
            Microsoft.VisualBasic.PrintLine(MyBase.FileNumber, stringRecord.ToString)

        End Sub

        ''' <summary>Writes a two-dimensional array to the file.</summary>
        ''' <param name="values">Contains the array values to write.  Array 
        '''   rows are written to the file as comma-separated successive lines.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong">Values is Nothing.</exception>
        <System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.LinkDemand, unrestricted:=True)> _
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId:="0#")> _
        Public Sub WriteColumns(ByVal values(,) As Double)

            ' update: use jagged arrays 

            If values Is Nothing Then
                Throw New System.ArgumentNullException("values")
            End If

            For i As Int32 = 0 To values.GetUpperBound(0)
                Dim rowRecord(values.GetUpperBound(1)) As Double
                For j As Int32 = 0 To values.GetUpperBound(1)
                    rowRecord(j) = values(i, j)
                Next
                WriteRow(rowRecord)
            Next

        End Sub

        ''' <summary>Writes a two-dimensional array to the file.</summary>
        ''' <param name="values">Contains the array values to write.  Array 
        '''   rows are written to the file as comma-separated successive lines.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong">Values is Nothing.</exception>
        <System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.LinkDemand, unrestricted:=True)> _
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId:="0#")> _
        Public Sub WriteRows(ByVal values(,) As Double)

            If values Is Nothing Then
                Throw New System.ArgumentNullException("values")
            End If

            For i As Int32 = 0 To values.GetUpperBound(1)
                Dim rowRecord(values.GetUpperBound(0)) As Double
                For j As Int32 = 0 To values.GetUpperBound(0)
                    rowRecord(j) = values(j, i)
                Next
                WriteRow(rowRecord)
            Next

        End Sub

        ''' <summary>Writes a jagged two-dimensional array to the file.</summary>
        ''' <param name="values">Contains the array values to write.  Array 
        '''   rows are writter to the file as comma-separated successive lines.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong">Values is Nothing.</exception>
        <System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.LinkDemand, unrestricted:=True)> _
        Public Sub WriteColumns(ByVal values()() As Double)

            If values Is Nothing Then
                Throw New System.ArgumentNullException("values")
            End If

            For j As Int32 = 0 To values.GetUpperBound(1)
                Dim rowRecord(values.GetUpperBound(0)) As Double
                For i As Int32 = 0 To values.GetUpperBound(0)
                    rowRecord(i) = values(i)(j)
                Next
                WriteRow(rowRecord)
            Next

        End Sub

        ''' <summary>Writes a jagged two-dimensional array to the file in columns.</summary>
        ''' <param name="values">Contains the array values to write.  Array 
        '''   rows are writter to the file as comma-separated successive lines.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong">Values is Nothing.</exception>
        <System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.LinkDemand, unrestricted:=True)> _
        Public Sub WriteRows(ByVal values()() As Double)

            If values Is Nothing Then
                Throw New System.ArgumentNullException("values")
            End If

            For i As Int32 = 0 To values.GetUpperBound(0)
                WriteRow(values(i))
            Next

        End Sub

#End Region

    End Class

End Namespace
