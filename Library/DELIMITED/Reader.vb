Namespace Delimited

    ''' <summary>This is the reader class of the delimited file class library.</summary>
    ''' <license>
    ''' (c) 2004 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="03/04/04" by="David Hary" revision="1.0.1524.x">
    ''' created.
    ''' </history>
    Public Class Reader

        ' based on the File inheritable class
        Inherits isr.IO.Delimited.File

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        Public Sub New()

            ' instantiate the base class
            MyBase.New()

        End Sub

        ''' <summary>Constructs this class.</summary>
        ''' <param name="instanceName">Specifies the name of the instance.</param>
        ''' <remarks>Use this constructor to instantiate this class
        '''   and set the instance name, which is useful in tracing</remarks>
        Public Sub New( _
          ByVal instanceName As String)

            ' instantiate the base class
            MyBase.New(instanceName)

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            If Not MyBase.IsDisposed Then

                Try

                    If disposing Then

                        ' dispose of managed resources

                    End If

                    ' Free shared unmanaged resources

                Finally

                    ' Invoke the base class dispose method
                    MyBase.Dispose(disposing)

                End Try

            End If

        End Sub

#End Region

#Region " METHODS  and  PROPERTIES "

        ''' <summary>opens the file.</summary>
        ''' <returns>A Boolean data type</returns>
        ''' <exception cref="OperationOpenException" guarantee="strong"></exception>
        ''' <remarks>Use this method to open the instance.  The method returns true if success or 
        '''   false if it failed opening the instance.</remarks>
        <System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.LinkDemand, unrestricted:=True)> _
      Public Function [Open]() As Boolean

            Try

                ' open the file for reading
                MyBase.FileNumber = File.NewFileNumber
                Microsoft.VisualBasic.FileOpen(MyBase.FileNumber, MyBase.FilePathName, Microsoft.VisualBasic.OpenMode.Input)
                Return True

            Catch ex As System.IO.IOException

                ' close to meet strong guarantees
                Try
                    Me.Close()
                Finally
                End Try

                ' catch an io exception so that we can return our open error handle
                Throw New isr.IO.IOException(String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "{0} Failed opening Delimited File for reading.", Me.InstanceName), ex)

            Catch

                ' close to meet strong guarantees
                Try
                    Me.Close()
                Finally
                End Try

                ' throw an exception
                Throw

            End Try

        End Function

        Private _lastValue As Object = String.Empty
        ''' <summary>
        ''' Returns the last value read.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property LastValue() As Object
            Get
                Return _lastValue
            End Get
        End Property

        ''' <summary>Reads a value from the delimited file.</summary>
        ''' <returns>An Object</returns>
        <System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.LinkDemand, unrestricted:=True)> _
        Public Function ReadObject() As Object
            If MyBase.IsEndOfFile Then
                ' if end of file, raise an exception
                Throw New isr.IO.EndOfFileException
            Else
                Microsoft.VisualBasic.Input(MyBase.FileNumber, _lastValue)
                Return _lastValue
            End If
        End Function

        ''' <summary>Reads a value from the delimited file.</summary>
        ''' <value><c>Value</c>is an Object readonly property</value>
        ''' <remarks>Use this property to read a value from the delimited file.</remarks>
        Public ReadOnly Property Value() As Object
            <System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.LinkDemand, unrestricted:=True)> _
            Get
                Return Me.ReadObject()
            End Get
        End Property

        ''' <summary>
        ''' Returns an array of row data.
        ''' </summary>
        ''' <param name="elementCount">Specifies the number of elements to tread.</param>
        ''' <exception cref="ArgumentOutOfRangeException" guarantee="strong">element count is negative.</exception>
        <System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.LinkDemand, unrestricted:=True)> _
        Public Function ReadRowDouble(ByVal elementCount As Int32) As Double()

            If elementCount < 0 Then
                Dim message As String = "Array size specified as {0} must be non-negative."
                message = String.Format(Globalization.CultureInfo.CurrentCulture, message, elementCount)
                Throw New ArgumentOutOfRangeException("elementCount", elementCount, message)
            End If

            If elementCount = 0 Then

                ' return the empty array 
                Dim data() As Double = {}
                Return data

            Else

                Dim record As String = Microsoft.VisualBasic.LineInput(MyBase.FileNumber)
                If record.Length > 0 Then

                    ' if we have data, get an array
                    Dim stringValues As String() = record.Split(","c)

                    ' allow as many elements as requested or present.
                    elementCount = Math.Min(elementCount, stringValues.Length)

                    ' allocate data array
                    Dim data(elementCount - 1) As Double

                    For i As Int32 = 0 To elementCount - 1
                        data(i) = Convert.ToDouble(stringValues(i), Globalization.CultureInfo.CurrentCulture)
                    Next
                    Return data

                Else

                    ' return the empty array 
                    Dim data() As Double = {}
                    Return data

                End If

            End If

        End Function

        ''' <summary>
        ''' Returns an array of row data.
        ''' </summary>
        ''' <exception cref="ArgumentOutOfRangeException" guarantee="strong">element count is negative.</exception>
        <System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.LinkDemand, unrestricted:=True)> _
        Public Function ReadRowDouble() As Double()

            Dim record As String = Microsoft.VisualBasic.LineInput(MyBase.FileNumber)
            If record.Length > 0 Then

                ' if we have data, get an array
                Dim stringValues As String() = record.Split(","c)

                ' allocate data array
                Dim data(stringValues.Length - 1) As Double

                For i As Int32 = 0 To stringValues.Length - 1
                    data(i) = Convert.ToDouble(stringValues(i), Globalization.CultureInfo.CurrentCulture)
                Next
                Return data

            Else

                ' return the empty array 
                Dim data() As Double = {}
                Return data

            End If

        End Function

        ''' <summary>
        ''' Returns file rows as a two-dimensional array.
        ''' </summary>
        <System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.LinkDemand, unrestricted:=True)> _
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId:="Return")> _
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1814:PreferJaggedArraysOverMultidimensional", MessageId:="Body")> _
        Public Function ReadRowsDoubleArray() As Double(,)

            Dim data(,) As Double = {}
            Dim columnIndex As Int32
            Do While Not MyBase.IsEndOfFile
                Dim oneDimensionValues As Double() = ReadRowDouble()
                If (oneDimensionValues IsNot Nothing) AndAlso oneDimensionValues.Length > 0 Then
                    If data.Length = 0 Then
                        ReDim data(oneDimensionValues.Length - 1, columnIndex)
                    ElseIf data.GetUpperBound(1) < columnIndex Then
                        ReDim Preserve data(data.GetUpperBound(0), columnIndex)
                    End If
                    If data IsNot Nothing Then
                        For i As Int32 = 0 To Math.Min(data.GetUpperBound(0), oneDimensionValues.Length - 1)
                            data(i, columnIndex) = Convert.ToDouble(oneDimensionValues(i))
                        Next
                    End If
                End If
                columnIndex += 1
            Loop
            Return data
        End Function

        ''' <summary>
        ''' Return file rows as a jagged two-dimensional array allowing each row
        ''' to be of different length.
        ''' </summary>
        <System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.LinkDemand, unrestricted:=True)> _
        Public Function ReadRowsDouble() As Double()()

            Dim data()() As Double = {}
            Dim rowIndex As Int32
            Do While Not MyBase.IsEndOfFile
                Dim oneDimensionValues As Double() = ReadRowDouble()
                If (oneDimensionValues IsNot Nothing) AndAlso oneDimensionValues.Length > 0 Then
                    ReDim data(rowIndex)(oneDimensionValues.Length - 1)
                    For i As Int32 = 0 To Math.Min(data.GetUpperBound(0), oneDimensionValues.Length - 1)
                        data(rowIndex)(i) = oneDimensionValues(i)
                    Next
                End If
                rowIndex += 1
            Loop
            Return data

        End Function

        ''' <summary>
        ''' Return file rows as a jagged array allowing each row
        ''' to be of different length.
        ''' </summary>
        Public Function ReadAllRows() As String()()

            Dim data()() As String = {}

            Using MyReader As New Microsoft.VisualBasic.FileIO.TextFieldParser(MyBase.FilePathName)

                MyReader.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited
                MyReader.Delimiters = New String() {","}
                'MyReader.HasFieldsEnclosedInQuotes'
                Dim currentRow As String()
                'Loop through all of the fields in the file. 
                'If any lines are corrupt, report an error and continue parsing. 
                While Not MyReader.EndOfData
                    currentRow = MyReader.ReadFields()
                    If (currentRow IsNot Nothing) AndAlso currentRow.Length > 0 Then
                        ReDim data(data.GetLength(0))(currentRow.Length - 1)
                        data(data.GetUpperBound(0)) = currentRow
                    End If
                End While

            End Using

            Return data

        End Function

#End Region

    End Class

End Namespace
