Namespace Cryptography

    ''' <summary>Creates and verifies XML signatures.</summary>
    ''' <remarks>Use this class to sign data using XML signature algorithms.</remarks>
    ''' <license>
    ''' (c) 2003 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="06/17/03" by="David Hary" revision="1.0.1263.x">
    ''' created.
    ''' </history>
    Public Class XmlSign

        Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        Protected Sub New()

            If Me._rsaSigningKey IsNot Nothing Then
                Me._rsaSigningKey.Clear()
            End If

        End Sub

        ''' <summary>Constructs this class.</summary>
        ''' <param name="instanceName">Specifies the name of the instance.</param>
        ''' <remarks>Use this constructor to instantiate this class
        '''   and set the instance name, which is useful in tracing</remarks>
        Public Sub New(ByVal instanceName As String)

            ' instantiate the base class
            Me.New()
            _instanceName = instanceName

        End Sub

        ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        ''' <remarks>Do not make this method overridable (virtual) because a derived 
        '''   class should not be able to override this method.</remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        Private _isDisposed As Boolean
        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
        ''' provided proper implementation.
        ''' </summary>
        Protected Property IsDisposed() As Boolean
            Get
                Return _isDisposed
            End Get
            Private Set(ByVal value As Boolean)
                _isDisposed = value
            End Set
        End Property

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                Try

                    If disposing Then

                        ' Free managed resources when explicitly called
                        _statusMessage = String.Empty
                        _instanceName = String.Empty

                    End If

                    ' Free shared unmanaged resources

                Finally

                    ' set the sentinel indicating that the class was disposed.
                    Me.IsDisposed = True

                End Try

            End If

        End Sub

        ''' <summary>This destructor will run only if the Dispose method 
        '''   does not get called. It gives the base class the opportunity to 
        '''   finalize. Do not provide destructors in types derived from this class.</summary>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal in terms of
            ' readability and maintainability.
            Dispose(False)
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Sub

#End Region

#Region " BASE METHODS AND PROPERTIES "

        ''' <summary>Overrides ToString returning the instance name if not empty.</summary>
        ''' <remarks>Use this method to return the instance name. If instance name is not set, 
        '''   returns the base class ToString value.</remarks>
        Public Overrides Function ToString() As String
            If String.IsNullOrEmpty(Me._instanceName) Then
                Return MyBase.ToString
            Else
                Return Me._instanceName
            End If
        End Function

        Private _isOpen As Boolean
        ''' <summary>Gets or sets the opened status of the instance.</summary>
        ''' <value><c>IsOpen</c> is a <see cref="Boolean"/> property that is True if the 
        '''   instance is open.</value>
        Public Property IsOpen() As Boolean
            Get
                Return _isOpen
            End Get
            Set(ByVal value As Boolean)
                _isOpen = value
            End Set
        End Property

        Private _instanceName As String = String.Empty
        ''' <summary>Gets or sets the name given to an instance of this class.</summary>
        ''' <value><c>InstanceName</c> is a String property.</value>
        ''' <remarks>If instance name is not set, returns .ToString.</remarks>
        ''' <history date="11/23/04" by="David Hary" revision="1.0.1788.x">
        '''   Correct code not to get instance name from .ToString but from MyBase.ToString
        '''   so that calling this method from the child class will not break the rule of
        '''   calling overridable methods from the constructor.
        ''' </history>
        Public Property InstanceName() As String
            Get
                If Not String.IsNullOrEmpty(_instanceName) Then
                    Return _instanceName
                Else
                    Return MyBase.ToString
                End If
            End Get
            Set(ByVal value As String)
                _instanceName = value
            End Set
        End Property

        Private _statusMessage As String = String.Empty
        ''' <summary>Gets or sets the status message.</summary>
        ''' <value>A <see cref="System.String">String</see>.</value>
        ''' <remarks>Use this property to get the status message generated by the object.</remarks>
        Public Property StatusMessage() As String
            Get
                Return _statusMessage
            End Get
            Set(ByVal value As String)
                _statusMessage = value
            End Set
        End Property

#End Region

#Region " SHARED "

        ''' <summary>Adds a resource to the signature file.</summary>
        ''' <param name="resourceData">
        '''   is an Object expression that specifies the data to add.</param>
        ''' <param name="resourceId">Specifies the ID of the resource to add.</param>
        ''' <param name="xmlSignature">
        '''   is a System.Security.Cryptography.Xml.SignedXml object that has the
        '''   xmlSignature and to which the data is added.</param>
        ''' <returns>A boolean data type that is set true if the rresource
        '''   was added and properly referenced.</returns>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        ''' <remarks>Use this method to add resource specified by its data and id to the signed 
        '''   data set.</remarks>
        Public Overloads Shared Function AddResource( _
          ByVal resourceData As Xml.XmlNodeList, _
          ByVal resourceId As String, _
          ByVal xmlSignature As System.Security.Cryptography.Xml.SignedXml) As Boolean

            If resourceData Is Nothing Then
                Throw New System.ArgumentNullException("resourceData")
            End If
            If resourceId Is Nothing Then
                Throw New System.ArgumentNullException("resourceId")
            End If
            If xmlSignature Is Nothing Then
                Throw New System.ArgumentNullException("xmlSignature")
            End If

            ' Create a data object to hold the data to sign.
            Dim xmlDataObject As System.Security.Cryptography.Xml.DataObject = New System.Security.Cryptography.Xml.DataObject

            ' set the data and id
            xmlDataObject.Data = resourceData
            xmlDataObject.Id = resourceId

            ' Add the data object to the signature.
            xmlSignature.AddObject(xmlDataObject)

            ' Create a reference to be able to package everything into the message.
            Dim xmlReference As New System.Security.Cryptography.Xml.Reference
            xmlReference.Uri = String.Format(Globalization.CultureInfo.CurrentCulture, _
                  "#{0}", xmlDataObject.Id)

            ' Add the reference to the message
            xmlSignature.AddReference(xmlReference)

            ' return true
            Return True

        End Function

        ''' <summary>This shared method creates an XML resource document from the text data to sign.</summary>
        ''' <param name="textData">Specifies the data to encapsulate in the XML document.</param>
        ''' <param name="prefixName">Specifies prefix for the element name which to create.
        '''   This can be empty.</param>
        ''' <param name="elementName">Specifies the name of the element which to create.</param>
        ''' <param name="nameSpaceUri">Specifies the namespace of the element to be created, 
        '''   e.g., "http://www.isr.cc/security".</param>
        ''' <returns>A Boolean data type</returns>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        ''' <remarks>Use this method to create an XML document with data to sign.</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1059:MembersShouldNotExposeCertainConcreteTypes", MessageId:="System.Xml.XmlNode")> _
        Public Shared Function CreateResource(ByVal textData As String, _
          ByVal prefixName As String, _
          ByVal elementName As String, _
          ByVal namespaceUri As System.Uri) As System.Xml.XmlDocument

            If prefixName Is Nothing Then
                Throw New System.ArgumentNullException("prefixName")
            End If
            If elementName Is Nothing Then
                Throw New System.ArgumentNullException("elementName")
            End If
            If namespaceUri Is Nothing Then
                Throw New System.ArgumentNullException("namespaceUri")
            End If

            ' Create example data to sign.
            Dim xmlDoc As New System.Xml.XmlDocument
            Dim xmlNode As System.Xml.XmlNode = xmlDoc.CreateNode( _
                System.Xml.XmlNodeType.Element, prefixName, elementName, namespaceUri.ToString)
            xmlNode.InnerText = textData
            xmlDoc.AppendChild(xmlNode)
            Return xmlDoc

        End Function

        ''' <summary>Returns the data string containing the signed resource.</summary>
        ''' <param name="xmlSignature">
        '''   is a System.Security.Cryptography.Xml.SignedXml that specifies the signature.</param>
        ''' <param name="resourceId">Specifies the ID of the resource in the signature.</param>
        ''' <returns>A Boolean data type</returns>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        ''' <remarks>Use this method to get the data including the signed resource is stored.</remarks>
        Public Overloads Shared Function GetResourceData( _
          ByVal xmlSignature As System.Security.Cryptography.Xml.SignedXml, _
          ByVal resourceId As String) As String

            If resourceId Is Nothing Then
                Throw New System.ArgumentNullException("resourceId")
            End If
            If xmlSignature Is Nothing Then
                Throw New System.ArgumentNullException("xmlSignature")
            End If

            ' get the resource element and return the text
            Return XmlSign.GetResourceElement(xmlSignature, resourceId).OuterXml

        End Function

        ''' <summary>Returns the signed XML element containing the signed resource.</summary>
        ''' <param name="xmlSignature">
        '''   is a System.Security.Cryptography.Xml.SignedXml that specifies the signature.</param>
        ''' <param name="resourceId">Specifies the ID of the resource in the signature.</param>
        ''' <returns>A Boolean data type</returns>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        ''' <remarks>Use this method to get the XML data element where the signed resource is stored.</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1059:MembersShouldNotExposeCertainConcreteTypes", MessageId:="System.Xml.XmlNode")> _
        Public Overloads Shared Function GetResourceElement( _
          ByVal xmlSignature As System.Security.Cryptography.Xml.SignedXml, _
          ByVal resourceId As String) As System.Xml.XmlElement

            If resourceId Is Nothing Then
                Throw New System.ArgumentNullException("resourceId")
            End If
            If xmlSignature Is Nothing Then
                Throw New System.ArgumentNullException("xmlSignature")
            End If

            ' Load the XML.
            Dim xmlDoc As New System.Xml.XmlDocument
            xmlDoc.PreserveWhitespace = False
            xmlDoc.LoadXml(xmlSignature.GetXml.OuterXml)
            Return xmlSignature.GetIdElement(xmlDoc, resourceId)

        End Function

        ''' <summary>Verifies the signature XML elements.</summary>
        ''' <param name="xmlElement">
        '''   is a System.Xml.XmlElement expression that specifies the XML signature element.</param>
        ''' <returns>A boolean data type that is set true if the signature 
        '''   verified.</returns>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        ''' <remarks>Use this method to verify a signature XML element.</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1059:MembersShouldNotExposeCertainConcreteTypes", MessageId:="System.Xml.XmlNode")> _
        Public Overloads Shared Function IsVerifySignature(ByVal xmlElement As System.Xml.XmlElement) As Boolean

            If xmlElement Is Nothing Then
                Throw New System.ArgumentNullException("xmlElement")
            End If

            ' Load the XML element into an xml document
            Dim xmlDoc As New System.Xml.XmlDocument
            xmlDoc.PreserveWhitespace = True
            ' xmlDoc.LoadXml(xmlElement.OuterXml)
            xmlDoc.LoadXml(xmlElement.OuterXml)

            ' Create a SignedXml
            Dim signedXml As System.Security.Cryptography.Xml.SignedXml = XmlSign.LoadSignature(xmlDoc)

            ' return true if checked
            Return signedXml.CheckSignature()

        End Function

        ''' <summary>Verifies a signature file.</summary>
        ''' <param name="xmlSignatureFilePathName">Specifies the name of the signature file.</param>
        ''' <returns>A <see cref="System.Boolean"/> value that is true if the signature was 
        '''   verified.</returns>
        ''' <remarks>Use this method to verify a signature file using a key stored in the
        '''   signature file.</remarks>
        Public Shared Function IsVerifySignatureFile(ByVal xmlSignatureFilePathName As String _
          ) As Boolean

            If xmlSignatureFilePathName IsNot Nothing Then
                ' read the signature and return true if checked.
                Return XmlSign.ReadSignatureFile(xmlSignatureFilePathName).CheckSignature()
            End If

        End Function

        ''' <summary>Verifies a signature file.</summary>
        ''' <param name="xmlSignatureFilePathName">Specifies the name of the signature file.</param>
        ''' <param name="key">
        '''   is an System.Security.Cryptography.KeyedHashAlgorithm object specifying the
        '''   key which used to sign the file.</param>
        ''' <returns>A <see cref="System.Boolean"/> value that is true if the signature was 
        '''   verified.</returns>
        ''' <remarks>Use this method to verify a signature file using a key..</remarks>
        Public Shared Function IsVerifySignatureFile(ByVal xmlSignatureFilePathName As String, _
          ByVal key As System.Security.Cryptography.KeyedHashAlgorithm) As Boolean

            If Not (xmlSignatureFilePathName Is Nothing OrElse key Is Nothing) Then
                ' read the signature and return true if checked.
                Return XmlSign.ReadSignatureFile(xmlSignatureFilePathName).CheckSignature(key)
            End If

        End Function

        ''' <summary>Load a signature from an XML document.</summary>
        ''' <param name="xmlDoc">Specifies the name of the signature file.</param>
        ''' <returns>A System.Security.Cryptography.Xml.SignedXml
        '''   object that includes the signature.</returns>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        ''' <remarks>Use this method to read a signature file.</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1059:MembersShouldNotExposeCertainConcreteTypes", MessageId:="System.Xml.XmlNode")> _
        Public Shared Function LoadSignature(ByVal xmlDoc As System.Xml.XmlDocument) _
        As System.Security.Cryptography.Xml.SignedXml

            If xmlDoc Is Nothing Then
                Throw New System.ArgumentNullException("xmlDoc")
            End If

            ' Find the "Signature" node and create a new XmlNodeList object.
            Dim xmlNodeList As System.Xml.XmlNodeList = xmlDoc.GetElementsByTagName("Signature")

            ' Create a new SignedXml object and pass it the XML document class.
            Dim signedXml As New System.Security.Cryptography.Xml.SignedXml

            ' Load the signature node.
            signedXml.LoadXml(CType(xmlNodeList(0), System.Xml.XmlElement))

            ' return the signature 
            Return signedXml

        End Function

        ''' <summary>Reads a signature file.</summary>
        ''' <param name="xmlSignatureFilePathName">Specifies the name of the signature file.</param>
        ''' <returns>A System.Security.Cryptography.Xml.SignedXml
        '''   object that includes the signature.</returns>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        ''' <remarks>Use this method to read a signature file.</remarks>
        Public Shared Function ReadSignatureFile(ByVal xmlSignatureFilePathName As String) _
        As System.Security.Cryptography.Xml.SignedXml

            If xmlSignatureFilePathName Is Nothing Then
                Throw New System.ArgumentNullException("xmlSignatureFilePathName")
            End If

            ' Create a new XML document.
            Dim xmlDoc As New System.Xml.XmlDocument

            ' Load the passed XML file into the document.
            xmlDoc.PreserveWhitespace = True
            xmlDoc.Load(xmlSignatureFilePathName)

            ' load and return the XML signature
            Return XmlSign.LoadSignature(xmlDoc)

        End Function

        ''' <summary>Signs an XML file using an RSA key.</summary>
        ''' <param name="resourceData">
        '''   is an Object expression that specifies the resource data which to sign.</param>
        ''' <param name="resourceId">Specifies the ID to associate with the signed resource.</param>
        ''' <param name="xmlSignatureFilePathName">Specifies the name of the output signature file.</param>
        ''' <returns>A Boolean data type</returns>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        ''' <remarks>Use this method to sign a detached resource data and save the signature to a 
        '''   signature file.</remarks>
        Public Shared Function SignResource(ByVal resourceData As Xml.XmlNodeList, _
          ByVal resourceId As String, _
          ByVal xmlSignatureFilePathName As String) _
        As System.Security.Cryptography.Xml.SignedXml

            If resourceId Is Nothing Then
                Throw New System.ArgumentNullException("resourceId")
            End If
            If xmlSignatureFilePathName Is Nothing Then
                Throw New System.ArgumentNullException("xmlSignatureFilePathName")
            End If

            ' Create a SignedXml object.
            Dim signedXml As New System.Security.Cryptography.Xml.SignedXml

            ' Add the xml data to the signed XML
            XmlSign.AddResource(resourceData, resourceId, signedXml)

            ' instantiate an RSA signing key
            Dim rsaSigningKey As System.Security.Cryptography.RSA = System.Security.Cryptography.RSA.Create()

            ' assign the key as a signing key.
            signedXml.SigningKey = rsaSigningKey

            ' Add a KeyInfo to the signature
            Dim thisKeyInfo As New System.Security.Cryptography.Xml.KeyInfo
            thisKeyInfo.AddClause(New System.Security.Cryptography.Xml.RSAKeyValue(rsaSigningKey))
            signedXml.KeyInfo = thisKeyInfo

            ' Compute the signature.
            signedXml.ComputeSignature()

            ' save the signature
            XmlSign.WriteSignatureFile(signedXml, xmlSignatureFilePathName)

            ' return the signed xml
            Return signedXml

        End Function

        ''' <summary>Signs a resource.</summary>
        ''' <param name="resourceUri">Specifies the location of the resource which to sign.</param>
        ''' <param name="xmlSignatureFilePathName">Specifies the name of the output signature file.</param>
        ''' <param name="key">is an System.Security.Cryptography.KeyedHashAlgorithm object specifying the
        '''   key which to use for the signature.</param>
        ''' <returns>A Boolean data type</returns>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        ''' <remarks>Use this method to Sign a resource (e.g., "http://www.microsoft.com") 
        '''   and save the signature in a new file.  the resource can be an XML file.</remarks>
        Public Shared Function SignResource(ByVal resourceUri As System.Uri, _
          ByVal xmlSignatureFilePathName As String, _
          ByVal key As System.Security.Cryptography.KeyedHashAlgorithm) _
        As System.Security.Cryptography.Xml.SignedXml

            If xmlSignatureFilePathName Is Nothing Then
                Throw New System.ArgumentNullException("xmlSignatureFilePathName")
            End If
            If key Is Nothing Then
                Throw New System.ArgumentNullException("key")
            End If
            If resourceUri Is Nothing Then
                Throw New System.ArgumentNullException("resourceUri")
            End If

            ' Create a SignedXml object.
            Dim signedXml As New System.Security.Cryptography.Xml.SignedXml

            ' Create a reference to be signed.
            Dim reference As New System.Security.Cryptography.Xml.Reference

            ' Add the passed URI to the reference object.
            reference.Uri = resourceUri.ToString

            ' Add a transformation if the URI is an XML file.
            If resourceUri.ToString.EndsWith("xml") Then
                reference.AddTransform(New System.Security.Cryptography.Xml.XmlDsigC14NTransform)
            End If

            ' Add the reference to the SignedXml object.
            signedXml.AddReference(reference)

            ' Compute the signature.
            signedXml.ComputeSignature(key)

            ' save the signature
            XmlSign.WriteSignatureFile(signedXml, xmlSignatureFilePathName)

            ' return the signed xml
            Return signedXml

        End Function

        ''' <summary>Signs an XML file using an RSA key.</summary>
        ''' <param name="xmlFileName">Specifies the XML file name which to sign.</param>
        ''' <param name="resourceId">Specifies the ID to associate with the signed resource.</param>
        ''' <param name="xmlSignatureFilePathName">Specifies the name of the output signature file.</param>
        ''' <returns>A Boolean data type</returns>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        ''' <remarks>Use this method to sign an XML file and save the signature to a 
        '''   signature file.</remarks>
        Public Shared Function SignResource(ByVal xmlFileName As String, _
          ByVal resourceId As String, _
          ByVal xmlSignatureFilePathName As String) _
        As System.Security.Cryptography.Xml.SignedXml

            If xmlSignatureFilePathName Is Nothing Then
                Throw New System.ArgumentNullException("xmlSignatureFilePathName")
            End If
            If resourceId Is Nothing Then
                Throw New System.ArgumentNullException("resourceId")
            End If

            ' instantiate a document to read the XML data
            Dim xmlDoc As New System.Xml.XmlDocument

            ' read the XML data
            xmlDoc.Load(xmlFileName)

            ' sign the XML data
            Return XmlSign.SignResource(xmlDoc.ChildNodes, resourceId, xmlSignatureFilePathName)

        End Function

        ''' <summary>Signs a resource.</summary>
        ''' <param name="xmlSignature">
        '''   is a System.Security.Cryptography.Xml.SignedXml that specifies the signature.</param>
        ''' <param name="xmlSignatureFilePathName">Specifies the name of the output signature file.</param>
        ''' <returns>A Boolean data type</returns>
        ''' <remarks>Use this method to save the signature to file.</remarks>
        Public Shared Function WriteSignatureFile( _
            ByVal xmlSignature As System.Security.Cryptography.Xml.SignedXml, _
            ByVal xmlSignatureFilePathName As String) _
          As Boolean

            If Not (xmlSignature Is Nothing OrElse xmlSignatureFilePathName Is Nothing) Then

                ' Save the signed XML document to a file specified using the passed string.
                Dim xmltw As New System.Xml.XmlTextWriter(xmlSignatureFilePathName, _
                    New System.Text.UTF8Encoding(False))
                xmlSignature.GetXml.WriteTo(xmltw)
                xmltw.Close()

                ' return the signed xml
                Return True

            End If

        End Function

#End Region

#Region " METHODS "

        ''' <summary>opens this instance.</summary>
        ''' <returns>A Boolean data type</returns>
        ''' <exception cref="OperationOpenException" guarantee="strong"></exception>
        ''' <remarks>Use this method to open the instance.</remarks>
        Public Overridable Function [Open]() As Boolean

            Try

                ' clear the status message
                Me.StatusMessage = String.Empty

                ' instantiate the signed XML message.
                _signedXml = New System.Security.Cryptography.Xml.SignedXml

                Me.IsOpen = True
                Return Me.IsOpen

            Catch ex As isr.IO.BaseException

                ' close to meet strong guarantees
                Try
                    Me.Close()
                Finally
                End Try

                ' throw an exception
                Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "{0} failed opening: {1}", Me.InstanceName, ex.Message)
                Throw New isr.IO.OperationOpenException(Me.StatusMessage, ex)

            End Try

        End Function

        ''' <summary>Closes the instance.</summary>
        ''' <returns>A Boolean data type</returns>
        ''' <exception cref="OperationCloseException" guarantee="strong"></exception>
        ''' <remarks>Use this method to close the instance.</remarks>
        Public Overridable Function [Close]() As Boolean

            Try

                ' clear the status message
                Me.StatusMessage = String.Empty

                ' terminate the signing key
                _rsaSigningKey = Nothing

                ' terminate the signed XML data object
                _signedXml = Nothing

                Me.IsOpen = False
                Return Not Me.IsOpen

            Catch ex As isr.IO.BaseException
                ' throw an exception
                Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "{0} operation failed closing: {1}", Me.InstanceName, ex.Message)
                Throw New isr.IO.OperationCloseException(Me.StatusMessage, ex)
            End Try

        End Function

        ''' <summary>Adds data to the signature file.</summary>
        ''' <param name="resourceData">
        '''   is an Object expression that specifies the data to add.</param>
        ''' <param name="resourceId">Specifies the ID of the data to add.</param>
        ''' <returns>A boolean data type that is set true if the data
        '''   was added and properly referenced.</returns>
        ''' <remarks>Use this method to add data to the signed data set.</remarks>
        Public Overloads Function AddResource(ByVal resourceData As Xml.XmlNodeList, ByVal resourceId As String) As Boolean

            If resourceData Is Nothing OrElse resourceId Is Nothing Then

                ' add the resource data.
                If XmlSign.AddResource(resourceData, resourceId, _signedXml) Then
                    ' return true
                    Return True
                Else
                    ' set the status message and return false
                    Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
                        "{0} failed adding resource data.", Me.InstanceName)
                    Return False
                End If

            Else

                Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "{0} Resource Data or Resource ID is nothing.", Me.InstanceName)
                Return False

            End If

        End Function

        ''' <summary>Adds data to the signature file.</summary>
        ''' <param name="xmlFileName">Specifies the XML file name where the data resides.</param>
        ''' <param name="resourceId">Specifies the ID of the data to add.</param>
        ''' <returns>A boolean data type that is set true if the data
        '''   was added and properly referenced.</returns>
        ''' <remarks>Use this method to add XML data to the signed data set.</remarks>
        Public Overloads Function AddResource(ByVal xmlFileName As String, ByVal resourceId As String) As Boolean

            If xmlFileName Is Nothing OrElse resourceId Is Nothing Then
                Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "{0} XML File Name or Resource ID is nothing.", Me.InstanceName)
                Return False
            End If

            ' instantiate a document to read the XML data
            Dim xmlDoc As New System.Xml.XmlDocument

            ' read the XML data
            xmlDoc.Load(xmlFileName)

            ' Add the xml data
            If Me.AddResource(xmlDoc.ChildNodes, resourceId) Then
                Return True
            Else
                ' set the status message and return false
                Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "{0} failed adding XML data.", Me.InstanceName)
                Return False
            End If

        End Function

        ''' <summary>Generates the signature.</summary>
        ''' <returns>A boolean data type that is set true if the signature 
        '''   generated correctly.</returns>
        ''' <remarks>Use this method to generate the signature.</remarks>
        Public Function GenerateSignature() As Boolean

            ' instantiate an RSA signing key
            If _rsaSigningKey Is Nothing Then
                _rsaSigningKey = System.Security.Cryptography.RSA.Create()
            End If

            ' assign the key as a signing key.
            _signedXml.SigningKey = _rsaSigningKey

            ' Add a KeyInfo to the signature
            Dim thisKeyInfo As New System.Security.Cryptography.Xml.KeyInfo
            thisKeyInfo.AddClause(New System.Security.Cryptography.Xml.RSAKeyValue(_rsaSigningKey))
            _signedXml.KeyInfo = thisKeyInfo

            ' Compute the signature.
            _signedXml.ComputeSignature()

            Return True

        End Function

        ''' <summary>Parses the signature and returns the resource data.</summary>
        ''' <param name="resourceId">Specifies the ID of the data to add.</param>
        ''' <returns>A Boolean data type</returns>
        ''' <remarks>Use this method to get the resource data from the signature based on the resource id.</remarks>
        Public Overloads Function GetResourceData(ByVal resourceId As String) As String

            If resourceId Is Nothing Then
                Return String.Empty
            Else
                ' return the outer XML of the resource
                Return XmlSign.GetResourceElement(_signedXml, resourceId).OuterXml
            End If

        End Function

        ''' <summary>Returns the resource XML data element.</summary>
        ''' <param name="resourceId">Specifies the ID of the data to add.</param>
        ''' <returns>Returns the resource encapsulated in the resource XML 
        '''   Element data element.</returns>
        ''' <remarks>Use this method to get the resource encpaulsated in the signature
        '''   in its XML data element.</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1059:MembersShouldNotExposeCertainConcreteTypes", MessageId:="System.Xml.XmlNode")> _
        Public Overloads Function GetResourceElement(ByVal resourceId As String) As System.Xml.XmlElement

            If resourceId Is Nothing Then
                Return Nothing
            Else
                ' return the outer XML of the resource
                Return XmlSign.GetResourceElement(_signedXml, resourceId)
            End If

        End Function

        ''' <summary>verifies the signature.</summary>
        ''' <returns>A boolean data type that is set true if the signature 
        '''   verified.</returns>
        ''' <remarks>Use this method to verify the signature.</remarks>
        Public Overloads Function IsVerifySignature() As Boolean

            Return XmlSign.IsVerifySignature(_signedXml.GetXml)

        End Function

        ''' <summary>verifies the signature.</summary>
        ''' <param name="signatureFileName">Specifies the XML file name where the data resides.</param>
        ''' <returns>A boolean data type that is set true if the signature 
        '''   verified.</returns>
        ''' <remarks>Use this method to read and verify the signature.</remarks>
        Public Overloads Function IsVerifySignature(ByVal signatureFileName As String) As Boolean

            If signatureFileName IsNot Nothing Then
                ' read the signature
                Me._signedXml = XmlSign.ReadSignatureFile(signatureFileName)

                ' return true if verified
                Return Me.IsVerifySignature()
            End If

        End Function

        ''' <summary>Reads the signature file.</summary>
        ''' <param name="xmlSignatureFilePathName">Specifies the XML file name where the data resides.</param>
        ''' <returns>A boolean data type that is set true if the signed 
        '''   information was read.</returns>
        ''' <remarks>Use this method to read the signature Information.</remarks>
        Public Function ReadSignature(ByVal xmlSignatureFilePathName As String) As Boolean

            If xmlSignatureFilePathName IsNot Nothing Then
                ' read and set internal signature object.
                _signedXml = XmlSign.ReadSignatureFile(xmlSignatureFilePathName)

                ' return true
                Return True
            End If

        End Function

        ''' <summary>Writes the signature to the XML file.</summary>
        ''' <param name="xmlSignatureFilePathName ">Specifies the name of the signature file.</param>
        ''' <returns>A boolean data type that is set true if the signed 
        '''   information was saved and signed.</returns>
        ''' <remarks>Use this method to write the signature.</remarks>
        Public Overloads Function WriteSignature(ByVal xmlSignatureFilePathName As String) As Boolean

            If xmlSignatureFilePathName IsNot Nothing Then
                Return XmlSign.WriteSignatureFile(Me._signedXml, xmlSignatureFilePathName)
            End If

        End Function

#End Region

#Region " PROPERTIES "

        Private _signedXml As System.Security.Cryptography.Xml.SignedXml
        ''' <summary>Gets or sets the signed XML.</summary>
        Public ReadOnly Property SignedXml() As System.Security.Cryptography.Xml.SignedXml
            Get
                Return _signedXml
            End Get
        End Property

        Private _rsaSigningKey As System.Security.Cryptography.RSA
        ''' <summary>Gets or sets the signing RSA key.</summary>
        Public ReadOnly Property RsaSigningKey() As System.Security.Cryptography.RSA
            Get
                Return _rsaSigningKey
            End Get
        End Property

        ''' <summary>Gets or sets the signature.</summary>
        ''' <value><c>Signature</c> is a String property that can be read from (read only).</value>
        ''' <remarks>Use this property to get the signatute.</remarks>
        Public ReadOnly Property Signature() As String
            Get
                Return _signedXml.GetXml.OuterXml
            End Get
        End Property

#End Region

#Region " EXAMPLES "

#If DEMO Then
        Private Shared Sub SampleOne()

            Try

                ' The URI to sign.
                Dim resourceToSign As System.Uri = New System.Uri("http://www.microsoft.com")

                ' The name of the file to which to save the XML signature.
                Dim xmlFileName As String = "xmlsig.xml"

                ' Generate a signing key.
                Dim Key As New System.Security.Cryptography.HMACSHA1

                Console.WriteLine("Signing: {0}", resourceToSign)

                ' Sign the detached resource and save the signature in an XML file.
                SignResource(resourceToSign, xmlFileName, Key)

                Console.WriteLine("XML signature was succesfully computed and saved to {0}.", xmlFileName)

                ' Verify the signature of the signed XML.
                Console.WriteLine("Verifying signature...")

                ' Verify the XML signature in the XML file.
                Dim result As Boolean = IsVerifySignatureFile(xmlFileName, Key)

                ' Display the results of the signature verification to the console.
                If result Then
                    Console.WriteLine("The XML signature is valid.")
                Else
                    Console.WriteLine("The XML signature is not valid.")
                End If

            Catch e As System.Security.Cryptography.CryptographicException
                Console.WriteLine(e.Message)
            End Try

        End Sub
#End If

#End Region

    End Class

End Namespace
