Namespace Cryptography

    ''' <summary>This is an Interface class for implementing token signing.</summary>
    ''' <remarks>Implement this class in Time Piece classes.</remarks>
    ''' <license>
    ''' (c) 2010 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="07/05/03" by="David Hary" revision="1.0.1281.x">
    '''   Add fileName
    ''' </history>
    ''' <history date="06/21/03" by="David Hary" revision="1.0.1267.x">
    ''' created.
    ''' </history>
    Public Interface ITokenSign

#Region " METHODS "

        ''' <summary>A must override method that is implemented in the child token signing.</summary>
        ''' <returns>A Boolean data type that is true if the token was created.</returns>
        ''' <param name="signatureFileName">Spoecifies the signature file name.</param>
        ''' <remarks>Use this method to create the signed token.</remarks>
        Function CreateToken(ByVal signatureFileName As String) As Boolean

        ''' <summary>Verifies the signed token.</summary>
        ''' <returns>A Boolean data type that is true if the token was verified.</returns>
        ''' <param name="signatureFileName">Spoecifies the signature file name.</param>
        ''' <remarks>Use this method to verify the signed token.</remarks>
        Function IsVerifyToken(ByVal signatureFileName As String) As Boolean

        ''' <summary>Reads the signed token.</summary>
        ''' <returns>A Boolean data type that is true if the token was read.</returns>
        ''' <param name="signatureFileName">Spoecifies the signature file from which to read 
        '''   the token.</param>
        ''' <remarks>Use this method to read the signed token.</remarks>
        Function ReadToken(ByVal signatureFileName As String) As Boolean

        ''' <summary>Writes the signed token.</summary>
        ''' <returns>A Boolean data type that is true if the token was written.</returns>
        ''' <param name="signatureFileName">Spoecifies the signature file to which to write 
        '''   the token.</param>
        ''' <remarks>Use this method to write the signed token.</remarks>
        Function WriteToken(ByVal signatureFileName As String) As Boolean

#End Region

    End Interface

End Namespace
