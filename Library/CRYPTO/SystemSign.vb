Namespace Cryptography

    ''' <summary>Signs system tokens.</summary>
    ''' <remarks>use this class as a Core. for simple library classes.</remarks>
    ''' <license>
    ''' (c) 2010 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="06/21/03" by="David Hary" revision="1.0.1267.x">
    ''' created.
    ''' </history>
    Public Class SystemSign

        ' based on the TokenSign inheritable class
        Inherits isr.IO.Cryptography.TokenSign

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="instanceName">Specifies the name of the object.</param>
        ''' <remarks>Use this constructor to instantiate this class
        '''   and set the instance name, which is useful in tracing</remarks>
        Public Sub New( _
            ByVal instanceName As String)

            ' instantiate the base class
            MyBase.New(instanceName)

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            If Not MyBase.IsDisposed Then

                Try

                    If disposing Then

                        ' Free managed resources when explicitly called

                    End If

                    ' Free shared unmanaged resources

                Finally

                    ' Invoke the base class dispose method
                    MyBase.Dispose(disposing)

                End Try

            End If

        End Sub

#End Region

#Region " TYPES "

        Private Const systemDriveSerialNumberTokenName As String = "SystemDriveToken"
        Private Const systemDriveSerialNumberTokenId As String = "isrSystemDriveToken"

#End Region

#Region " SHARED "

        ''' <summary>This shared method gets a system property.</summary>
        ''' <param name="systemPath">
        '''   is a String expression that identifies the system including a system name
        '''   and key (see remarks).</param>
        ''' <param name="systemProperty">
        '''   is a String expression that identifies the system property.</param>
        ''' <remarks>Use Read a system property
        '''   The following information can be derived (path/property):
        '''   Path:  "win32_logicaldisk.deviceid='c:'" 
        '''   Properties: "VolumeSerialNumber", "deviceid", (see win32_logicaldisk)
        '''   Path: "Win32_Processor.deviceid='CPU0'"
        '''   Properties: "Processorid"
        '''   Note that some devices require two keys in the path.  For these
        '''   it would be possible to get the required information from the enumerated
        '''   list.
        ''' </remarks>
        Public Shared Function GetSystemProperty(ByVal systemPath As String, _
            ByVal systemProperty As String) As String

            Dim systemEntity As New System.Management.ManagementObject(systemPath)
            systemEntity.Get()
            Return systemEntity(systemProperty).ToString

        End Function

        ''' <summary>This shared method gets a system property.</summary>
        ''' <param name="systemName">
        '''   is a String expression that identifies the system.</param>
        ''' <param name="systemProperty">
        '''   is a String expression that identifies the system property.</param>
        ''' <remarks>Use Read a system property
        '''   The following information can be derived (name/property):
        '''   Name: "Win32_Processor"
        '''   Properties: "Processorid"
        '''   Name: "Win32_BIOS"
        '''   Properties: "Version" "SMBIOSBIOSVersion"
        '''   Note that some devices require two keys in the path.  For theses
        '''   it would be possible to get the required information from the enumerated
        '''   list.
        ''' </remarks>
        Public Shared Function GetSystemPropertyFirstEnum(ByVal systemName As String, _
            ByVal systemProperty As String) As String
            Try
                Dim systemEntity As System.Management.ManagementClass
                Dim systemsAvailable As System.Management.ManagementObjectCollection
                Dim oneSystem As System.Management.ManagementObject
                systemEntity = New System.Management.ManagementClass(systemName)
                systemsAvailable = systemEntity.GetInstances()
                For Each oneSystem In systemsAvailable
                    Return oneSystem(systemProperty).ToString
                Next
                Return String.Empty
            Catch ex As isr.IO.BaseException
                Return String.Empty
            End Try
        End Function

        ''' <summary>This shared method enumerates the systems.</summary>
        ''' <param name="systemName">
        '''   is a String expression that identifies the system.</param>
        ''' <param name="systemProperty">
        '''   is a String expression that identifies the system property.</param>
        ''' <example>
        '''   <code>
        '''     Dim systemName As Object
        '''     Dim systemList As ArrayList
        '''     systemList = SystemSign.EnumerateSystems("Win32_NetworkAdapter")
        '''     me.systemsTextBox.Clear()
        '''     For Each systemName In systemList
        '''       me.systemsTextBox.AppendText(systemName.ToString)
        '''       me.systemsTextBox.AppendText(Environment.NewLine)
        '''     Next
        '''   </code>
        ''' </example>
        Public Shared Function EnumerateSystems(ByVal systemName As String, _
        ByVal systemProperty As String) As ArrayList
            Dim systemEntity As System.Management.ManagementClass
            Dim systemsAvailable As System.Management.ManagementObjectCollection
            Dim oneSystem As System.Management.ManagementObject
            Dim names As New ArrayList

            systemEntity = New System.Management.ManagementClass(systemName)
            systemsAvailable = systemEntity.GetInstances()
            names.Clear()
            For Each oneSystem In systemsAvailable
                names.Add(oneSystem(systemProperty).ToString)
            Next
            Return names
        End Function

#End Region

#Region " METHODS  and  PROPERTIES "

        ''' <summary>Creates a system token.</summary>
        ''' <returns>A Boolean data type that is true if the token was 
        '''  Created.</returns>
        ''' <remarks>Use this method to create the signed token.</remarks>
        Public Overrides Function CreateToken(ByVal signatureFileName As String) As Boolean

            Try

                ' clear status message
                Me.StatusMessage = String.Empty

                If MyBase.IsOpen Then

                    ' add file name object
                    If MyBase.AddFileNameToken(signatureFileName) Then

                        ' add object per the selected system token
                        If Me._systemTokenType = SystemTokenTypes.SystemDriveSerialNumber Then

                            ' add the System Drive Serial Number Token
                            If addSystemDriveSerialNumberToken() Then

                                ' generate the signature
                                If MyBase.XmlSignature.GenerateSignature() Then
                                    Return True
                                Else
                                    ' report failure
                                    Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
                                      "{0} failed generating token", Me.InstanceName)
                                    Return False
                                End If

                            Else

                                ' report failure
                                Me.StatusMessage = _
                                    String.Format(Globalization.CultureInfo.CurrentCulture, _
                                      "{0} failed adding system token:: {1}", _
                                      Me.InstanceName, MyBase.XmlSignature.StatusMessage)
                                Return False

                            End If

                        Else

                            ' report failure
                            Me.StatusMessage = _
                                String.Format(Globalization.CultureInfo.CurrentCulture, _
                                  "{0} failed creating token because selected token type is not supported at this time", _
                                Me.InstanceName)
                            Return False

                        End If

                    Else

                        ' report failure to add fileName token
                        Me.StatusMessage = _
                            String.Format(Globalization.CultureInfo.CurrentCulture, _
                        "{0} failed creating token because file name token failed adding:: {1}", _
                            Me.InstanceName, Me.StatusMessage)
                        Return False

                    End If
                Else

                    ' report failure
                    Me.StatusMessage = _
                        String.Format(Globalization.CultureInfo.CurrentCulture, _
                      "{0} failed creating system token because {0} was not open", _
                        Me.InstanceName)
                    Return False

                End If

            Catch ex As isr.IO.BaseException
                ' throw an exception
                Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "{0} failed creating token: {1}", Me.InstanceName, ex.Message)
                Throw New isr.IO.OperationCloseException(Me.StatusMessage, ex)
            End Try

        End Function

        ''' <summary>verifies the signed token.</summary>
        ''' <returns>A Boolean data type that is true if the token was 
        '''   verified.</returns>
        ''' <remarks>Use this method to verify the signed token.</remarks>
        Public Overrides Function IsVerifyToken(ByVal signatureFileName As String) As Boolean

            Try

                ' clear status message
                Me.StatusMessage = String.Empty

                If MyBase.IsOpen Then
                    If MyBase.XmlSignature.IsVerifySignature() Then
                        If MyBase.IsVerifyFileNameToken(signatureFileName) Then
                            If Me._systemTokenType = SystemTokenTypes.SystemDriveSerialNumber Then
                                Return verifySystemDriveSerialNumberToken()
                            End If
                        Else
                            ' report failure
                            Me.StatusMessage = _
                                String.Format(Globalization.CultureInfo.CurrentCulture, _
                          "{0} failed verifying system token signature:: {1}", _
                                Me.InstanceName, Me.StatusMessage)
                            Return False
                        End If
                    Else
                        ' report failure
                        Me.StatusMessage = _
                            String.Format(Globalization.CultureInfo.CurrentCulture, _
                        "{0} failed verifying system token signature", _
                            Me.InstanceName)
                        Return False
                    End If
                Else
                    ' report failure
                    Me.StatusMessage = _
                        String.Format(Globalization.CultureInfo.CurrentCulture, _
                      "{0} failed verifying system token because {0} was not open", _
                        Me.InstanceName)
                    Return False
                End If
            Catch ex As isr.IO.BaseException
                ' report failure
                Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "{0} failed verifying token: {1}", Me.InstanceName, ex.Message)
                Return False
            End Try
        End Function

        Private _filePathName As String = String.Empty
        ''' <summary>Gets or sets the file path name.</summary>
        ''' <value><c>FilePathName</c> is a String property.</value>
        ''' <remarks>Use this property to get or set the file name.</remarks>
        Public Property FilePathName() As String
            Get
                Return _filePathName
            End Get
            Set(ByVal value As String)
                _filePathName = value
            End Set
        End Property

        Private _systemTokenType As SystemTokenTypes = SystemTokenTypes.SystemDriveSerialNumber
        ''' <summary>Gets or sets the system token type.</summary>
        ''' <value><c>SystemTokenType</c> is a SystemTokenTypes enumerated property that can be read from 
        '''   or written too (read or write).</value>
        ''' <remarks>Use this property to get or set the type of token(s) the System Token will
        '''  Create and sign.</remarks>
        Public Property SystemTokenType() As SystemTokenTypes
            Get
                Return _systemTokenType
            End Get
            Set(ByVal value As SystemTokenTypes)
                _systemTokenType = value
            End Set
        End Property

#End Region

#Region " PRIVATE  and  PROTECTED "

        ''' <summary>Adds a system token data object based on the HDD serial 
        '''   number.</summary>
        ''' <exception cref="isr.IO.BaseException" guarantee="strong">
        '''   Failed initializing.</exception>
        ''' <remarks>Use this method to add an HDD serial number system token.</remarks>
        Private Function addSystemDriveSerialNumberToken() As Boolean

            Try

                ' get serial number of system drive
                Dim systemDriveSNToken As String = Me.getSystemDriveSerialNumberToken()

                ' create a resource to hold the signed data
                Dim xmlDoc As System.Xml.XmlDocument = _
                    XmlSign.CreateResource(systemDriveSNToken, "", systemDriveSerialNumberTokenName, New System.Uri("MyBase.tokenNamespace"))

                ' add the resource to the signature
                MyBase.XmlSignature.AddResource(xmlDoc.ChildNodes, systemDriveSerialNumberTokenId)

                Return True

            Catch ex As isr.IO.BaseException
                ' report failure
                Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "{0} failed creating system drive serial number token: {1}", Me.InstanceName, ex.Message)
                Return False
            End Try

        End Function

        ''' <summary>verifies a system token data object based on the HDD serial 
        '''   number.</summary>
        ''' <remarks>Use this method to verify a token based on the system HDD serial number.</remarks>
        Private Function verifySystemDriveSerialNumberToken() As Boolean

            Try

                ' get the serial number of system drive
                Dim systemToken As String = Me.getSystemDriveSerialNumberToken()

                ' get the signature data
                Dim resourceData As String = MyBase.XmlSignature.GetResourceData(systemDriveSerialNumberTokenId)

                ' check if we have the HDD there
                Return (resourceData.IndexOf(systemToken) > 0)

            Catch ex As isr.IO.BaseException
                ' report failure
                Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "{0} failed verifying system drive serial number token: {1}", Me.InstanceName, ex.Message)
                Return False
            End Try

        End Function

        ''' <summary>Gets the serial number fo the system HDD.</summary>
        ''' <remarks>Use this method to get the serial number of the system drive.</remarks>
        Private Function getSystemDriveSerialNumberToken() As String

            Try

                Dim drive As String = Environment.SystemDirectory.Substring(0, 2)
                Dim disk As New System.Management.ManagementObject( _
                    String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "win32_logicaldisk.deviceid='{0}'", drive))
                disk.Get()
                Return disk("VolumeSerialNumber").ToString

            Catch ex As isr.IO.BaseException

                ' report failure
                Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "{0} failed getting system drive serial number token: {1}", Me.InstanceName, ex.Message)
                Return String.Empty

            End Try

        End Function

#End Region

    End Class

    ''' <summary>Enumerates the system token presently available.</summary>
    <Flags()> Public Enum SystemTokenTypes
        <ComponentModel.Description("None")> None
        <ComponentModel.Description("System Drive Serial Number")> SystemDriveSerialNumber = 1    ' System Drive Serial Number
        <ComponentModel.Description("File Distance")> FileDistance = 2    ' Distance between two hidden system files
        <ComponentModel.Description("System Drive Creation Date")> SystemDriveCreationDate = 8  ' c:\ creation date and time
        <ComponentModel.Description("Processor Id")> ProcessorId = 16  ' CPU ID
        <ComponentModel.Description("Bios Version")> BiosVersion = 32  ' Bios Version
    End Enum

End Namespace

