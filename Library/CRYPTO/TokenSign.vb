Namespace Cryptography

    ''' <summary>This is the abstract base class of the library of token signing.</summary>
    ''' <remarks>Inherit this class to implement component token signing.</remarks>
    ''' <license>
    ''' (c) 2010 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="06/21/03" by="David Hary" revision="1.0.1267.x">
    ''' created.
    ''' </history>
    Public MustInherit Class TokenSign

        ' implements a time piece interface
        Implements ITokenSign
        Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        Protected Sub New()

            Me.New(String.Empty)

        End Sub

        ''' <summary>Constructs this class.</summary>
        ''' <param name="instanceName">Specifies the name of the instance.</param>
        Protected Sub New(ByVal instanceName As String)

            MyBase.New()
            _instanceName = instanceName

        End Sub

        ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        ''' <remarks>Do not make this method overridable (virtual) because a derived 
        '''   class should not be able to override this method.</remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        Private _isDisposed As Boolean
        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
        ''' provided proper implementation.
        ''' </summary>
        Protected Property IsDisposed() As Boolean
            Get
                Return _isDisposed
            End Get
            Private Set(ByVal value As Boolean)
                _isDisposed = value
            End Set
        End Property

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                Try

                    If disposing Then

                        ' Free managed resources when explicitly called
                        _statusMessage = String.Empty
                        _instanceName = String.Empty

                        If Me._xmlSignature IsNot Nothing Then
                            Me._xmlSignature.Dispose()
                        End If

                    End If

                    ' Free shared unmanaged resources

                Finally

                    ' set the sentinel indicating that the class was disposed.
                    Me.IsDisposed = True

                End Try

            End If

        End Sub

        ''' <summary>This destructor will run only if the Dispose method 
        '''   does not get called. It gives the base class the opportunity to 
        '''   finalize. Do not provide destructors in types derived from this class.</summary>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal in terms of
            ' readability and maintainability.
            Dispose(False)
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Sub

#End Region

#Region " BASE METHODS AND PROPERTIES "

        ''' <summary>Overrides ToString returning the instance name if not empty.</summary>
        ''' <remarks>Use this method to return the instance name. If instance name is not set, 
        '''   returns the base class ToString value.</remarks>
        Public Overrides Function ToString() As String
            If String.IsNullOrEmpty(Me._instanceName) Then
                Return MyBase.ToString
            Else
                Return Me._instanceName
            End If
        End Function

        Private _isOpen As Boolean
        ''' <summary>Gets or sets the opened status of the instance.</summary>
        ''' <value><c>IsOpen</c> is a <see cref="Boolean"/> property that is True if the 
        '''   instance is open.</value>
        Public Property IsOpen() As Boolean
            Get
                Return _isOpen
            End Get
            Set(ByVal value As Boolean)
                _isOpen = value
            End Set
        End Property

        Private _instanceName As String = String.Empty
        ''' <summary>Gets or sets the name given to an instance of this class.</summary>
        ''' <value><c>InstanceName</c> is a String property.</value>
        ''' <remarks>If instance name is not set, returns .ToString.</remarks>
        ''' <history date="11/23/04" by="David Hary" revision="1.0.1788.x">
        '''   Correct code not to get instance name from .ToString but from MyBase.ToString
        '''   so that calling this method from the child class will not break the rule of
        '''   calling overridable methods from the constructor.
        ''' </history>
        Public Property InstanceName() As String
            Get
                If Not String.IsNullOrEmpty(_instanceName) Then
                    Return _instanceName
                Else
                    Return MyBase.ToString
                End If
            End Get
            Set(ByVal value As String)
                _instanceName = value
            End Set
        End Property

        Private _statusMessage As String = String.Empty
        ''' <summary>Gets or sets the status message.</summary>
        ''' <value>A <see cref="System.String">String</see>.</value>
        ''' <remarks>Use this property to get the status message generated by the object.</remarks>
        Public Property StatusMessage() As String
            Get
                Return _statusMessage
            End Get
            Set(ByVal value As String)
                _statusMessage = value
            End Set
        End Property

#End Region

#Region " TYPES "

        Protected Const TokenNamespace As String = "http://www.isr.cc/security"
        Protected Const FileNameTokenName As String = "FileNameToken"
        Protected Const FileNameTokenId As String = "isrFileNameToken"

#End Region

#Region " METHODS  and  PROPERTIES "

        ''' <summary>opens this instance.</summary>
        ''' <returns>A Boolean data type</returns>
        ''' <exception cref="OperationOpenException" guarantee="strong"></exception>
        ''' <remarks>Use this method to open the instance.  The method returns true if success or 
        '''   false if it failed opening the instance.</remarks>
        Public Overridable Function [Open]() As Boolean

            Try

                ' instantiate the XML signature object
                _xmlSignature = New isr.IO.Cryptography.XmlSign("SystemToken")
                Me.IsOpen = _xmlSignature.Open()
                Return Me.IsOpen

            Catch ex As isr.IO.BaseException

                ' close to meet strong guarantees
                Try
                    Me.Close()
                Finally
                End Try

                ' throw an exception
                Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "{0} failed opening: {1}", Me.InstanceName, ex.Message)
                Throw New isr.IO.OperationOpenException(Me.StatusMessage, ex)

            End Try

        End Function

        ''' <summary>Closes the instance.</summary>
        ''' <returns>A Boolean data type</returns>
        ''' <exception cref="OperationCloseException" guarantee="strong"></exception>
        ''' <remarks>Use this method to close the instance.  The method returns true if success or 
        '''   false if it failed closing the instance.</remarks>
        Public Overridable Function [Close]() As Boolean

            Try

                ' close the signature object
                Me.IsOpen = _xmlSignature.Close()
                Return Not Me.IsOpen

            Catch ex As isr.IO.BaseException
                ' throw an exception
                Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "{0} failed closing: {1}", Me.InstanceName, ex.Message)
                Throw New isr.IO.OperationCloseException(Me.StatusMessage, ex)
            End Try

        End Function

        ''' <summary>This must override method must be implemented in the child token signing.</summary>
        ''' <returns>A Boolean data type that is true if the token was 
        '''  Created.</returns>
        ''' <param name="signatureFileName">Spoecifies the signature file name.</param>
        ''' <remarks>Use this method to create the signed token.</remarks>
        ''' <history date="07/05/03" by="David Hary" revision="1.0.1281.x">
        '''   Add fileName
        ''' </history>
        Public MustOverride Function CreateToken(ByVal signatureFileName As String) As Boolean Implements ITokenSign.CreateToken

        ''' <summary>Verifies the signed token.</summary>
        ''' <returns>A Boolean data type that is true if the token was verified.</returns>
        ''' <param name="signatureFileName">Spoecifies the signature file name.</param>
        ''' <remarks>Use this method to verify the signed token.</remarks>
        ''' <history date="07/05/03" by="David Hary" revision="1.0.1281.x">
        '''   Add fileName
        ''' </history>
        Public MustOverride Function IsVerifyToken(ByVal signatureFileName As String) As Boolean Implements ITokenSign.IsVerifyToken

        ''' <summary>Reads the signed token.</summary>
        ''' <returns>A Boolean data type that is true if the token was read.</returns>
        ''' <param name="signatureFileName">Spoecifies the signature file from which to read 
        '''   the token.</param>
        ''' <remarks>Use this method to read the signed token.</remarks>
        Public Function ReadToken(ByVal signatureFileName As String) As Boolean Implements ITokenSign.ReadToken

            Try

                ' clear status message
                Me.StatusMessage = String.Empty

                If Me.IsOpen Then

                    ' read signature file
                    If _xmlSignature.ReadSignature(signatureFileName) Then

                        Return True

                    Else

                        ' report failure
                        Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
                        "{0} failed reading token:: {1}", _
                            Me.InstanceName, _xmlSignature.StatusMessage)
                        Return False

                    End If

                Else

                    ' report failure
                    Me.StatusMessage = _
                        String.Format(Globalization.CultureInfo.CurrentCulture, _
                      "{0} failed reading token because {0} was not open", _
                        Me.InstanceName)
                    Return False

                End If

            Catch ex As isr.IO.BaseException

                ' report failure
                Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "{0} failed reading token: {1}", Me.InstanceName, ex.Message)
                Return False
            End Try

        End Function

        ''' <summary>Writes the signed token.</summary>
        ''' <returns>A Boolean data type that is true if the token was written.</returns>
        ''' <param name="signatureFileName">Spoecifies the signature file to which to write 
        '''   the token.</param>
        ''' <remarks>Use this method to write the signed token.</remarks>
        Public Function WriteToken(ByVal signatureFileName As String) As Boolean Implements ITokenSign.WriteToken
            Try

                ' clear status message
                Me.StatusMessage = String.Empty

                If Me.IsOpen Then

                    ' read signature file
                    If _xmlSignature.WriteSignature(signatureFileName) Then

                        Return True

                    Else

                        ' report failure
                        Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
                        "{0} failed writing token:: {1}", _
                            Me.InstanceName, _xmlSignature.StatusMessage)
                        Return False

                    End If

                Else

                    ' report failure
                    Me.StatusMessage = _
                        String.Format(Globalization.CultureInfo.CurrentCulture, _
                      "{0} failed writing token because {0} was not open", _
                        Me.InstanceName)
                    Return False

                End If

            Catch ex As isr.IO.BaseException
                ' report failure
                Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "{0} failed writing token: {1}", Me.InstanceName, ex.Message)
                Return False
            End Try

        End Function

        Private _xmlSignature As isr.IO.Cryptography.XmlSign
        ''' <summary>Gets or sets the XML signature object.</summary>
        ''' <value><c>XmlSignature</c> is an isr.IO.Cryptography.XmlSign property that can be read from (read only).</value>
        ''' <remarks>Use this property to get reference to the XML signature for the token.</remarks>
        Public ReadOnly Property XmlSignature() As XmlSign
            Get
                Return _xmlSignature
            End Get
        End Property

#End Region

#Region " ON EVENT HANDLERS "

        ''' <summary>Adds a token data object based on the file name.</summary>
        ''' <param name="signatureFileName">Spoecifies the signature file name.</param>
        ''' <remarks>Use this method to add a file name token.</remarks>
        Protected Function AddFileNameToken(ByVal signatureFileName As String) As Boolean

            Try

                ' create a resource to hold the signed data
                Dim xmlDoc As System.Xml.XmlDocument = _
                    XmlSign.CreateResource(signatureFileName, "", FileNameTokenName, New System.Uri("MyBase.tokenNamespace"))

                ' add the resource to the signature
                Return _xmlSignature.AddResource(xmlDoc.ChildNodes, FileNameTokenId)

            Catch ex As isr.IO.BaseException

                ' report failure
                Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "{0} failed creating fileName token: {1}", Me.InstanceName, ex.Message)
                Return False

            End Try

        End Function

        ''' <summary>verifies the signed token.</summary>
        ''' <returns>A Boolean data type that is true if the token was 
        '''   verified.</returns>
        ''' <param name="signatureFileName">Spoecifies the signature file name.</param>
        ''' <remarks>Use this method to verify the signed token.</remarks>
        Protected Function IsVerifyFileNameToken(ByVal signatureFileName As String) As Boolean

            If signatureFileName Is Nothing Then
                Return False
            End If
            Try

                ' get the signature data
                Dim resourceData As String = _xmlSignature.GetResourceData(FileNameTokenId)

                ' check if we have the HDD there
                If resourceData Is Nothing Then
                    Return False
                Else
                    Return (resourceData.ToUpper(Globalization.CultureInfo.CurrentCulture).IndexOf(signatureFileName.ToUpper(Globalization.CultureInfo.CurrentCulture)) > 0)
                End If

            Catch ex As isr.IO.BaseException
                ' report failure
                Me.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "{0} failed verifying fileName token: {1}", Me.InstanceName, ex.Message)
                Return False
            End Try

        End Function

#End Region

    End Class

End Namespace
