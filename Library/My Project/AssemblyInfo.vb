Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Security.Permissions

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("ISR IO VB Library")> 
<Assembly: AssemblyDescription("ISR IO VB Library")> 
<Assembly: AssemblyCompany("Integrated Scientific Resources, Inc.")> 
<Assembly: AssemblyProduct("ISR IO VB Library 1.2")> 
<Assembly: AssemblyCopyright("(c) 2005 Integrated Scientific Resources, Inc. All rights reserved.")>  
<Assembly: AssemblyTrademark("isr.IO and the ISR logo are trademarks of Integrated Scientific Resources, Inc.")> 
<Assembly: CLSCompliant(True)> 
<Assembly: Resources.NeutralResourcesLanguage("en-US", Resources.UltimateResourceFallbackLocation.MainAssembly)> 

' The following key pair file provides a strong name signature of this library
'<Assembly: AssemblyKeyFileAttribute("\\muscat\\c\projects\keyPairs\isr\keyPair.snk")> 
 
' Disable accessibility of an individual managed type or member, or of all types within an assembly, to COM.
<Assembly: ComVisible(False)> 

' The following GUID is for the ID of the type library if this project is exposed to COM
' <Assembly: Guid("8EF06776-34FF-4832-9A94-56C431E0BF6D")> '("AE35654F-9C43-43B2-8A3F-9C19D09CFB46")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
<Assembly: AssemblyVersion("1.2.*")> 
