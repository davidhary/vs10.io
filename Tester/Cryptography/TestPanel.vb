Namespace Cryptography

  ''' <summary>Includes code for form TestPanel.</summary>
  ''' <remarks>Launch this form by calling its Show or ShowDialog method from its default
  '''   instance.</remarks>
''' <license>
''' (c) 2003 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
  ''' <history date="06/17/03" by="David Hary" revision="1.0.1263.x">
  ''' Created
  ''' </history>
  Friend Class TestPanel
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
      MyBase.New()

      ' Initialize user components that might be affected by resize or paint actions
      onInitialize()

      ' This method is required by the Windows Form Designer.
      InitializeComponent()

      ' Add any initialization after the InitializeComponent() call
      onInstantiate()

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    Try

      If disposing Then
      
        ' Free managed resources when explicitly called
        onDisposeManagedResources()

        If components IsNot Nothing Then
          components.Dispose()
        End If

      
      End If

          ' Free shared unmanaged resources
         onDisposeUnmanagedResources()

      Finally

        ' Invoke the base class dispose method
      MyBase.Dispose(disposing)

    End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents dataFileCaptionLabel As System.Windows.Forms.Label
    Friend WithEvents signatureFileCaptionLabel As System.Windows.Forms.Label
    Friend WithEvents signAndSaveButton As System.Windows.Forms.Button
    Friend WithEvents readAndVerifyButton As System.Windows.Forms.Button
    Friend WithEvents xmlSignDataTextBox As System.Windows.Forms.TextBox
    Friend WithEvents signatureCaptionLabel As System.Windows.Forms.Label
    Friend WithEvents signatureTextBox As System.Windows.Forms.TextBox
    Friend WithEvents xmlfilePathNameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents signatureTextBoxFilePathName As System.Windows.Forms.TextBox
    Friend WithEvents signatureDateCaptionLabel As System.Windows.Forms.Label
    Friend WithEvents outcomeLabel As System.Windows.Forms.Label
    Friend WithEvents mainTabControl As System.Windows.Forms.TabControl
    Friend WithEvents xmlSignTabPage As System.Windows.Forms.TabPage
    Friend WithEvents tokenSignTabPage As System.Windows.Forms.TabPage
    Friend WithEvents verifyButton As System.Windows.Forms.Button
    Friend WithEvents createButton As System.Windows.Forms.Button
    Friend WithEvents showMeButton As System.Windows.Forms.Button
    Friend WithEvents signatureDataTextBox As System.Windows.Forms.TextBox
    Friend WithEvents signatureDataLabel As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
      Me.xmlfilePathNameTextBox = New System.Windows.Forms.TextBox
      Me.dataFileCaptionLabel = New System.Windows.Forms.Label
      Me.signatureFileCaptionLabel = New System.Windows.Forms.Label
      Me.signatureTextBoxFilePathName = New System.Windows.Forms.TextBox
      Me.signAndSaveButton = New System.Windows.Forms.Button
      Me.readAndVerifyButton = New System.Windows.Forms.Button
      Me.xmlSignDataTextBox = New System.Windows.Forms.TextBox
      Me.signatureDateCaptionLabel = New System.Windows.Forms.Label
      Me.signatureCaptionLabel = New System.Windows.Forms.Label
      Me.signatureTextBox = New System.Windows.Forms.TextBox
      Me.outcomeLabel = New System.Windows.Forms.Label
      Me.mainTabControl = New System.Windows.Forms.TabControl
      Me.xmlSignTabPage = New System.Windows.Forms.TabPage
      Me.tokenSignTabPage = New System.Windows.Forms.TabPage
      Me.verifyButton = New System.Windows.Forms.Button
      Me.createButton = New System.Windows.Forms.Button
      Me.showMeButton = New System.Windows.Forms.Button
      Me.signatureDataTextBox = New System.Windows.Forms.TextBox
      Me.signatureDataLabel = New System.Windows.Forms.Label
      Me.mainTabControl.SuspendLayout()
      Me.xmlSignTabPage.SuspendLayout()
      Me.tokenSignTabPage.SuspendLayout()
      Me.SuspendLayout()
      '
      'xmlfilePathNameTextBox
      '
      Me.xmlfilePathNameTextBox.Location = New System.Drawing.Point(56, 32)
      Me.xmlfilePathNameTextBox.Name = "xmlfilePathNameTextBox"
      Me.xmlfilePathNameTextBox.Size = New System.Drawing.Size(248, 20)
      Me.xmlfilePathNameTextBox.TabIndex = 0
      Me.xmlfilePathNameTextBox.Text = "CreditCardInfo.xml"
      '
      'dataFileCaptionLabel
      '
      Me.dataFileCaptionLabel.AutoSize = True
      Me.dataFileCaptionLabel.Location = New System.Drawing.Point(56, 16)
      Me.dataFileCaptionLabel.Name = "dataFileCaptionLabel"
      Me.dataFileCaptionLabel.Size = New System.Drawing.Size(277, 16)
      Me.dataFileCaptionLabel.TabIndex = 1
      Me.dataFileCaptionLabel.Text = "File including data to sign (also used as resource id):   "
      Me.dataFileCaptionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
      '
      'signatureFileCaptionLabel
      '
      Me.signatureFileCaptionLabel.Location = New System.Drawing.Point(56, 64)
      Me.signatureFileCaptionLabel.Name = "signatureFileCaptionLabel"
      Me.signatureFileCaptionLabel.Size = New System.Drawing.Size(138, 16)
      Me.signatureFileCaptionLabel.TabIndex = 3
      Me.signatureFileCaptionLabel.Text = "Signature File: "
      Me.signatureFileCaptionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
      '
      'signatureTextBoxFilePathName
      '
      Me.signatureTextBoxFilePathName.Location = New System.Drawing.Point(56, 80)
      Me.signatureTextBoxFilePathName.Name = "signatureTextBoxFilePathName"
      Me.signatureTextBoxFilePathName.Size = New System.Drawing.Size(248, 20)
      Me.signatureTextBoxFilePathName.TabIndex = 2
      Me.signatureTextBoxFilePathName.Text = "signature.xml"
      '
      'signAndSaveButton
      '
      Me.signAndSaveButton.Location = New System.Drawing.Point(312, 24)
      Me.signAndSaveButton.Name = "signAndSaveButton"
      Me.signAndSaveButton.Size = New System.Drawing.Size(136, 32)
      Me.signAndSaveButton.TabIndex = 4
      Me.signAndSaveButton.Text = "Sign and Save"
      '
      'readAndVerifyButton
      '
      Me.readAndVerifyButton.Location = New System.Drawing.Point(312, 72)
      Me.readAndVerifyButton.Name = "readAndVerifyButton"
      Me.readAndVerifyButton.Size = New System.Drawing.Size(136, 32)
      Me.readAndVerifyButton.TabIndex = 5
      Me.readAndVerifyButton.Text = "Read and Verify"
      '
      'xmlSignDataTextBox
      '
      Me.xmlSignDataTextBox.Location = New System.Drawing.Point(56, 136)
      Me.xmlSignDataTextBox.Multiline = True
      Me.xmlSignDataTextBox.Name = "xmlSignDataTextBox"
      Me.xmlSignDataTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
      Me.xmlSignDataTextBox.Size = New System.Drawing.Size(392, 100)
      Me.xmlSignDataTextBox.TabIndex = 6
      Me.xmlSignDataTextBox.Text = "This data will be signed"
      '
      'signatureDateCaptionLabel
      '
      Me.signatureDateCaptionLabel.Location = New System.Drawing.Point(56, 112)
      Me.signatureDateCaptionLabel.Name = "signatureDateCaptionLabel"
      Me.signatureDateCaptionLabel.Size = New System.Drawing.Size(138, 16)
      Me.signatureDateCaptionLabel.TabIndex = 7
      Me.signatureDateCaptionLabel.Text = "XML Signature Data: "
      Me.signatureDateCaptionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
      '
      'signatureCaptionLabel
      '
      Me.signatureCaptionLabel.Location = New System.Drawing.Point(56, 240)
      Me.signatureCaptionLabel.Name = "signatureCaptionLabel"
      Me.signatureCaptionLabel.Size = New System.Drawing.Size(138, 16)
      Me.signatureCaptionLabel.TabIndex = 11
      Me.signatureCaptionLabel.Text = "Signature: "
      Me.signatureCaptionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
      '
      'signatureTextBox
      '
      Me.signatureTextBox.Location = New System.Drawing.Point(56, 256)
      Me.signatureTextBox.Multiline = True
      Me.signatureTextBox.Name = "signatureTextBox"
      Me.signatureTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
      Me.signatureTextBox.Size = New System.Drawing.Size(392, 136)
      Me.signatureTextBox.TabIndex = 10
      Me.signatureTextBox.Text = "where the signature goes"
      '
      'outcomeLabel
      '
      Me.outcomeLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
      Me.outcomeLabel.Location = New System.Drawing.Point(64, 400)
      Me.outcomeLabel.Name = "outcomeLabel"
      Me.outcomeLabel.Size = New System.Drawing.Size(304, 16)
      Me.outcomeLabel.TabIndex = 13
      Me.outcomeLabel.Text = "Outcome:  "
      '
      'mainTabControl
      '
      Me.mainTabControl.Controls.Add(Me.xmlSignTabPage)
      Me.mainTabControl.Controls.Add(Me.tokenSignTabPage)
      Me.mainTabControl.Dock = System.Windows.Forms.DockStyle.Fill
      Me.mainTabControl.Location = New System.Drawing.Point(0, 0)
      Me.mainTabControl.Name = "mainTabControl"
      Me.mainTabControl.SelectedIndex = 0
      Me.mainTabControl.Size = New System.Drawing.Size(568, 470)
      Me.mainTabControl.TabIndex = 14
      '
      'xmlSignTabPage
      '
      Me.xmlSignTabPage.Controls.Add(Me.xmlfilePathNameTextBox)
      Me.xmlSignTabPage.Controls.Add(Me.outcomeLabel)
      Me.xmlSignTabPage.Controls.Add(Me.dataFileCaptionLabel)
      Me.xmlSignTabPage.Controls.Add(Me.signatureFileCaptionLabel)
      Me.xmlSignTabPage.Controls.Add(Me.signatureTextBoxFilePathName)
      Me.xmlSignTabPage.Controls.Add(Me.signAndSaveButton)
      Me.xmlSignTabPage.Controls.Add(Me.readAndVerifyButton)
      Me.xmlSignTabPage.Controls.Add(Me.xmlSignDataTextBox)
      Me.xmlSignTabPage.Controls.Add(Me.signatureDateCaptionLabel)
      Me.xmlSignTabPage.Controls.Add(Me.signatureCaptionLabel)
      Me.xmlSignTabPage.Controls.Add(Me.signatureTextBox)
      Me.xmlSignTabPage.Location = New System.Drawing.Point(4, 22)
      Me.xmlSignTabPage.Name = "xmlSignTabPage"
      Me.xmlSignTabPage.Size = New System.Drawing.Size(560, 444)
      Me.xmlSignTabPage.TabIndex = 0
      Me.xmlSignTabPage.Text = "XML Signature"
      '
      'tokenSignTabPage
      '
      Me.tokenSignTabPage.Controls.Add(Me.verifyButton)
      Me.tokenSignTabPage.Controls.Add(Me.createButton)
      Me.tokenSignTabPage.Controls.Add(Me.showMeButton)
      Me.tokenSignTabPage.Controls.Add(Me.signatureDataTextBox)
      Me.tokenSignTabPage.Controls.Add(Me.signatureDataLabel)
      Me.tokenSignTabPage.Location = New System.Drawing.Point(4, 22)
      Me.tokenSignTabPage.Name = "tokenSignTabPage"
      Me.tokenSignTabPage.Size = New System.Drawing.Size(560, 444)
      Me.tokenSignTabPage.TabIndex = 1
      Me.tokenSignTabPage.Text = "Token Signatures"
      '
      'verifyButton
      '
      Me.verifyButton.Location = New System.Drawing.Point(240, 340)
      Me.verifyButton.Name = "verifyButton"
      Me.verifyButton.Size = New System.Drawing.Size(136, 23)
      Me.verifyButton.TabIndex = 10
      Me.verifyButton.Text = "Verify System Token"
      '
      'createButton
      '
      Me.createButton.Location = New System.Drawing.Point(96, 340)
      Me.createButton.Name = "createButton"
      Me.createButton.Size = New System.Drawing.Size(136, 23)
      Me.createButton.TabIndex = 9
      Me.createButton.Text = "Create System Token"
      '
      'showMeButton
      '
      Me.showMeButton.Location = New System.Drawing.Point(8, 340)
      Me.showMeButton.Name = "showMeButton"
      Me.showMeButton.TabIndex = 8
      Me.showMeButton.Text = "Show Me"
      '
      'signatureDataTextBox
      '
      Me.signatureDataTextBox.Location = New System.Drawing.Point(8, 87)
      Me.signatureDataTextBox.Multiline = True
      Me.signatureDataTextBox.Name = "signatureDataTextBox"
      Me.signatureDataTextBox.Size = New System.Drawing.Size(520, 246)
      Me.signatureDataTextBox.TabIndex = 7
      Me.signatureDataTextBox.Text = ""
      '
      'signatureDataLabel
      '
      Me.signatureDataLabel.AutoSize = True
      Me.signatureDataLabel.Location = New System.Drawing.Point(8, 68)
      Me.signatureDataLabel.Name = "signatureDataLabel"
      Me.signatureDataLabel.Size = New System.Drawing.Size(86, 16)
      Me.signatureDataLabel.TabIndex = 6
      Me.signatureDataLabel.Text = "Signature Data: "
      '
      'TestPanel
      '
      Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
      Me.ClientSize = New System.Drawing.Size(568, 470)
      Me.Controls.Add(Me.mainTabControl)
      Me.Name = "TestPanel"
      Me.Text = "TestPanel"
      Me.mainTabControl.ResumeLayout(False)
      Me.xmlSignTabPage.ResumeLayout(False)
      Me.tokenSignTabPage.ResumeLayout(False)
      Me.ResumeLayout(False)

    End Sub

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Cleans up managed components.</summary>
    ''' <remarks>Use this method to reclaim managed resources used by this class.</remarks>
      Private Sub onDisposeManagedResources()

    End Sub

    ''' <summary>Cleans up unmanaged components.</summary>
    ''' <remarks>Use this method to reclaim unmanaged resources used by this class.</remarks>
      Private Sub onDisposeUnmanagedResources()

    End Sub

    ''' <summary>Initializes components that might be affected by resize and paint events.</summary>
    ''' <remarks>Called from the class constructor before initializing the form components 
    '''		for components that are affected by resize and paint events.</remarks>
      Private Sub onInitialize()
    End Sub

    ''' <summary>Initializes additional components.</summary>
    ''' <remarks>Called from the class constructor to add other controls or 
    '''   change anything set by the InitializeComponent method.</remarks>
      Private Sub onInstantiate()
    End Sub

#End Region

#Region " METHODS "

#End Region

#Region " PROPERTIES "

    Private _statusMessage As String = String.Empty
    ''' <summary>Gets or sets the status message.</summary>
    ''' <value>A <see cref="System.String">String</see>.</value>
    ''' <remarks>Use this property to get the status message generated by the object.</remarks>
      Public ReadOnly Property StatusMessage() As String
      Get
        Return _statusMessage
      End Get
    End Property

    ''' <summary>Returns true if an instance of the class was created and not disposed.</summary>
    Friend Shared ReadOnly Property Instantiated() As Boolean
      ' Returns true if an instance of the class was created and not disposed
      Get
        Return My.Application.OpenForms.Count > 0 AndAlso _
            My.Application.OpenForms.Item(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name) IsNot Nothing
      End Get
    End Property

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary>Occurs after the form is closed.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
  ''' <remarks>This event is a notification that the form has already gone away before
  ''' control is returned to the calling method (in case of a modal form).  Use this
  ''' method to delete any temporary files that were created or dispose of any objects
  ''' not disposed with the closing event.
  ''' </remarks>
      Private Sub form_Closed(ByVal sender As System.Object, ByVal e As System.EventArgs) _
      Handles MyBase.Closed

    End Sub

    ''' <summary>Occurs before the form is closed</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.ComponentModel.CancelEventArgs"/></param>
    ''' <remarks>Use this method to allow canceling the closing of the form.
    '''   Because the form is not yet closed at this point, this is also the best 
    '''   place to serialize a form's visible properties, such as size and 
    '''   location.</remarks>
      Private Sub form_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) _
      Handles MyBase.Closing

      ' disable the timer if any
      ' actionTimer.Enabled = False
      System.Windows.Forms.Application.DoEvents()

      ' set module objects that reference other objects to Nothing

      ' terminate form objects
      Me.terminateObjects()

    End Sub

    ''' <summary>Is true after form has completed loading so that we can disable check boxes 
    '''   that should not fire until the form is fully loaded.</summary>
    ''' <remarks>Initialization was removed per performance rule CA1805 as it is done 
    '''   automatically by the runtime.</remarks>
      Private loaded As Boolean

    ''' <summary>Occurs when the form is loaded.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>Use this method for doing any final initialization right before 
    '''   the form is shown.  This is a good place to change the Visible and
    '''   ShowInTaskbar properties to start the form as hidden.  
    '''   Starting a form as hidden is useful for forms that need to be running but that
    '''   should not show themselves right away, such as forms with a notify icon in the
    '''   task bar.</remarks>
      Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) _
      Handles MyBase.Load

      Try

        ' Turn on the form hourglass cursor
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        ' instantiate form objects
        Me.instantiateObjects()

        ' set the form caption
        Me.Text = isr.Core.AssemblyInfoExtensions.ExtendedCaption(My.Application.Info) & ": CRYPTO TEST PANEL"

        ' set tool tips
        initializeUserInterface()

        ' center the form
        Me.CenterToScreen()

        ' turn on the loaded flag
        loaded = True

      Catch

        ' Use throw without an argument in order to preserve the stack location 
        ' where the exception was initially raised.
        Throw

      Finally

        ' Turn off the form hourglass cursor
        Me.Cursor = System.Windows.Forms.Cursors.Default

      End Try

    End Sub

    Private Sub form_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated
      '   Me.notesMessageList.PushMessage("Activated")
    End Sub

#End Region

#Region " PRIVATE  and  PROTECTED "

    Private _xmlSign As isr.IO.Cryptography.XmlSign
    ''' <summary>Initializes the class objects.</summary>
    ''' <remarks>Called from the form load method to instantiate 
    '''   module-level objects.</remarks>
    Private Sub instantiateObjects()

      ' instantiate and open the XML sign class
      _xmlSign = New isr.IO.Cryptography.XmlSign("XML Sign TestPanel")

    End Sub

    ''' <summary>Initializes the user interface and tool tips.</summary>
    ''' <remarks>Call this method from the form load method to set the user interface.</remarks>
      Private Sub initializeUserInterface()
      '    tipsToolTip.SetToolTip(Me.txtDuration, "Enter count-down duration in seconds")
      '    tipsToolTip.SetToolTip(Me.exitButton, "Click to exit")
    End Sub

  ''' <summary>Terminates and disposes of class-level objects.</summary>
  ''' <remarks>Called from the form Closing method.</remarks>
      Private Sub terminateObjects()

      ' terminate the xml sign object
      _xmlSign = Nothing

    End Sub

    Private Sub showResults()

      Try

        If False Then
          Dim itemName As Object
          Dim itemList As ArrayList
          itemList = isr.IO.Cryptography.SystemSign.EnumerateSystems("Win32_NetworkAdapter", "deviceid")
          Me.signatureDataTextBox.Clear()
          For Each itemName In itemList
            Me.signatureDataTextBox.AppendText(itemName.ToString())
            Me.signatureDataTextBox.AppendText(Environment.NewLine)
          Next
          Me.signatureDataTextBox.AppendText("MAC Address: ")
          Me.signatureDataTextBox.AppendText(isr.IO.Cryptography.SystemSign.GetSystemProperty("Win32_NetworkAdapter.deviceid='3'", "MACAddress"))
          Exit Sub
        End If

        ' Me.statusLabel.Text = System.IO.Directory.GetCreationTime("c:\").ToString
        Me.signatureDataTextBox.Clear()
        Me.signatureDataTextBox.AppendText("C Drive S/N: ")
        Me.signatureDataTextBox.AppendText(isr.IO.Cryptography.SystemSign.GetSystemProperty("Win32_LogicalDisk.deviceid='c:'", "VolumeSerialNumber"))
        Me.signatureDataTextBox.AppendText(Environment.NewLine)
        Me.signatureDataTextBox.AppendText("Processor ID: ")
        Me.signatureDataTextBox.AppendText(isr.IO.Cryptography.SystemSign.GetSystemProperty("Win32_Processor.deviceid='CPU0'", "Processorid"))
        Me.signatureDataTextBox.AppendText(Environment.NewLine)
        Me.signatureDataTextBox.AppendText("MAC Address: ")
        Me.signatureDataTextBox.AppendText(isr.IO.Cryptography.SystemSign.GetSystemPropertyFirstEnum("Win32_NetworkAdapter", "MACAddress"))
        Me.signatureDataTextBox.AppendText(Environment.NewLine)
        Me.signatureDataTextBox.AppendText("Bios Version: ")
        Me.signatureDataTextBox.AppendText(isr.IO.Cryptography.SystemSign.GetSystemPropertyFirstEnum("Win32_BIOS", "Version"))
        Me.signatureDataTextBox.AppendText(Environment.NewLine)
        Me.signatureDataTextBox.AppendText("C:\ creating time: ")
        Me.signatureDataTextBox.AppendText(System.IO.Directory.GetCreationTime("c:\").ToString(Globalization.CultureInfo.CurrentCulture))

        'Me.statusLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture,           "{0} {1}", Date.Now.Ticks, Gopher.UsingDevices.ToString)
        'Me.Close()

      Catch ex As isr.IO.BaseException

        ' report failure 
        _statusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
            "Failed closing {0}{1}", _
            Environment.NewLine, ex.Message)
        MessageBox.Show(_statusMessage, Me.Name, _
            MessageBoxButtons.OK, MessageBoxIcon.Error)

      End Try

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary>Closes the form and exits the application.</summary>
    Private Sub exitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

      Try

        ' Me.statusLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "{0} {1}", Date.Now.Ticks, Gopher.UsingDevices.ToString)
        Me.Close()

      Catch ex As System.Exception

        ' report failure to print
        Dim userMessage As String = String.Format(Globalization.CultureInfo.CurrentCulture, _
            "Failed closing {0}{1}", _
            Environment.NewLine, ex.Message)
        MessageBox.Show(userMessage, Me.Name, _
            MessageBoxButtons.OK, MessageBoxIcon.Error)

      End Try

    End Sub

    ''' <summary>Occurs when the operator selects the Sign and Save button.</summary>
    ''' <remarks>Use this method to read XML data to sign, sign and save the signature and display 
    '''   the signature and data.</remarks>
    Private Sub signAndSaveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
      Handles signAndSaveButton.Click

      Dim dataId As String

      Try

        ' Clear the outcome label
        Me.outcomeLabel.Text = String.Empty
        Me.signatureTextBox.Clear()
        Me.xmlSignDataTextBox.Clear()

        ' open the XML signature object
        _xmlSign.Open()

        ' add a data object
        dataId = Me.xmlfilePathNameTextBox.Text.ToLower(Globalization.CultureInfo.CurrentCulture)
        If _xmlSign.AddResource(Me.xmlfilePathNameTextBox.Text, dataId) Then

          ' Generate the Signature
          If _xmlSign.GenerateSignature() Then

            ' Verify the signature
            If _xmlSign.IsVerifySignature() Then

              If _xmlSign.WriteSignature(Me.signatureTextBoxFilePathName.Text) Then
                Me.outcomeLabel.Text = "Signature created and verified"
                Me.signatureTextBox.Text = _xmlSign.Signature
                Me.xmlSignDataTextBox.Text = _xmlSign.GetResourceData(dataId)
              Else
                Me.outcomeLabel.Text = "Filed to save signature"
                Me.signatureTextBox.Text = _xmlSign.StatusMessage
              End If
            Else
              Me.outcomeLabel.Text = "Signature failed"
              Me.signatureTextBox.Text = _xmlSign.StatusMessage
            End If

          Else
            Me.outcomeLabel.Text = "Failed generating signature"
            Me.signatureTextBox.Text = _xmlSign.StatusMessage
          End If

        Else
          Me.outcomeLabel.Text = "Failed adding data"
          Me.signatureTextBox.Text = _xmlSign.StatusMessage
        End If

        ' close the XML signature object
        _xmlSign.Close()

      Catch ex As isr.IO.BaseException

        Try
          ' close the XML signature object
          _xmlSign.Close()
        Catch exn As System.ApplicationException
        End Try

        ' report failure to print
        Dim userMessage As String = String.Format(Globalization.CultureInfo.CurrentCulture, _
            "Failed to sign and save {0}{1}", _
            Environment.NewLine, ex.Message)
        MessageBox.Show(userMessage, Me.Name, _
            MessageBoxButtons.OK, MessageBoxIcon.Error)

      End Try
    End Sub

    ''' <summary>Occurs when the operator selects the Read and Verify button.</summary>
    ''' <remarks>Use this method to read a signature file, verify and display the signature 
    '''   and data.</remarks>
      Private Sub readAndVerifyButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
      Handles readAndVerifyButton.Click

      Dim dataId As String

      Try

        ' Clear the outcome label
        Me.outcomeLabel.Text = String.Empty
        Me.signatureTextBox.Clear()
        Me.xmlSignDataTextBox.Clear()

        ' open the XML signature object
        _xmlSign.Open()

        ' use the resource id from the file name
        dataId = Me.xmlfilePathNameTextBox.Text.ToLower(Globalization.CultureInfo.CurrentCulture)

        ' read the signature file
        If _xmlSign.ReadSignature(Me.signatureTextBoxFilePathName.Text) Then

          ' Verify the signature
          If _xmlSign.IsVerifySignature() Then
            Me.outcomeLabel.Text = "Signature read and verified"
            Me.signatureTextBox.Text = _xmlSign.Signature
            Me.xmlSignDataTextBox.Text = _xmlSign.GetResourceData(dataId)
          Else
            Me.outcomeLabel.Text = "Signature not verified"
            Me.signatureTextBox.Text = _xmlSign.StatusMessage
          End If

        Else
          Me.outcomeLabel.Text = "Failed reading signature"
          Me.signatureTextBox.Text = _xmlSign.StatusMessage
        End If

        ' close the XML signature object
        _xmlSign.Close()

      Catch ex As System.Exception

        Try
          ' close the XML signature object
          _xmlSign.Close()
        Catch exn As System.ApplicationException
        End Try

        ' report failure to print
        Dim userMessage As String = String.Format(Globalization.CultureInfo.CurrentCulture, _
            "Failed reading and verifying {0}{1}", _
            Environment.NewLine, ex.Message)
        MessageBox.Show(userMessage, Me.Name, _
            MessageBoxButtons.OK, MessageBoxIcon.Error)

      End Try

    End Sub

    Private Sub showMeButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
      Handles showMeButton.Click

      Try

        showResults()
        ' Me.statusLabel.Text = System.IO.Directory.GetCreationTime("c:\").ToString
        'Me.statusLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "{0} {1}", Date.Now.Ticks, Gopher.UsingDevices.ToString)
        'Me.Close()

      Catch ex As isr.IO.BaseException

        ' report failure 
        _statusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
            "Failed to show results {0}{1}", _
            Environment.NewLine, ex.Message)
        MessageBox.Show(_statusMessage, Me.Name, _
            MessageBoxButtons.OK, MessageBoxIcon.Error)

      End Try

    End Sub

    Private tokenFilePathName As String = "systemToken.xml"
    Private Sub createButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
      Handles createButton.Click

      Try
        tokenFilePathName = My.Application.Info.AssemblyName & ".exe.permit"
        Dim sysToken As New isr.IO.Cryptography.SystemSign("isrCreateSystemToken")
        If sysToken.Open Then
          If sysToken.CreateToken(tokenFilePathName) Then
            If sysToken.WriteToken(tokenFilePathName) Then
              Me.signatureDataTextBox.Text = "Token created and written to disk: "
              Me.signatureDataTextBox.AppendText(tokenFilePathName)
              Me.signatureDataTextBox.AppendText(Environment.NewLine)
              Me.signatureDataTextBox.AppendText(sysToken.XmlSignature.GetResourceData("isrSystemTokenOne"))
            Else
              Me.signatureDataTextBox.Text = "Failed writing token to: "
              Me.signatureDataTextBox.AppendText(tokenFilePathName)
              Me.signatureDataTextBox.AppendText(Environment.NewLine)
              Me.signatureDataTextBox.AppendText(sysToken.StatusMessage)
            End If
          Else
            Me.signatureDataTextBox.Text = "Failed creating token: "
            Me.signatureDataTextBox.AppendText(Environment.NewLine)
            Me.signatureDataTextBox.AppendText(sysToken.StatusMessage)
          End If
        Else
          Me.signatureDataTextBox.Text = "Failed opening token object: "
          Me.signatureDataTextBox.AppendText(Environment.NewLine)
          Me.signatureDataTextBox.AppendText(sysToken.StatusMessage)
        End If

      Catch ex As isr.IO.BaseException

        ' report failure 
        _statusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
            "Failed creating system token:: {0}{1}", Environment.NewLine, ex.Message)
        MessageBox.Show(_statusMessage, Me.Name, _
            MessageBoxButtons.OK, MessageBoxIcon.Error)

      End Try

    End Sub

    Private Sub verifyButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
      Handles verifyButton.Click

      Try
        tokenFilePathName = My.Application.Info.AssemblyName & ".exe.permit"
        Dim sysToken As New isr.IO.Cryptography.SystemSign("isrVerifySystemToken")
        If sysToken.Open Then
          If sysToken.ReadToken(tokenFilePathName) Then
            If sysToken.IsVerifyToken(tokenFilePathName) Then
              Me.signatureDataTextBox.Text = "Token read and verified from "
              Me.signatureDataTextBox.AppendText(tokenFilePathName)
              Me.signatureDataTextBox.AppendText(Environment.NewLine)
              Me.signatureDataTextBox.AppendText(sysToken.XmlSignature.GetResourceData("isrSystemTokenOne"))
            Else
              Me.signatureDataTextBox.Text = "Failed verifying token: "
              Me.signatureDataTextBox.AppendText(Environment.NewLine)
              Me.signatureDataTextBox.AppendText(sysToken.StatusMessage)
            End If
          Else
            Me.signatureDataTextBox.Text = "Failed reading token for: "
            Me.signatureDataTextBox.AppendText(tokenFilePathName)
            Me.signatureDataTextBox.AppendText(Environment.NewLine)
            Me.signatureDataTextBox.AppendText(sysToken.StatusMessage)
          End If
        Else
          Me.signatureDataTextBox.Text = "Failed opening token object: "
          Me.signatureDataTextBox.AppendText(Environment.NewLine)
          Me.signatureDataTextBox.AppendText(sysToken.StatusMessage)
        End If

      Catch ex As isr.IO.BaseException

        ' report failure 
        _statusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
            "Failed verifying system token:: {0}{1}", Environment.NewLine, ex.Message)
        MessageBox.Show(_statusMessage, Me.Name, _
            MessageBoxButtons.OK, MessageBoxIcon.Error)

      End Try

    End Sub

#End Region

  End Class

End Namespace