''' <summary>Includes code for form SwitchBoardForm, which serves as a switchboard for this program.</summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="04/19/05" by="David Hary" revision="1.0.1935.x">
''' Created
''' </history>
Friend Class SwitchBoardForm
  Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    ' Initialize user components that might be affected by resize or paint actions
    onInitialize()

    ' This method is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call
    onInstantiate()

  End Sub

  ' Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    Try

    If disposing Then

      ' Free managed resources when explicitly called
      onDisposeManagedResources()

      If components IsNot Nothing Then
        components.Dispose()
      End If

        End If
    ' Free shared unmanaged resources

    Finally

      ' Invoke the base class dispose method
      MyBase.Dispose(disposing)

    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer
  Public tipsToolTip As System.Windows.Forms.ToolTip
  Public WithEvents actionsComboBox As System.Windows.Forms.ComboBox
  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.
  'Do not modify it using the code editor.
  Friend WithEvents exitButton As System.Windows.Forms.Button
  Private WithEvents semtechPictureBox As System.Windows.Forms.PictureBox
  Friend WithEvents openButton As System.Windows.Forms.Button
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(SwitchBoardForm))
    Me.tipsToolTip = New System.Windows.Forms.ToolTip(Me.components)
    Me.actionsComboBox = New System.Windows.Forms.ComboBox
    Me.semtechPictureBox = New System.Windows.Forms.PictureBox
    Me.exitButton = New System.Windows.Forms.Button
    Me.openButton = New System.Windows.Forms.Button
    Me.SuspendLayout()
    '
    'actionsComboBox
    '
    Me.actionsComboBox.BackColor = System.Drawing.SystemColors.Window
    Me.actionsComboBox.Cursor = System.Windows.Forms.Cursors.Default
    Me.actionsComboBox.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.actionsComboBox.ForeColor = System.Drawing.SystemColors.WindowText
    Me.actionsComboBox.Location = New System.Drawing.Point(8, 16)
    Me.actionsComboBox.Name = "actionsComboBox"
    Me.actionsComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
    Me.actionsComboBox.Size = New System.Drawing.Size(361, 27)
    Me.actionsComboBox.TabIndex = 2
    Me.actionsComboBox.Text = "Select option from the list"
    '
    'semtechPictureBox
    '
    Me.semtechPictureBox.Cursor = System.Windows.Forms.Cursors.Hand
    Me.semtechPictureBox.Location = New System.Drawing.Point(11, 149)
    Me.semtechPictureBox.Name = "semtechPictureBox"
    Me.semtechPictureBox.Size = New System.Drawing.Size(62, 100)
    Me.semtechPictureBox.TabIndex = 6
    Me.semtechPictureBox.TabStop = False
    '
    'exitButton
    '
    Me.exitButton.Location = New System.Drawing.Point(360, 56)
    Me.exitButton.Name = "exitButton"
    Me.exitButton.TabIndex = 7
    Me.exitButton.Text = "E&xit"
    '
    'openButton
    '
    Me.openButton.Location = New System.Drawing.Point(376, 16)
    Me.openButton.Name = "openButton"
    Me.openButton.Size = New System.Drawing.Size(58, 24)
    Me.openButton.TabIndex = 8
    Me.openButton.Text = "&Open..."
    '
    'SwitchBoardForm
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.BackColor = System.Drawing.SystemColors.Control
    Me.ClientSize = New System.Drawing.Size(448, 94)
    Me.Controls.Add(Me.openButton)
    Me.Controls.Add(Me.exitButton)
    Me.Controls.Add(Me.actionsComboBox)
    Me.Controls.Add(Me.semtechPictureBox)
    Me.Cursor = System.Windows.Forms.Cursors.Default
    Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Location = New System.Drawing.Point(219, 182)
    Me.MaximizeBox = False
    Me.Name = "SwitchBoardForm"
    Me.RightToLeft = System.Windows.Forms.RightToLeft.No
    Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>Cleans up managed components.</summary>
  ''' <remarks>Use this method to reclaim managed resources used by this class.</remarks>
  Private Sub onDisposeManagedResources()
  End Sub

  ''' <summary>Cleans up unmanaged components.</summary>
  ''' <remarks>Use this method to reclaim unmanaged resources used by this class.</remarks>
  Private Sub onDisposeUnmanagedResources()
  End Sub

  ''' <summary>Initializes components that might be affected by resize and paint events.</summary>
  ''' <remarks>Called from the class constructor before initializing the form components 
  '''		for components that are affected by resize and paint events.</remarks>
  Private Sub onInitialize()
  End Sub

  ''' <summary>Initializes additional components.</summary>
  ''' <remarks>Called from the class constructor to add other controls or 
  '''   change anything set by the InitializeComponent method.</remarks>
  Private Sub onInstantiate()
  End Sub

#End Region

#Region " METHODS  AND  PROPERTIES "

  ''' <summary>Returns True if an instance of the class was created and not disposed.</summary>
  Friend Shared ReadOnly Property Instantiated() As Boolean
    ' Returns true if an instance of the class was created and not disposed
    Get
      Return My.Application.OpenForms.Count > 0 AndAlso _
          My.Application.OpenForms.Item(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name) IsNot Nothing
    End Get
  End Property

  Private _statusMessage As String = String.Empty
  ''' <summary>Gets or sets the Status message.</summary>
  ''' <value>A <see cref="System.String">String</see>.</value>
  ''' <remarks>Use this property to get the Status message generated by the object.</remarks>
  Public ReadOnly Property StatusMessage() As String
    Get
      Return _statusMessage
    End Get
  End Property

#End Region

#Region " FORM EVENT HANDLERS "

  ''' <summary>Occurs after the form is closed.</summary>
  ''' <param name="sender"><see cref="System.Object"/> instance of this 
  '''   <see cref="System.Windows.Forms.Form"/></param>
  ''' <param name="e"><see cref="System.EventArgs"/></param>
  ''' <remarks>This event is a notification that the form has already gone away before
  ''' control is returned to the calling method (in case of a modal form).  Use this
  ''' method to delete any temporary files that were created or dispose of any objects
  ''' not disposed with the closing event.
  ''' </remarks>
  Private Sub form_Closed(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles MyBase.Closed

  End Sub

  ''' <summary>Occurs before the form is closed</summary>
  ''' <param name="sender"><see cref="System.Object"/> instance of this 
  '''   <see cref="System.Windows.Forms.Form"/></param>
  ''' <param name="e"><see cref="System.ComponentModel.CancelEventArgs"/></param>
  ''' <remarks>Use this method to optionally cancel the closing of the form.
  ''' Because the form is not yet closed at this point, this is also the best 
  ''' place to serialize a form's visible properties, such as size and 
  ''' location. Finally, dispose of any form level objects especially those that
  ''' might needs access to the form and thus should not be terminated after the
  ''' form closed.
  ''' </remarks>
  Private Sub form_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) _
    Handles MyBase.Closing

    ' disable the timer if any
    ' actionTimer.Enabled = False
    System.Windows.Forms.Application.DoEvents()

    ' terminate form objects
    Me.terminateObjects()

  End Sub

  ''' <summary>Is true after form has completed loading so that we can disable check boxes 
  '''   that should not fire until the form is fully loaded.</summary>
  ''' <remarks>Initialization was removed per performance rule CA1805 as it is done 
  '''   automatically by the runtime.</remarks>
  Private loaded As Boolean

  ''' <summary>Occurs when the form is loaded.</summary>
  ''' <param name="sender"><see cref="System.Object"/> instance of this 
  '''   <see cref="System.Windows.Forms.Form"/></param>
  ''' <param name="e"><see cref="System.EventArgs"/></param>
  ''' <remarks>Use this method for doing any final initialization right before 
  '''   the form is shown.  This is a good place to change the Visible and
  '''   ShowInTaskbar properties to start the form as hidden.  
  '''   Starting a form as hidden is useful for forms that need to be running but that
  '''   should not show themselves right away, such as forms with a notify icon in the
  '''   task bar.</remarks>
  Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles MyBase.Load

    Try

      ' Turn on the form hourglass cursor
      Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

      ' instantiate form objects
      Me.instantiateObjects()

      ' set the form caption
      Me.Text = isr.Core.AssemblyInfoExtensions.ExtendedCaption(My.Application.Info) & ": SWITCH BOARD"

      ' set tool tips
      initializeUserInterface()

      ' center the form
      Me.CenterToScreen()

      ' turn on the loaded flag
      loaded = True

    Catch

      ' Use throw without an argument in order to preserve the stack location 
      ' where the exception was initially raised.
      Throw

    Finally

      Me.Cursor = System.Windows.Forms.Cursors.Default

    End Try

  End Sub

#End Region

#Region " PRIVATE  and  PROTECTED "

  ''' <summary>Enumerates the action options.</summary>
  Private Enum ActionOption
    Delimited_File_Test_Panel
    Signatures_Test_Panel
  End Enum

  ''' <summary>Initializes the class objects.</summary>
  ''' <remarks>Called from the form load method to instantiate 
  '''   module-level objects.</remarks>
  Private Sub instantiateObjects()

    ' populate the action list
    Me.populateActiveList()

    ' set registration
    checkPermit()

  End Sub

  ''' <summary>Populates the list of options in the action combo box.</summary>
  ''' <remarks>It seems that out enumerated list does not work very well with
  '''   this list.</remarks>
  Private Sub populateActiveList()

    ' set the action list
    Dim itemCaption As String
    actionsComboBox.Items.Clear()
    itemCaption = ActionOption.Delimited_File_Test_Panel.ToString
    actionsComboBox.Items.Add(itemCaption.Replace("_", " "))
    itemCaption = ActionOption.Signatures_Test_Panel.ToString
    actionsComboBox.Items.Add(itemCaption.Replace("_", " "))
    '    actionsComboBox.Enabled = False
    '    actionsComboBox.SelectedIndex = 0
    '    actionsComboBox.Enabled = True

  End Sub

  ''' <summary>Initializes the user interface and tool tips.</summary>
  ''' <remarks>Call this method from the form load method to set the user interface.</remarks>
  Private Sub initializeUserInterface()
    '    tipsToolTip.SetToolTip(Me.txtDuration, "Enter count-down duration in seconds")
    '    tipsToolTip.SetToolTip(Me.exitButton, "Click to exit")
  End Sub

  ''' <summary>Terminates and disposes of class-level objects.</summary>
  ''' <remarks>Called from the form Closing method.</remarks>
  Private Sub terminateObjects()

  End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

  ''' <summary>Closes the form and exits the application.</summary>
  Private Sub exitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
      Handles exitButton.Click

    ' validate controls manually.
    If Not isr.WindowsForms.Helper.ValidatedControls(Me) Then
      ' indicate that the modal dialog is still running
      Me.DialogResult = Windows.Forms.DialogResult.None
    Else
      Me.Close()
    End If

  End Sub

  ''' <summary>Open selected items.</summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  Private Sub openButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openButton.Click
    Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
    Select Case actionsComboBox.Text.Replace(" ", "_")
      Case ActionOption.Delimited_File_Test_Panel.ToString
        If Delimited.TestPanel.Instantiated Then
          My.Forms.isr_IO_Testers_Delimited_TestPanel.Dispose()
        End If
        My.Forms.isr_IO_Testers_Delimited_TestPanel.ShowDialog()
        My.Forms.isr_IO_Testers_Delimited_TestPanel.Dispose()
      Case ActionOption.Signatures_Test_Panel.ToString
        If Cryptography.TestPanel.Instantiated Then
          My.Forms.isr_IO_Testers_Cryptography_TestPanel.Dispose()
        End If
        My.Forms.isr_IO_Testers_Cryptography_TestPanel.ShowDialog()
        My.Forms.isr_IO_Testers_Cryptography_TestPanel.Dispose()
      Case Else
    End Select
    Me.Cursor = System.Windows.Forms.Cursors.Default
  End Sub

#End Region

#Region " UNUSED "

  ''' <summary>Check if program is registered.  If not, display the registration button</summary>
  ''' 'todo: replace with XML signature.
  Private Sub checkPermit()

#If False Then
		' check if we have a registered version
		If goisrcRegistry.blnIsLicensed Then
		actionsComboBox.Visible = True
		Else
		actionsComboBox.Visible = False
		With cmdRegister
		.Top = actionsComboBox.Top
		.Visible = True
		End With
		End If
#Else
    actionsComboBox.Visible = True
#End If

  End Sub

#If False Then

  Private WithEvents moisrcPW As isrPassword.isrcPassword

  ''' <summary>Handles a password change and saves the new password in the registry</summary>
  ''' <param name="newPassword">The new password</param>
  Private Sub moisrcPW_Change(ByVal newPassword As String) Handles moisrcPW.Change

    ' Save the password
    ' goisrcRegistry.strPassword = newPassword

  End Sub

      moisrcPW = Nothing

#End If

#End Region

End Class