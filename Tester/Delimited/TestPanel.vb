Namespace Delimited

  ''' <summary>Includes code for form TestPanel.</summary>
  ''' <remarks>Launch this form by calling its Show or ShowDialog method from its default
  '''   instance.</remarks>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
  ''' <history date="03/04/04" by="David Hary" revision="1.0.1524.x">
  ''' Created
  ''' </history>
  Friend Class TestPanel
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
      MyBase.New()

      ' Initialize user components that might be affected by resize or paint actions
      onInitialize()

      ' This method is required by the Windows Form Designer.
      InitializeComponent()

      ' Add any initialization after the InitializeComponent() call
      onInstantiate()

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    Try

      If disposing Then
      
        ' Free managed resources when explicitly called
        onDisposeManagedResources()

        If components IsNot Nothing Then
          components.Dispose()
        End If

      
      End If

          ' Free shared unmanaged resources
         onDisposeUnmanagedResources()

      Finally

        ' Invoke the base class dispose method
      MyBase.Dispose(disposing)

    End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents exitButton As System.Windows.Forms.Button
    Friend WithEvents filePathNameLabel As System.Windows.Forms.Label
    Friend WithEvents filePathNameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents outputTextBox As System.Windows.Forms.TextBox
    Friend WithEvents dataWrittenLabel As System.Windows.Forms.Label
    Friend WithEvents statusLabel As System.Windows.Forms.Label
    Friend WithEvents inputTextBox As System.Windows.Forms.TextBox
    Friend WithEvents writeButton As System.Windows.Forms.Button
    Friend WithEvents readButton As System.Windows.Forms.Button
    Friend WithEvents mainTabControl As System.Windows.Forms.TabControl
    Friend WithEvents singleValuesTabPage As System.Windows.Forms.TabPage
    Friend WithEvents arraysTabPage As System.Windows.Forms.TabPage
    Friend WithEvents writeArrayButton As System.Windows.Forms.Button
    Friend WithEvents readArrayButton As System.Windows.Forms.Button
    Friend WithEvents arrayDataGrid As System.Windows.Forms.DataGrid
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
      Me.exitButton = New System.Windows.Forms.Button
      Me.filePathNameLabel = New System.Windows.Forms.Label
      Me.filePathNameTextBox = New System.Windows.Forms.TextBox
      Me.outputTextBox = New System.Windows.Forms.TextBox
      Me.dataWrittenLabel = New System.Windows.Forms.Label
      Me.statusLabel = New System.Windows.Forms.Label
      Me.inputTextBox = New System.Windows.Forms.TextBox
      Me.writeButton = New System.Windows.Forms.Button
      Me.readButton = New System.Windows.Forms.Button
      Me.mainTabControl = New System.Windows.Forms.TabControl
      Me.singleValuesTabPage = New System.Windows.Forms.TabPage
      Me.arraysTabPage = New System.Windows.Forms.TabPage
      Me.arrayDataGrid = New System.Windows.Forms.DataGrid
      Me.readArrayButton = New System.Windows.Forms.Button
      Me.writeArrayButton = New System.Windows.Forms.Button
      Me.mainTabControl.SuspendLayout()
      Me.singleValuesTabPage.SuspendLayout()
      Me.arraysTabPage.SuspendLayout()
      CType(Me.arrayDataGrid, System.ComponentModel.ISupportInitialize).BeginInit()
      Me.SuspendLayout()
      '
      'exitButton
      '
      Me.exitButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
      Me.exitButton.Location = New System.Drawing.Point(416, 320)
      Me.exitButton.Name = "exitButton"
      Me.exitButton.TabIndex = 0
      Me.exitButton.Text = "E&xit"
      '
      'filePathNameLabel
      '
      Me.filePathNameLabel.Location = New System.Drawing.Point(8, 6)
      Me.filePathNameLabel.Name = "filePathNameLabel"
      Me.filePathNameLabel.Size = New System.Drawing.Size(63, 16)
      Me.filePathNameLabel.TabIndex = 1
      Me.filePathNameLabel.Text = "File Name: "
      Me.filePathNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
      '
      'filePathNameTextBox
      '
      Me.filePathNameTextBox.Location = New System.Drawing.Point(76, 4)
      Me.filePathNameTextBox.Name = "filePathNameTextBox"
      Me.filePathNameTextBox.Size = New System.Drawing.Size(412, 20)
      Me.filePathNameTextBox.TabIndex = 2
      Me.filePathNameTextBox.Text = ""
      '
      'outputTextBox
      '
      Me.outputTextBox.Location = New System.Drawing.Point(13, 40)
      Me.outputTextBox.Multiline = True
      Me.outputTextBox.Name = "outputTextBox"
      Me.outputTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
      Me.outputTextBox.Size = New System.Drawing.Size(216, 176)
      Me.outputTextBox.TabIndex = 3
      Me.outputTextBox.Text = ""
      '
      'dataWrittenLabel
      '
      Me.dataWrittenLabel.Location = New System.Drawing.Point(13, 16)
      Me.dataWrittenLabel.Name = "dataWrittenLabel"
      Me.dataWrittenLabel.Size = New System.Drawing.Size(120, 16)
      Me.dataWrittenLabel.TabIndex = 4
      Me.dataWrittenLabel.Text = "Data Written to the File"
      '
      'statusLabel
      '
      Me.statusLabel.Location = New System.Drawing.Point(245, 16)
      Me.statusLabel.Name = "statusLabel"
      Me.statusLabel.Size = New System.Drawing.Size(120, 16)
      Me.statusLabel.TabIndex = 6
      Me.statusLabel.Text = "Data Read from the File"
      '
      'inputTextBox
      '
      Me.inputTextBox.Location = New System.Drawing.Point(245, 40)
      Me.inputTextBox.Multiline = True
      Me.inputTextBox.Name = "inputTextBox"
      Me.inputTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
      Me.inputTextBox.Size = New System.Drawing.Size(216, 176)
      Me.inputTextBox.TabIndex = 5
      Me.inputTextBox.Text = ""
      '
      'writeButton
      '
      Me.writeButton.Location = New System.Drawing.Point(13, 224)
      Me.writeButton.Name = "writeButton"
      Me.writeButton.TabIndex = 7
      Me.writeButton.Text = "&Write"
      '
      'readButton
      '
      Me.readButton.Location = New System.Drawing.Point(245, 224)
      Me.readButton.Name = "readButton"
      Me.readButton.TabIndex = 8
      Me.readButton.Text = "&Read"
      '
      'mainTabControl
      '
      Me.mainTabControl.Controls.Add(Me.singleValuesTabPage)
      Me.mainTabControl.Controls.Add(Me.arraysTabPage)
      Me.mainTabControl.Location = New System.Drawing.Point(8, 32)
      Me.mainTabControl.Name = "mainTabControl"
      Me.mainTabControl.SelectedIndex = 0
      Me.mainTabControl.Size = New System.Drawing.Size(480, 280)
      Me.mainTabControl.TabIndex = 9
      '
      'singleValuesTabPage
      '
      Me.singleValuesTabPage.Controls.Add(Me.writeButton)
      Me.singleValuesTabPage.Controls.Add(Me.inputTextBox)
      Me.singleValuesTabPage.Controls.Add(Me.readButton)
      Me.singleValuesTabPage.Controls.Add(Me.outputTextBox)
      Me.singleValuesTabPage.Controls.Add(Me.statusLabel)
      Me.singleValuesTabPage.Controls.Add(Me.dataWrittenLabel)
      Me.singleValuesTabPage.Location = New System.Drawing.Point(4, 22)
      Me.singleValuesTabPage.Name = "singleValuesTabPage"
      Me.singleValuesTabPage.Size = New System.Drawing.Size(472, 254)
      Me.singleValuesTabPage.TabIndex = 0
      Me.singleValuesTabPage.Text = "Single Values"
      '
      'arraysTabPage
      '
      Me.arraysTabPage.Controls.Add(Me.arrayDataGrid)
      Me.arraysTabPage.Controls.Add(Me.readArrayButton)
      Me.arraysTabPage.Controls.Add(Me.writeArrayButton)
      Me.arraysTabPage.Location = New System.Drawing.Point(4, 22)
      Me.arraysTabPage.Name = "arraysTabPage"
      Me.arraysTabPage.Size = New System.Drawing.Size(472, 254)
      Me.arraysTabPage.TabIndex = 1
      Me.arraysTabPage.Text = "Arrays"
      '
      'arrayDataGrid
      '
      Me.arrayDataGrid.DataMember = ""
      Me.arrayDataGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText
      Me.arrayDataGrid.Location = New System.Drawing.Point(8, 8)
      Me.arrayDataGrid.Name = "arrayDataGrid"
      Me.arrayDataGrid.Size = New System.Drawing.Size(464, 208)
      Me.arrayDataGrid.TabIndex = 12
      '
      'readArrayButton
      '
      Me.readArrayButton.Location = New System.Drawing.Point(360, 224)
      Me.readArrayButton.Name = "readArrayButton"
      Me.readArrayButton.Size = New System.Drawing.Size(112, 23)
      Me.readArrayButton.TabIndex = 11
      Me.readArrayButton.Text = "&Read Array..."
      '
      'writeArrayButton
      '
      Me.writeArrayButton.Location = New System.Drawing.Point(232, 224)
      Me.writeArrayButton.Name = "writeArrayButton"
      Me.writeArrayButton.Size = New System.Drawing.Size(112, 23)
      Me.writeArrayButton.TabIndex = 10
      Me.writeArrayButton.Text = "&Write Array..."
      '
      'TestPanel
      '
      Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
      Me.ClientSize = New System.Drawing.Size(496, 350)
      Me.Controls.Add(Me.mainTabControl)
      Me.Controls.Add(Me.filePathNameTextBox)
      Me.Controls.Add(Me.filePathNameLabel)
      Me.Controls.Add(Me.exitButton)
      Me.Name = "TestPanel"
      Me.Text = "TestPanel"
      Me.mainTabControl.ResumeLayout(False)
      Me.singleValuesTabPage.ResumeLayout(False)
      Me.arraysTabPage.ResumeLayout(False)
      CType(Me.arrayDataGrid, System.ComponentModel.ISupportInitialize).EndInit()
      Me.ResumeLayout(False)

    End Sub

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Cleans up unmanaged components.</summary>
    ''' <remarks>Use this method to reclaim unmanaged resources used by this class.</remarks>
      Private Sub onDisposeUnmanagedResources()
    End Sub

    ''' <summary>Initializes components that might be affected by resize and paint events.</summary>
    ''' <remarks>Called from the class constructor before initializing the form components 
    '''		for components that are affected by resize and paint events.</remarks>
      Private Sub onInitialize()
    End Sub

    ''' <summary>Initializes additional components.</summary>
    ''' <remarks>Called from the class constructor to add other controls or 
    '''   change anything set by the InitializeComponent method.</remarks>
      Private Sub onInstantiate()
    End Sub

    ''' <summary>Initializes the class objects.</summary>
    ''' <exception cref="ApplicationException" guarantee="strong">
    '''   failed instantiating objects.</exception>
    ''' <remarks>Called from the form load method to instantiate 
    '''   module-level objects.</remarks>
      Private Sub instantiateObjects()

    End Sub

    ''' <summary>Initializes the user interface and tool tips.</summary>
    ''' <remarks>Call this method from the form load method to set the user interface.</remarks>
      Private Sub initializeUserInterface()
      '    tipsToolTip.SetToolTip(Me.txtDuration, "Enter count-down duration in seconds")
      '    tipsToolTip.SetToolTip(Me.exitButton, "Click to exit")
    End Sub

  ''' <summary>Terminates and disposes of class-level objects.</summary>
  ''' <remarks>Called from the form Closing method.</remarks>
      Private Sub terminateObjects()

    End Sub

#End Region

#Region " PROPERTIES "

    Private _statusMessage As String = String.Empty
    ''' <summary>Gets or sets the status message.</summary>
    ''' <value>A <see cref="System.String">String</see>.</value>
    ''' <remarks>Use this property to get the status message generated by the object.</remarks>
      Public ReadOnly Property StatusMessage() As String
      Get
        Return _statusMessage
      End Get
    End Property

    ''' <summary>Returns true if an instance of the class was created and not disposed.</summary>
    Friend Shared ReadOnly Property Instantiated() As Boolean
      ' Returns true if an instance of the class was created and not disposed
      Get
        Return My.Application.OpenForms.Count > 0 AndAlso _
            My.Application.OpenForms.Item(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name) IsNot Nothing
      End Get
    End Property

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary>Occurs after the form is closed.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
  ''' <remarks>This event is a notification that the form has already gone away before
  ''' control is returned to the calling method (in case of a modal form).  Use this
  ''' method to delete any temporary files that were created or dispose of any objects
  ''' not disposed with the closing event.
  ''' </remarks>
      Private Sub form_Closed(ByVal sender As System.Object, ByVal e As System.EventArgs) _
      Handles MyBase.Closed

    End Sub

    ''' <summary>Occurs before the form is closed</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.ComponentModel.CancelEventArgs"/></param>
    ''' <remarks>Use this method to allow canceling the closing of the form.
    '''   Because the form is not yet closed at this point, this is also the best 
    '''   place to serialize a form's visible properties, such as size and 
    '''   location.</remarks>
      Private Sub form_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) _
      Handles MyBase.Closing

      ' disable the timer if any
      ' actionTimer.Enabled = False
      System.Windows.Forms.Application.DoEvents()

      ' set module objects that reference other objects to Nothing

      ' terminate form objects
      Me.terminateObjects()

    End Sub

    ''' <summary>Is true after form has completed loading so that we can disable check boxes 
    '''   that should not fire until the form is fully loaded.</summary>
    ''' <remarks>Initialization was removed per performance rule CA1805 as it is done 
    '''   automatically by the runtime.</remarks>
      Private loaded As Boolean

    ''' <summary>Occurs when the form is loaded.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>Use this method for doing any final initialization right before 
    '''   the form is shown.  This is a good place to change the Visible and
    '''   ShowInTaskbar properties to start the form as hidden.  
    '''   Starting a form as hidden is useful for forms that need to be running but that
    '''   should not show themselves right away, such as forms with a notify icon in the
    '''   task bar.</remarks>
      Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) _
      Handles MyBase.Load

      Try

        ' Turn on the form hourglass cursor
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        ' instantiate form objects
        Me.instantiateObjects()

        ' set the form caption
        Me.Text = isr.Core.AssemblyInfoExtensions.ExtendedCaption(My.Application.Info) & ": DELIMITED I/O TEST PANEL"

        ' set tool tips
        initializeUserInterface()

        ' center the form
        Me.CenterToScreen()

        ' update the file name string
        If Me.filePathNameTextBox.Text.Length = 0 Then
          Dim delimitedFile As isr.IO.Delimited.File = New isr.IO.Delimited.File
          Me.filePathNameTextBox.Text = delimitedFile.FilePathName
          delimitedFile.Dispose()
        End If

        ' turn on the loaded flag
        loaded = True

      Catch

        ' Use throw without an argument in order to preserve the stack location 
        ' where the exception was initially raised.
        Throw

      Finally

        ' Turn off the form hourglass cursor
        Me.Cursor = System.Windows.Forms.Cursors.Default

      End Try

    End Sub

    Private Sub form_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated
      ' Me.notesMessageList.PushMessage("Activated")
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary>Closes the form and exits the application.</summary>
    Private Sub exitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
        Handles exitButton.Click

      Try

        'Me.statusLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, _
        '    "{0} {1}", Date.Now.Ticks, Gopher.UsingDevices.ToString)

        ' validate controls manually.
        If Not isr.WindowsForms.Helper.ValidatedControls(Me) Then
          ' indicate that the modal dialog is still running
          Me.DialogResult = Windows.Forms.DialogResult.None
        Else
          Me.Close()
        End If

      Catch ex As isr.IO.BaseException

        ' report failure 
        _statusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
            "Failed closing {0}{1}", _
            Environment.NewLine, ex.Message)
        MessageBox.Show(_statusMessage, Me.Name, _
            MessageBoxButtons.OK, MessageBoxIcon.Error)

      End Try

    End Sub

    Dim dataValues As Double(,) = {}
    Private arrayFilePathName As String
    ''' <summary>reads an array into the data grid.</summary>
    Private Sub readArrayButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles readArrayButton.Click

      ' get a new file name
      arrayFilePathName = OpenFileOpenDialog(arrayFilePathName)

      ' instantiate the delimiter file reader
      Dim reader As isr.IO.Delimited.Reader = New isr.IO.Delimited.Reader("Array Reader")

      ' open the array file 
      reader.FilePathName = arrayFilePathName
      reader.Open()

      Try

        ' display the file name
        filePathNameTextBox.Text = reader.FilePathName()

        ' clear the data array
        Array.Clear(dataValues, 0, dataValues.Length)

        ' read the array from the file
        dataValues = reader.ReadRowsDoubleArray

        ' display the array values
        bindArrayToGrid(arrayDataGrid, dataValues)

        arrayDataGrid.AllowNavigation = False
        arrayDataGrid.ReadOnly = True
        arrayDataGrid.RowHeaderWidth = 0

      Catch ex As isr.IO.BaseException

        Dim frm As New isr.WindowsForms.ExceptionDisplay
        frm.ShowDialog(ex, isr.WindowsForms.ExceptionDisplayButtons.Continue)

      Finally

        ' close the file
        reader.Close()

        ' dispose fo the reader
        reader.Dispose()

      End Try

    End Sub

    Private Sub readButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readButton.Click

      ' instantiate a new reader class
      Dim reader As isr.IO.Delimited.Reader = New isr.IO.Delimited.Reader("Main Reader")

      ' open the file name using the default file name
      reader.Open()

      Try

        ' display the file name
        filePathNameTextBox.Text = reader.FilePathName()

        ' read values from the file
        Dim value As Object = Nothing
        Do While Not reader.IsEndOfFile
          value = reader.Value
          inputTextBox.AppendText(String.Format$("{0}: {1}{2}", value.GetType, value.ToString, _
              Environment.NewLine))
        Loop

      Catch ex As isr.IO.BaseException

        Dim frm As New isr.WindowsForms.ExceptionDisplay
        frm.ShowDialog(ex, isr.WindowsForms.ExceptionDisplayButtons.Continue)

      Finally

        ' close the file
        reader.Close()

        ' dispose fo the reader
        reader.Dispose()

      End Try
    End Sub

    ''' <summary>writes an array to file.</summary>
    Private Sub writeArrayButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles writeArrayButton.Click

      ' get a new file name
      arrayFilePathName = Me.OpenSaveFileDialog(arrayFilePathName)

      ' instantiate the delimiter file reader
      Dim writer As isr.IO.Delimited.Writer = New isr.IO.Delimited.Writer("Array Writer")

      ' open the array file 
      writer.FilePathName = arrayFilePathName
      writer.Open(False)

      Try

        ' display the file name
        filePathNameTextBox.Text = writer.FilePathName()

        ' read the array from the file
        writer.WriteColumns(dataValues)

        ' display the array values
        bindArrayToGrid(arrayDataGrid, dataValues)

        arrayDataGrid.AllowNavigation = False
        arrayDataGrid.ReadOnly = True
        arrayDataGrid.RowHeaderWidth = 0

      Catch ex As isr.IO.BaseException

        Dim frm As New isr.WindowsForms.ExceptionDisplay
        frm.ShowDialog(ex, isr.WindowsForms.ExceptionDisplayButtons.Continue)

      Finally

        ' close the file
        writer.Close()

        ' dispose fo the reader
        writer.Dispose()

      End Try

    End Sub

    ''' <summary>Occurs when the operator selects the Write button.</summary>
    ''' <remarks>This method writes a preset data to the file.</remarks>
    Private Sub writeButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles writeButton.Click

      ' instantiate a new writer class
      Dim writer As isr.IO.Delimited.Writer = New isr.IO.Delimited.Writer("Main Writer")

      ' open the file name using the default file name
      writer.Open(False)

      Try

        ' display the file name
        filePathNameTextBox.Text = writer.FilePathName()

        ' write the bunch of value to file and display on screen
        outputTextBox.Clear()

        Dim value As Object = String.Empty
        writer.Value = value
        outputTextBox.AppendText(String.Format$("{0}: {1}{2}", _
            writer.Value.GetType, writer.Value.ToString, Environment.NewLine))

        writer.Value = System.DBNull.Value
        outputTextBox.AppendText(String.Format$("{0}: {1}{2}", _
            writer.Value.GetType, writer.Value.ToString, Environment.NewLine))

        writer.Value = Int16.MaxValue
        outputTextBox.AppendText(String.Format$("{0}: {1}{2}", _
            writer.Value.GetType, writer.Value.ToString, Environment.NewLine))

        writer.Value = Int32.MaxValue
        outputTextBox.AppendText(String.Format$("{0}: {1}{2}", _
            writer.Value.GetType, writer.Value.ToString, Environment.NewLine))

        writer.Value = Int64.MaxValue - 1
        outputTextBox.AppendText(String.Format$("{0}: {1}{2}", _
            writer.Value.GetType, writer.Value.ToString, Environment.NewLine))

        writer.Value = Convert.ToSingle(1000.001)
        outputTextBox.AppendText(String.Format$("{0}: {1}{2}", _
            writer.Value.GetType, writer.Value.ToString, Environment.NewLine))

        writer.Value = Convert.ToDouble(1000.00001)
        outputTextBox.AppendText(String.Format$("{0}: {1}{2}", _
            writer.Value.GetType, writer.Value.ToString, Environment.NewLine))

        writer.Value = Convert.ToDecimal(1000.01)
        outputTextBox.AppendText(String.Format$("{0}: {1}{2}", _
            writer.Value.GetType, writer.Value.ToString, Environment.NewLine))

        writer.Value = Date.Now
        outputTextBox.AppendText(String.Format$("{0}: {1}{2}", _
            writer.Value.GetType, writer.Value.ToString, Environment.NewLine))

        writer.Value = "A String"
        outputTextBox.AppendText(String.Format$("{0}: {1}{2}", _
            writer.Value.GetType, writer.Value.ToString, Environment.NewLine))

        '      Dim erx As ErrObject
        '      erx.Number = 69
        '      writer.Value = erx
        '      outputTextBox.AppendText(String.Format$("{0}: {1}{2}", _
        '         TypeName(writer.Value), writer.Value.ToString, Environment.NewLine))

        writer.Value = True
        outputTextBox.AppendText(String.Format$("{0}: {1}{2}", _
            writer.Value.GetType, writer.Value.ToString, Environment.NewLine))

        writer.EndOfRecord = True
        writer.Value = "End Of Record"
        outputTextBox.AppendText(String.Format$("{0}: {1}{2}", _
            writer.Value.GetType, writer.Value.ToString, Environment.NewLine))

      Catch ex As isr.IO.BaseException

        Dim frm As New isr.WindowsForms.ExceptionDisplay
        frm.ShowDialog(ex, isr.WindowsForms.ExceptionDisplayButtons.Continue)

      Finally

        ' close the file
        writer.Close()

        ' dispose fo the writer
        writer.Dispose()

      End Try

    End Sub

#End Region

#Region " PRIVATE  and  PROTECTED "

    ''' <summary>Binds a two-dimensional array to a data grid.</summary>
    ''' <param name="grid">the grid</param>
    ''' <param name="dataArray">The data array.</param>
      Private Sub bindArrayToGrid(ByVal grid As DataGrid, ByVal dataArray(,) As Double)

      ' Create a new DataTable.
      Dim myDataTable As DataTable = New DataTable("DataTable")

      ' Declare variables for DataColumn and DataRow objects.
      Dim myDataColumn As DataColumn
      Dim myDataRow As DataRow

      ' Create ID DataColumn, set DataType, ColumnName and add to DataTable.    
      myDataColumn = New DataColumn
      myDataColumn.DataType = System.Type.GetType("System.Int32")
      myDataColumn.ColumnName = "ID"
      myDataColumn.AutoIncrement = True
      myDataColumn.Caption = "ID"
      myDataColumn.ReadOnly = True
      myDataColumn.Unique = True
      ' Add the column to the DataColumnCollection.
      myDataTable.Columns.Add(myDataColumn)

      ' Create new DataColumn, set DataType, ColumnName and add to DataTable.    
      For columnIndex As Int32 = 0 To dataArray.GetUpperBound(1)
        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = columnIndex.ToString(Globalization.CultureInfo.CurrentCulture)
        myDataColumn.ReadOnly = True
        myDataColumn.Unique = False
        ' Add the Column to the DataColumnCollection.
        myDataTable.Columns.Add(myDataColumn)
      Next

      ' Instantiate the DataSet variable.
      Dim myDataSet As New DataSet

      ' Add the new DataTable to the DataSet.
      myDataSet.Tables.Add(myDataTable)

      ' Create three new DataRow objects and add them to the DataTable
      For rowIndex As Int32 = 0 To dataArray.GetUpperBound(0)
        myDataRow = myDataTable.NewRow()
        myDataRow("id") = rowIndex
        For columnIndex As Int32 = 0 To dataArray.GetUpperBound(1)
          myDataRow.Item(columnIndex + 1) = dataArray(rowIndex, columnIndex).ToString(Globalization.CultureInfo.CurrentCulture)
        Next
        myDataTable.Rows.Add(myDataRow)
      Next rowIndex

      ' Instruct the DataGrid to bind to the DataSet, with the 
      ' DataTable as the topmost DataTable.
      grid.SetDataBinding(myDataSet, "DataTable")

    End Sub

    ''' <summary>Cleans up managed components.</summary>
    ''' <remarks>Use this method to reclaim managed resources used by this class.</remarks>
      Private Sub onDisposeManagedResources()

    End Sub

    ''' <summary>Opens the File Open dialog box and gets a file name</summary>
    ''' <param name="fileName"></param>
      Private Function OpenFileOpenDialog(ByVal fileName As String) As String

      Dim fileDialog As New OpenFileDialog

      ' Use the common dialog box
      fileDialog.CheckFileExists = True
      fileDialog.CheckPathExists = True
      fileDialog.Title = "Read from a File"
      fileDialog.FileName = fileName
      fileDialog.Filter = "Text Files(*.TXT;*.CSV;*.LOG)|*.TXT;*.CSV;*.LOG|All files (*.*)|*.*"
      If fileDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
        Return fileDialog.FileName
      Else
        Return fileName
      End If

    End Function

    ''' <summary>Opens the File Save dialog box and gets a file name</summary>
    ''' <param name="fileName"></param>
      Private Function OpenSaveFileDialog(ByVal fileName As String) As String

      Dim fileDialog As New SaveFileDialog
      ' Use the common dialog box
      fileDialog.CheckFileExists = False
      fileDialog.CheckPathExists = True
      fileDialog.Title = "Save Data to a File"
      fileDialog.FileName = fileName
      fileDialog.Filter = "Text Files(*.TXT;*.CSV;*.LOG)|*.TXT;*.CSV;*.LOG|All files (*.*)|*.*"
      If fileDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
        Return fileDialog.FileName
      Else
        Return fileName
      End If
    End Function

#End Region

  End Class
End Namespace